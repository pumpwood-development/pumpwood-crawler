import datetime
import pandas
import itertools
from pumpwood_flaskmisc.database import build_engine_string
from pumpwood_flaskmisc.storage import PumpWoodStorage
from pumpwood_communication.microservices import PumpWoodMicroService
from pumpwood_communication.serializers import pump_json_serializer
from sqlalchemy import create_engine

class PusherClass:
    '''
    Class to help building pushers to datalake.
    '''
    _sql_alchemy_engine = None
    '''Object of the sql alchemy engine'''
    queue_model_class = None
    '''Queue model_class name'''
    job_model_class = None
    '''Job model_class name'''
    add_new_modeling_unit = None
    '''If the crawler can create new modeling units'''
    add_new_attribute = None
    '''If the crawler can create new attributes'''
    add_new_geoarea = None
    '''If the crawler can create new geoareas'''

    def __init__(self, database_config, microservice_config, storage_credencials):
        if self.queue_model_class is None:
            raise Exception('queue_model_class must be setted')

        if self.job_model_class is None:
            raise Exception('job_model_class must be setted')

        database_url = build_engine_string(**database_config)
        self._sql_alchemy_engine = create_engine(database_url
            , json_serializer=pump_json_serializer)
        self._storage = PumpWoodStorage(**storage_credencials)
        self._microservice = PumpWoodMicroService(**microservice_config)
        self._microservice.login()

    @staticmethod
    def break_in_chunks(df_to_break, chunksize=1000):
        to_return = list()
        for g, df in df_to_break.groupby(np.arange(len(df_to_break)) // chunksize):
            to_return.append(df)
        return to_return

    @staticmethod
    def add_queue_and_job_info(queue_data, job_data, extra_info:dict):
        if queue_data is not None:
            extra_info['push_queue_id'] = queue_data.get('pk')

        if job_data is not None:
            extra_info['push_job_id'] = job_data.get('pk')
            extra_info['push_job_name'] = job_data.get('name')

        return extra_info
            

    def get_queue(self, pk):
        '''
        Function responsible to get queue for the crawler.
           
        Args:
          pk (int): Pk of the queue to be retrieved
        
        Kwargs:
          No Kwargs

        Returns:
          dict: Serialized queue object.

        Raises:
          No especific raises.

        Example:
          queue_obj = pusher_class_obj.get_queue(pk=10)
        '''
        return self._microservice.retrieve(model_class=self.queue_model_class, 
            pk=pk)

    def get_job(self, pk):
        '''
        Function responsible to get job for the crawler.
           
        Args:
          pk (int): Pk of the job to be retrieved
        
        Kwargs:
          No Kwargs

        Returns:
          dict: Serialized job object.

        Raises:
          No especific raises.

        Example:
          job_obj = pusher_class_obj.get_job(pk=10)
        '''
        return self._microservice.retrieve(model_class=self.job_model_class,
            pk=pk)

    def process_message(self, msg_data: dict, parameters:dict=None):
        '''
        Process rabbitMQ message at pusher queue.
           
        Args:
          msg_data (dict): rabbitMQ message. Normaly a serialization of 
          the queue object.
        
        Kwargs:
          parameters(dict): Optional parameter to override job['default_parameters'].

        Returns:
          dict: Serialized final queue object.

        Raises:
          Raises errors from the different process at pusher.

        Example:
          job_obj = pusher_class_obj.get_job(pk=10).
        '''
        msg_data = self.get_queue(pk=msg_data['pk'])
        if msg_data['status'] != 'finish_pulling':
            raise Exception('Queue did not finish pulling')
        job_data = self.get_job(pk=msg_data['job_id'])

        #################
        #Processing data#
        print('!!!!pushing queue: %s' % (msg_data['pk'], ))
        msg_data['status'] = 'processing'
        msg_data['start_push_time'] = datetime.datetime.utcnow()
        msg_data = self._microservice.save(obj_dict=msg_data)
        try:
            parameters = parameters or job_data
            processed_data = self.process_data(msg_data=msg_data
                , parameters=parameters)
        except Exception as e:
            msg_data['status'] = 'error_processing'
            msg_data['results'].update({'error_processing': str(e)})
            self._microservice.save(obj_dict=msg_data)
            raise e
        #################

        msg_data = self.get_queue(pk=msg_data['pk'])
        msg_data['results'].update({'processing_results': 
            processed_data.pop('processing_results', {})})
        msg_data['status'] = 'pushing'
        self._microservice.save(obj_dict=msg_data)
        ##############
        #Pushing data#
        try:
            processed_data_results = self.push_data(
                processed_data=processed_data
                , queue_data=msg_data, job_data=job_data
            )
        except Exception as e:
            msg_data['status'] = 'error_pushing'
            msg_data['results'].update({'error_pushing': str(e)})
            self._microservice.save(obj_dict=msg_data)
            raise e
        ##############

        msg_data = self.get_queue(pk=msg_data['pk'])
        msg_data['status'] = 'finish_pushing'
        msg_data['end_push_time'] = datetime.datetime.utcnow().isoformat()
        msg_data['results'].update(processed_data_results)
        resp = self._microservice.save(obj_dict=msg_data)
        print('!!!!finish queue: %s' % (msg_data['pk'], ))
        return resp

    def resolve_modeling_unit(self, mu_dict:dict, add_new_modeling_unit:bool
            , queue_data:dict=None, job_data:dict=None):
        '''
        Resolve modeling units, get its pks by the descriptions
        and if the class attribute add_new_modeling_unit is set 
        True, it will create a new modeling unit with the dictionary
        key value.
           
        Args:
            mu_dict (dict): with the modeling units caracteristics ('description'
            , 'notes', 'extra_field'). The key is the name used in results
            at data key
            of the process_message result.
        
        Kwargs:
            No Kwargs.

        Returns:
            dict: A dictionary with same keys with the pk at server.

        Raises:
            Exception( 'Error when creating new modeling unit:\n--Payload:
            \n%s\n--Msg:%s': If the is an error creating a new modeling unit
            
            Exception('Pusher not adding new modeling units and some
            descriptions not found at server:\n%s': If the pusher is not allowed
            to create new modeling units and sobre descriptions are not at the
            server.

        Example:
            test = {
                'mu_1': {'description': 'Mu 1', 'notes': 'Notes Mu 1'
                        , 'extra_fields': {'mu_1': True}}
              , 'mu_2': {'description': 'Mu 2', 'notes': 'Notes Mu 2'
                        , 'extra_fields': {'mu_1': False}}
              , 'mu_3': {'description': 'Mu 3', 'notes': 'Notes Mu 3'
                        , 'extra_fields': {}}
            }
            pusher_object.resolve_modeling_unit(test)
        '''
        add_new_modeling_unit = add_new_modeling_unit if self.add_new_modeling_unit is None else self.add_new_modeling_unit

        mu_descriptions = [ x[1]['description'] for x in mu_dict.items() ]
        server_modeling_units = self._microservice.list_without_pag('DescriptionModelingUnit', {
            'description__in': mu_descriptions} )

        dict_server_modeling_units = {}
        for x in server_modeling_units:
            extra_info = x.get('extra_info', {})
            extra_info.pop('push_queue_id', None)
            extra_info.pop('push_job_id', None)
            extra_info.pop('push_job_name', None)
            x['extra_info'] = extra_info
            dict_server_modeling_units[x['description']] = x

        on_server = {}
        erro_descriptions = []
        for k, x in mu_dict.items():
            server_data = dict_server_modeling_units.get(x['description'])
            if server_data is not None:
                if add_new_modeling_unit:
                    if x['notes'] != server_data['notes'] or\
                        x['extra_info'] != server_data['extra_info']:
                        x['pk'] = server_data['pk']
                        x['model_class'] = server_data['model_class']
                        x['extra_info'] = self.add_queue_and_job_info(queue_data=queue_data
                                                  , job_data=job_data
                                                  , extra_info=x.get('extra_info', {}))
                        server_data = self._microservice.save(obj_dict=x)

                on_server[k] = server_data
            else:
                if add_new_modeling_unit:
                    try:
                        x['model_class'] = 'DescriptionModelingUnit'
                        x['extra_info'] = self.add_queue_and_job_info(queue_data=queue_data
                                                  , job_data=job_data
                                                  , extra_info=x.get('extra_info', {}))
                        server_data = self._microservice.save(obj_dict=x)
                        on_server[k] = server_data
                    except Exception as err:
                        raise Exception( 'Error when creating new modeling unit:\n--Payload:\n%s\n--Msg:%s' % ( 
                            str(x), str(err) ) )
                else:
                    erro_descriptions.append(x['description'])

        if len(erro_descriptions) != 0:
            raise Exception('Pusher not adding new modeling units and some descriptions not found at server:\n%s'
                , (str(erro_descriptions), )) 
        return on_server

    def resolve_attribute(self, att_dict:dict, add_new_attribute:bool
            , queue_data:dict=None, job_data:dict=None):
        '''
        Resolve attribute, get its pks by the descriptions
        and if the class attribute add_new_attribute is set 
        True, it will create a new attribute with the dictionary
        key value.
           
        Args:
            att_dict (dict): with the attribute caracteristics ('description'
            , 'notes', 'extra_field'). The key is the name used in results
            at data key
            of the process_message result.
        
        Kwargs:
            No Kwargs.

        Returns:
            dict: A dictionary with same keys with the pk at server.

        Raises:
            Exception( 'Error when creating new attribute:\n--Payload:
            \n%s\n--Msg:%s': If the is an error creating a new attribute
            
            Exception('Pusher not adding new attribute and some
            descriptions not found at server:\n%s': If the pusher is not allowed
            to create new attributes and sobre descriptions are not at the
            server.

        Example:
            test = {
                'att_1': {'description': 'Att 1', 'notes': 'Notes Att 1'
                        , 'extra_fields': {'att_1': True}}
              , 'att_2': {'description': 'Att 2', 'notes': 'Notes Att 2'
                        , 'extra_fields': {'att_1': False}}
              , 'mu_3': {'description': 'Att 3', 'notes': 'Notes Att 3'
                        , 'extra_fields': {}}
            }
            pusher_object.resolve_attribute(test)
        '''
        add_new_attribute = add_new_attribute if self.add_new_attribute is None else self.add_new_attribute

        att_descriptions = [ x[1]['description'] for x in att_dict.items() ]
        server_attributes = self._microservice.list_without_pag('DescriptionAttribute', {
            'description__in': att_descriptions} )

        dict_server_attributes = {}
        for x in server_attributes:
            extra_info = x.get('extra_info', {})
            extra_info.pop('push_queue_id', None)
            extra_info.pop('push_job_id', None)
            extra_info.pop('push_job_name', None)
            x['extra_info'] = extra_info
            dict_server_attributes[x['description']] = x

        on_server = {}
        erro_descriptions = []
        for k, x in att_dict.items():
            server_data = dict_server_attributes.get(x['description'])
            if server_data is not None:
                if add_new_attribute:
                    if x['notes'] != server_data['notes'] or\
                        x['extra_info'] != server_data['extra_info']:
                        x['pk'] = server_data['pk']
                        x['model_class'] = server_data['model_class']
                        x['extra_info'] = self.add_queue_and_job_info(queue_data=queue_data
                                                  , job_data=job_data
                                                  , extra_info=x.get('extra_info', {}))

                        server_data = self._microservice.save(obj_dict=x)
                on_server[k] = server_data
            else:
                if add_new_attribute:
                    try:
                        x['model_class'] = 'DescriptionAttribute'
                        x['extra_info'] = self.add_queue_and_job_info(queue_data=queue_data
                                                  , job_data=job_data
                                                  , extra_info=x.get('extra_info', {}))
                        server_data = self._microservice.save(obj_dict=x)
                        on_server[k] = server_data
                    except Exception as err:
                        raise Exception( 'Error when creating new attribute:\n--Payload:\n%s\n--Msg:%s' % ( 
                            str(x), str(err) ) )
                else:
                    erro_descriptions.append(x['description'])
        
        if len(erro_descriptions) != 0:
            raise Exception('Pusher not adding new attributes and some descriptions not found at server:\n%s'
                , (str(erro_descriptions), ))

        return on_server

    def resolve_geoarea(self, geo_dict:dict, add_new_geoarea:bool
            , queue_data:dict=None, job_data:dict=None):
        '''
        Resolve geoarea, get its pks by the descriptions
        and if the class geoarea add_new_geoarea is set 
        True, it will create a new geoarea with the dictionary
        key value.
           
        Args:
            geo_dict (dict): with the geoarea caracteristics ('description'
            , 'notes', 'extra_field'). The key is the name used in results
            at data key
            of the process_message result.
        
        Kwargs:
            No Kwargs.

        Returns:
            dict: A dictionary with same keys with the pk at server.

        Raises:
            Exception( 'Error when creating new geoarea:\n--Payload:
            \n%s\n--Msg:%s': If the is an error creating a new geoarea
            
            Exception('Pusher not adding new geoarea and some
            descriptions not found at server:\n%s': If the pusher is not allowed
            to create new geoareas and sobre descriptions are not at the
            server.

        Example:
            test = {
                'geoarea_1': {'description': 'Geoarea 1', 'notes': 'Notes Geoarea 1'
                        , 'extra_fields': {'geoarea_1': True}}
              , 'geoarea_2': {'description': 'Geoarea 2', 'notes': 'Notes Geoarea 2'
                        , 'extra_fields': {'geoarea_1': False}}
              , 'geoareas_3': {'description': 'Geoarea 3', 'notes': 'Notes Att 3'
                        , 'extra_fields': {}}
            }
            pusher_object.resolve_geoarea(test)
        '''
        add_new_geoarea = add_new_geoarea if self.add_new_geoarea is None else self.add_new_geoarea

        geoarea_descriptions = [ x[1]['description'] for x in geo_dict.items() ]
        server_geoarea = self._microservice.list_without_pag('DescriptionGeoArea', {
            'description__in': geoarea_descriptions} )

        dict_server_geoarea = {}
        for x in server_geoarea:
            extra_info = x.get('extra_info', {})
            extra_info.pop('push_queue_id', None)
            extra_info.pop('push_job_id', None)
            extra_info.pop('push_job_name', None)
            x['extra_info'] = extra_info
            dict_server_geoarea[x['description']] = x

        on_server = {}
        erro_descriptions = []
        for k, x in geo_dict.items():
            server_data = dict_server_geoarea.get(x['description'])
            if server_data is not None:
                if add_new_geoarea:
                    if x['notes'] != server_data['notes'] or\
                        x['extra_info'] != server_data['extra_info']:
                        x['pk'] = server_data['pk']
                        x['model_class'] = server_data['model_class']
                        x['extra_info'] = self.add_queue_and_job_info(queue_data=queue_data
                                                  , job_data=job_data
                                                  , extra_info=x.get('extra_info', {}))
                        server_data = self._microservice.save(obj_dict=x)
                on_server[k] = server_data
            else:
                if add_new_geoarea:
                    try:
                        x['model_class'] = 'DescriptionGeoArea'
                        x['extra_info'] = self.add_queue_and_job_info(queue_data=queue_data
                                                  , job_data=job_data
                                                  , extra_info=x.get('extra_info', {}))
                        server_data = self._microservice.save(obj_dict=x)
                        on_server[k] = server_data
                    except Exception as err:
                        raise Exception( 'Error when creating new geoarea:\n--Payload:\n%s\n--Msg:%s' % ( 
                            str(x), str(err) ) )
                else:
                    erro_descriptions.append(x['description'])
        
        if len(erro_descriptions) != 0:
            raise Exception('Pusher not adding new geoareas and some descriptions not found at server:\n%s'
                , (str(erro_descriptions), ))

        return on_server

    def process_data(self, msg_data:dict, parameters:dict):
        '''
        Function responsible to process crawler data to put in pumpwood
        format.
           
        Args:
            msg_data (dict): rabbitMQ message, usually a queue serialized
            object.
            parameters(dict): Some parameters to be used in data processing
            usually 
        
        Kwargs:
            No Kwargs.

        Returns:
            data (list): List of dictionaries with keys time, modeling_unit, geoarea
            , attribute, value
            dict_mu (dict): with keys that span all unique values of 
                data->modeling_unit
            dict_geo (dict): with keys that span all unique values of 
                data->geoarea
            dict_att (dict): with keys that span all unique values of 
                data->attribute

        Raises:
            No especify raises

        Example:
            ---
        '''
        raise Exception('process_data must be implemented')

    def push_data(self, processed_data:dict, queue_data:dict={}, job_data:dict={}):
        '''
        Push processed data to pumpwood server.
           
        Args:
            processed_data (dict): Dictionary result from process_data
            function.
        
        Kwargs:
            No Kwargs.

        Returns:
            dict: Returns the final queue serialized object.

        Raises:
            Exception('dataframe to load with duplicates: %s'): if the 
            data processed have more than one value for each time
            , modeling_unit, geoarea, attribute

        Example:
            ---
        '''
        data = processed_data['data']
        if len(data) == 0:
            return {
                "pushing_results": "no data to process",
            }

        dict_mu = processed_data['dict_mu']
        dict_att = processed_data['dict_att']
        dict_geo = processed_data['dict_geo']

        add_new_modeling_unit = job_data.get('default_parameters',{}).get('add_new_modeling_unit')
        add_new_attribute = job_data.get('default_parameters',{}).get('add_new_attribute')
        add_new_geoarea = job_data.get('default_parameters',{}).get('add_new_geoarea')

        add_new_modeling_unit = queue_data.get('extra_parameters').get('add_new_modeling_unit'
            , add_new_modeling_unit)
        add_new_attribute = queue_data.get('extra_parameters').get('add_new_attribute'
            , add_new_attribute)
        add_new_geoarea = queue_data.get('extra_parameters').get('add_new_geoarea'
            , add_new_geoarea)

        add_new_modeling_unit = False if add_new_modeling_unit is None else add_new_modeling_unit
        add_new_attribute = False if add_new_attribute is None else add_new_attribute
        add_new_geoarea = False if add_new_geoarea is None else add_new_geoarea

        resolved_mu_dict = self.resolve_modeling_unit(mu_dict=dict_mu
            , add_new_modeling_unit=add_new_modeling_unit
            , queue_data=queue_data, job_data=job_data)

        resolved_att_dict = self.resolve_attribute(att_dict=dict_att
            , add_new_attribute=add_new_attribute
            , queue_data=queue_data, job_data=job_data)

        resolved_geo_dict = self.resolve_geoarea(geo_dict=dict_geo
            , add_new_geoarea=add_new_geoarea
            , queue_data=queue_data, job_data=job_data)

        data_df = pandas.DataFrame(data)
        validation_count = data_df.groupby(['time', 'modeling_unit', 'geoarea', 'attribute']).count()
        validation = validation_count[validation_count['value'] != 1]
        if len(validation) != 0:
            validation = validation.reset_index()
            raise Exception('dataframe to load with duplicates: %s' % (validation.to_dict('records'), ))
        
        ##########################
        #Check missing on resolve#
        att_keys_list = list(resolved_att_dict.keys())
        missing_att = data_df.loc[~data_df['attribute'].isin(att_keys_list), 'attribute'].unique()
        
        geo_keys_list = list( resolved_geo_dict.keys() )
        missing_geo = data_df.loc[ ~data_df['geoarea'].isin( geo_keys_list ), 'geoarea' ].unique()

        mu_keys_list = list( resolved_mu_dict.keys() )
        missing_mu = data_df.loc[ ~data_df['modeling_unit'].isin(mu_keys_list), 'modeling_unit' ].unique()

        if ( len(missing_att) + len(missing_geo) + len(missing_mu) ) > 0:
            Exception('Some of the index are missing:\nmissing_att: %s\nmissing_geo: %s\nmissing_mu: %s' % (missing_att
                , missing_geo, missing_mu))
        ##########################

        ###############################################
        #Check if all keys have different descriptions#
        att_check_distinct = []
        for item, key in resolved_att_dict.items():
            att_check_distinct.append({'key': item, 'description': key['description']})

        geo_check_distinct = []
        for item, key in resolved_geo_dict.items():
            geo_check_distinct.append({'key': item, 'description': key['description']})

        mu_check_distinct = []
        for item, key in resolved_mu_dict.items():
            mu_check_distinct.append({'key': item, 'description': key['description']})

        att_check_distinct_df = pandas.DataFrame(att_check_distinct)
        geo_check_distinct_df = pandas.DataFrame(geo_check_distinct)
        mu_check_distinct_df = pandas.DataFrame(mu_check_distinct)

        att_check_count_description = att_check_distinct_df.groupby('description').count().reset_index()
        geo_check_count_description = geo_check_distinct_df.groupby('description').count().reset_index()
        mu_check_count_description = mu_check_distinct_df.groupby('description').count().reset_index()

        att_check_count_key = att_check_distinct_df.groupby('key').count().reset_index()
        geo_check_count_key = geo_check_distinct_df.groupby('key').count().reset_index()
        mu_check_count_key = mu_check_distinct_df.groupby('key').count().reset_index()

        att_check_distinct_agg = att_check_distinct_df.merge(att_check_count_description, how='left'
            , on=['description'], suffixes=['', '_desc_count']).merge(att_check_count_key, how='left'
            , on=['key'], suffixes=['', '_key_count'])

        geo_check_distinct_agg = geo_check_distinct_df.merge(geo_check_count_description, how='left'
            , on=['description'], suffixes=['', '_desc_count']).merge(geo_check_count_key, how='left'
            , on=['key'], suffixes=['', '_key_count'])

        mu_check_distinct_agg = mu_check_distinct_df.merge(mu_check_count_description, how='left'
            , on=['description'], suffixes=['', '_desc_count']).merge(mu_check_count_key, how='left'
            , on=['key'], suffixes=['', '_key_count'])

        more_key_att = att_check_distinct_agg.loc[att_check_distinct_agg['key_desc_count'] > 1, ['key', 'description'] ]
        more_key_geo = geo_check_distinct_agg.loc[geo_check_distinct_agg['key_desc_count'] > 1, ['key', 'description'] ]
        more_key_mu = mu_check_distinct_agg.loc[mu_check_distinct_agg['key_desc_count'] > 1, ['key', 'description'] ]

        more_desc_att = att_check_distinct_agg.loc[att_check_distinct_agg['description_key_count'] > 1, ['key', 'description'] ]
        more_desc_geo = geo_check_distinct_agg.loc[geo_check_distinct_agg['description_key_count'] > 1, ['key', 'description'] ]
        more_desc_mu = mu_check_distinct_agg.loc[mu_check_distinct_agg['description_key_count'] > 1, ['key', 'description'] ]

        more_key_att_dict = more_key_att.groupby('description')['key'].apply(list).to_dict()
        more_key_geo_dict = more_key_geo.groupby('description')['key'].apply(list).to_dict()
        more_key_mu_dict = more_key_mu.groupby('description')['key'].apply(list).to_dict()
        more_desc_att = more_desc_att.groupby('key')['description'].apply(list).to_dict()
        more_desc_geo = more_desc_geo.groupby('key')['description'].apply(list).to_dict()
        more_desc_mu = more_desc_mu.groupby('key')['description'].apply(list).to_dict()

        error_dict = {}
        error_dict['more_key_att_dict'] = more_key_att_dict if more_key_att_dict else None
        error_dict['more_key_geo_dict'] = more_key_geo_dict if more_key_geo_dict else None
        error_dict['more_key_mu_dict'] = more_key_mu_dict if more_key_mu_dict else None
        error_dict['more_desc_att'] = more_desc_att if more_desc_att else None
        error_dict['more_desc_geo'] = more_desc_geo if more_desc_geo else None
        error_dict['more_desc_mu'] = more_desc_mu if more_desc_mu else None

        error_dict = dict((k, v) for k, v in error_dict.items() if v is not None)
        if error_dict:
            raise Exception('Some keys or descriptions have more than one for each: %s' % (error_dict, ))
        ###############################################

        data_df['attribute_id'] = data_df['attribute'].apply(lambda x: resolved_att_dict[x]['pk'])
        data_df['geoarea_id'] = data_df['geoarea'].apply(lambda x: resolved_geo_dict[x]['pk'])
        data_df['modeling_unit_id'] = data_df['modeling_unit'].apply(lambda x: resolved_mu_dict[x]['pk'])

        data_df = data_df[ ['time', 'modeling_unit_id', 'geoarea_id', 'attribute_id', 'value'] ]


        data_input = self._microservice.save(obj_dict={
            'model_class': 'DataInputDatabaseVariable',
            'origin': 'REST',
            'created_by_id': 0
        })

        to_save = self.break_in_chunks(data_chunks)
        n_to_save = len(to_save)
        for i in range(0, n_to_save):
            print("Posting data chunks: %d from %d" % (i, n_to_save))
            bulk_load_resp = self._microservice.execute_action(model_class='DataInputDatabaseVariable'
                                            , action='bulk_load'
                                            , pk=data_input['pk']
                                            , parameters={'data_to_save': to_save[i].to_dict('records')})
        
        process_result = self._microservice.execute_action('DataInputDatabaseVariable',
                                                           'process_input',
                                                           data_input['pk'])
        return {
            "pushing_results": {
                'process_result': process_result.get('result', process_result)
              , 'data_input_id': data_input['pk']
            },
        }