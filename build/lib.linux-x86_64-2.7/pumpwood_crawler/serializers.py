from marshmallow import (
    Schema, pre_load, pre_dump, post_load, validates_schema,
    validates, fields, ValidationError
)

from pumpwoodflask_views import serializers

##########
###List###
##########
class SerializerCrawlerJob(serializers.PumpWoodSerializer):
    model = None

    class Meta:
        additional = ['name', 'last_run', 'default_parameters'
        , 'pumpwood_auth', 'pumpwood_end_points', 'data_origin_endpoints'
        , 'data_origin_auth']


class SerializerCrawlerJobQueue(serializers.PumpWoodSerializer):
    model = None

    class Meta:
        additional = ['job_id', 'creation_time', 'start_time', 'end_time'
        , 'start_push_time', 'end_push_time', 'status', 'results'
        , 'extra_parameters']
    


