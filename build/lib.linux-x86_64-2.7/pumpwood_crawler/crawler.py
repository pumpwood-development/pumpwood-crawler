# -*- coding: utf-8 -*-
from flask import Flask
from flask_migrate import Migrate
from flask_sqlalchemy import SQLAlchemy
from pumpwood_flaskmisc.database import build_engine_string
from pumpwood_flaskmisc.models import FlaskPumpWoodBaseModel
from pumpwood_flaskmisc.rabbitmq import PumpWoodRabbitMQ
from datetime import datetime

db = SQLAlchemy(model_class=FlaskPumpWoodBaseModel)

class CrawlerJob(db.Model):
    name = db.Column(db.String(154), nullable=False, unique=True, index=True)
    last_run = db.Column(db.DateTime(timezone=False), unique=True, nullable=True, index=True)
    default_parameters = db.Column(db.JSON)
    pumpwood_auth = db.Column(db.JSON)
    pumpwood_end_points = db.Column(db.JSON)
    data_origin_endpoints = db.Column(db.JSON)
    data_origin_auth = db.Column(db.JSON)

    __tablename__ = 'crawler_job'

    def __repr__(self):
        return '<%d:CrawlerJob %r>' % (self.id or -1, self.name)

    def create_job_queue(self, extra_parameters: dict={}):
        raise Exception('Not implemented create_job_queue')

    def push_data(self, pulled_data, pulled_keys, pumpwood_end_points, data_origin_auth):
        return None


QUEUE_STATUS = ('idle', 'pulling', 'error_pulling', 'saving', 'error_saving', 'finish_pulling'
    , 'processing', "error_processing", 'pushing', 'error_pushing', 'finish_pushing'
    , 'canceled')


class CrawlerJobQueue(db.Model):
    job_id = db.Column(db.Integer, db.ForeignKey('crawler_job.id'), nullable=False)

    creation_time = db.Column(db.DateTime(timezone=False), nullable=True, default=datetime.utcnow, index=True)
    start_time = db.Column(db.DateTime(timezone=False), nullable=True, index=True)
    end_time = db.Column(db.DateTime(timezone=False), nullable=True, index=True)
    start_push_time = db.Column(db.DateTime(timezone=False), nullable=True, index=True)
    end_push_time = db.Column(db.DateTime(timezone=False), nullable=True, index=True)
    status = db.Column(db.String(20), nullable=False, default="idle", index=True)
    results = db.Column(db.JSON, default={})
    extra_parameters = db.Column(db.JSON)

    __tablename__ = 'crawler_job_queue'
    __table_args__ = (db.CheckConstraint(status.in_(QUEUE_STATUS)), )

    def __repr__(self):
        return '<%d:CrawlerJobQueue >' % (self.id or -1, )
