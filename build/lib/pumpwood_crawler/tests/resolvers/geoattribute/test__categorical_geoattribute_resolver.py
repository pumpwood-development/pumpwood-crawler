"""Module to test geoattribute resolvers."""
# -*- coding: utf-8 -*-
# import requests
# import copy
import pandas as pd
from pumpwood_flaskmisc.testing import TestPumpWood
from pusher_resolvers.resolvers import GeoAttributeResolver
from pumpwood_communication.microservices import PumpWoodMicroService
from tests.config.settings import TEST_CONFIG


new_categories = pd.DataFrame([
    {'geoattribute': "Categoria vazia.",
     'value': 1, "ordering": 1,
     'description': 'Uma data 1',
     'extra_info': {"chubaca chcuhu": True}},

    {'geoattribute': "Categoria vazia.",
     'value': 2, "ordering": 2,
     'description': 'Uma data 2',
     'extra_info': {"chubaca chcuhu": 10}},

    {'geoattribute': "Categoria vazia.",
     'value': 3, "ordering": 3,
     'description': 'Uma data 3',
     'extra_info': {"chubaca não chuchu": 1}},

    {'geoattribute': "Categoria vazia.",
     'value': 4, "ordering": 4,
     'description': 'Uma data 4',
     'extra_info': {"chubaca não chuchu": 2}}, ])


update_categories = pd.DataFrame([
    # Swipe values and descriptions
    {'geoattribute': "Credit Risk by Moody's",
     'value': 1, "ordering": 2,
     'description': "A1",
     'extra_info': {"risco": "moodys"}},

    {'geoattribute': "Credit Risk by Moody's",
     'value': 2, "ordering": 1,
     'description': "BAA3",
     'extra_info': {"risco": "moodys"}},

    # Change value of an existing description
    {'geoattribute': "Credit Risk by DBRS",
     'value': 1, "ordering": 1000,
     'description': "BBB",
     'extra_info': {"risco": "dbrs"}},

    # Change description of an existing value
    {'geoattribute': "Credit Risk by Fitch",
     'value': 1, "ordering": 1,
     'description': "Chubaca",
     'extra_info': {"risco": "fitch"}}, ])


class TestCatericalGeoAttributeResolver(TestPumpWood):
    """Test Hierarchy Attribute resolvers."""

    load_balancer_address = 'http://localhost:8080/'
    apps_to_regenerate = ['pumpwood-datalake-app']

    # session = requests.session()
    # session.headers.update(
    #     {'Authorization': '6cab4ba54bb8f92af7e135d7ad8c7d70d4555f7f'})
    def test_add_category(self):
        """Test adding new geoattribute categories with resolver."""
        microservice = PumpWoodMicroService(**TEST_CONFIG['MICROSERVICE'])
        microservice.login()

        queue_data = microservice.list_one(
            model_class='DatabaseETLQueue', pk=4)
        job_data = microservice.list_one(
            model_class='DatabaseETLJob', pk=queue_data['job_id'])

        geoatt_resolver = GeoAttributeResolver(
            microservice=microservice, queue_data=queue_data,
            job_data=job_data, add_new_dimention=True,
            add_new_categorical='add', add_hierarchy='add')

        result_resolver = geoatt_resolver.resolve_categories(
            categories=new_categories.copy())

        descriptions = microservice.list_without_pag(
            model_class="DescriptionGeoAttribute",
            filter_dict={"description__in": new_categories[
                'geoattribute'].unique().tolist()})

        desc_pk = dict([
            (x['description'], x['pk']) for x in descriptions])
        temp_new_categories = new_categories.copy()
        temp_new_categories['geoattribute_id'] = temp_new_categories[
            'geoattribute'].map(desc_pk)
        temp_new_categories = temp_new_categories[[
            'ordering', 'value', 'geoattribute_id', 'description']]
        merge_result = result_resolver.merge(temp_new_categories)
        self.assertEqual(len(merge_result), len(new_categories))

    def test_fail_add_category(self):
        """Test adding new geoattribute categories with resolver."""
        microservice = PumpWoodMicroService(**TEST_CONFIG['MICROSERVICE'])
        microservice.login()

        queue_data = microservice.list_one(
            model_class='DatabaseETLQueue', pk=4)
        job_data = microservice.list_one(
            model_class='DatabaseETLJob', pk=queue_data['job_id'])

        geoatt_resolver = GeoAttributeResolver(
            microservice=microservice, queue_data=queue_data,
            job_data=job_data, add_new_dimention=True,
            add_new_categorical='fail', add_hierarchy='add')

        with self.assertRaises(Exception):
            geoatt_resolver.resolve_categories(
                categories=new_categories.copy())

    def test_update_category(self):
        """Test update new geoattribute categories with resolver."""
        microservice = PumpWoodMicroService(**TEST_CONFIG['MICROSERVICE'])
        microservice.login()

        queue_data = microservice.list_one(
            model_class='DatabaseETLQueue', pk=4)
        job_data = microservice.list_one(
            model_class='DatabaseETLJob', pk=queue_data['job_id'])

        geoatt_resolver = GeoAttributeResolver(
            microservice=microservice, queue_data=queue_data,
            job_data=job_data, add_new_dimention=True,
            add_new_categorical='add', add_hierarchy='add')

        result_resolver = geoatt_resolver.resolve_categories(
            categories=update_categories.copy())

        descriptions = microservice.list_without_pag(
            model_class="DescriptionGeoAttribute",
            filter_dict={"description__in": update_categories[
                'geoattribute'].unique().tolist()})

        desc_pk = dict([
            (x['description'], x['pk']) for x in descriptions])
        temp_update_categories = update_categories.copy()
        temp_update_categories['geoattribute_id'] = temp_update_categories[
            'geoattribute'].map(desc_pk)
        temp_update_categories = temp_update_categories[[
            'ordering', 'value', 'geoattribute_id', 'description']]
        merge_result = result_resolver.merge(temp_update_categories)
        self.assertEqual(len(merge_result), len(update_categories))

    def test_fail_update_category(self):
        """Test fail new geoattribute categories with resolver."""
        microservice = PumpWoodMicroService(**TEST_CONFIG['MICROSERVICE'])
        microservice.login()

        queue_data = microservice.list_one(
            model_class='DatabaseETLQueue', pk=4)
        job_data = microservice.list_one(
            model_class='DatabaseETLJob', pk=queue_data['job_id'])

        geoatt_resolver = GeoAttributeResolver(
            microservice=microservice, queue_data=queue_data,
            job_data=job_data, add_new_dimention=True,
            add_new_categorical='fail', add_hierarchy='add')

        with self.assertRaises(Exception):
            geoatt_resolver.resolve_categories(
                categories=update_categories.copy())

    def test_nochange_category(self):
        """Test no changing geoattribute categories with resolver."""
        microservice = PumpWoodMicroService(**TEST_CONFIG['MICROSERVICE'])
        microservice.login()

        queue_data = microservice.list_one(
            model_class='DatabaseETLQueue', pk=4)
        job_data = microservice.list_one(
            model_class='DatabaseETLJob', pk=queue_data['job_id'])

        cat_geoattributes = microservice.list_without_pag(
            model_class="CategoricalGeoAttributeDescription",
            filter_dict={"geoattribute_id": 57})
        df_cat_geoattributes = pd.DataFrame(cat_geoattributes)
        to_list = df_cat_geoattributes['geoattribute_id'].unique().tolist()

        cat_geoattributes = microservice.list_without_pag(
            model_class="DescriptionGeoAttribute",
            filter_dict={"pk__in": to_list})
        map_geoattribute_id = dict([
            (x['pk'], x['description'])
            for x in cat_geoattributes])
        df_cat_geoattributes['geoattribute'] = df_cat_geoattributes[
            'geoattribute_id'].map(map_geoattribute_id)
        nochange_categories = df_cat_geoattributes[[
            'geoattribute', 'description', 'value', 'ordering', 'extra_info']]

        geoatt_resolver = GeoAttributeResolver(
            microservice=microservice, queue_data=queue_data,
            job_data=job_data, add_new_dimention=True,
            add_new_categorical='add', add_hierarchy='add')
        result_resolver = geoatt_resolver.resolve_categories(
            categories=nochange_categories.copy())

        descriptions = microservice.list_without_pag(
            model_class="DescriptionGeoAttribute",
            filter_dict={"description__in": nochange_categories[
                'geoattribute'].unique().tolist()})

        desc_pk = dict([
            (x['description'], x['pk']) for x in descriptions])
        temp_nochange_categories = nochange_categories.copy()
        temp_nochange_categories['geoattribute_id'] = temp_nochange_categories[
            'geoattribute'].map(desc_pk)
        temp_nochange_categories = temp_nochange_categories[[
            'ordering', 'value', 'geoattribute_id', 'description']]
        merge_result = result_resolver.merge(temp_nochange_categories)
        self.assertEqual(len(merge_result), len(nochange_categories))

    def test_notfail_nochange_category(self):
        """Test not fail no changing geoattribute categories with resolver."""
        microservice = PumpWoodMicroService(**TEST_CONFIG['MICROSERVICE'])
        microservice.login()

        queue_data = microservice.list_one(
            model_class='DatabaseETLQueue', pk=4)
        job_data = microservice.list_one(
            model_class='DatabaseETLJob', pk=queue_data['job_id'])

        cat_geoattributes = microservice.list_without_pag(
            model_class="CategoricalGeoAttributeDescription",
            filter_dict={"geoattribute_id": 57})
        df_cat_geoattributes = pd.DataFrame(cat_geoattributes)
        to_list = df_cat_geoattributes['geoattribute_id'].unique().tolist()

        cat_geoattributes = microservice.list_without_pag(
            model_class="DescriptionGeoAttribute",
            filter_dict={"pk__in": to_list})
        map_geoattribute_id = dict([
            (x['pk'], x['description'])
            for x in cat_geoattributes])
        df_cat_geoattributes['geoattribute'] = df_cat_geoattributes[
            'geoattribute_id'].map(map_geoattribute_id)
        nochange_categories = df_cat_geoattributes[[
            'geoattribute', 'description', 'value', 'ordering', 'extra_info']]

        geoatt_resolver = GeoAttributeResolver(
            microservice=microservice, queue_data=queue_data,
            job_data=job_data, add_new_dimention=True,
            add_new_categorical='fail', add_hierarchy='add')
        result_resolver = geoatt_resolver.resolve_categories(
            categories=nochange_categories.copy())

        descriptions = microservice.list_without_pag(
            model_class="DescriptionGeoAttribute",
            filter_dict={"description__in": nochange_categories[
                'geoattribute'].unique().tolist()})

        desc_pk = dict([
            (x['description'], x['pk']) for x in descriptions])
        temp_nochange_categories = nochange_categories.copy()
        temp_nochange_categories['geoattribute_id'] = temp_nochange_categories[
            'geoattribute'].map(desc_pk)
        temp_nochange_categories = temp_nochange_categories[[
            'ordering', 'value', 'geoattribute_id', 'description']]
        merge_result = result_resolver.merge(temp_nochange_categories)
        self.assertEqual(len(merge_result), len(nochange_categories))
