"""Module to test attribute resolvers."""
# -*- coding: utf-8 -*-
# import requests
import copy
from pumpwood_flaskmisc.testing import TestPumpWood, is_dict_equal
from pusher_resolvers.resolvers import GeoAttributeResolver
from pumpwood_communication.microservices import PumpWoodMicroService
from tests.config.settings import TEST_CONFIG

new_geoattributes = {
    'chubaca': {
        'type_id': 'FIXED_CATEG',
        'description': 'chubaca',
        'notes': 'Ahhhhhhh!!!',
        'dimentions': {'movie': 'star_wars'},
        'extra_info': {'var_1': 'a',  'var_2': 'd'},
    },
    'chopolin colorado!': {
        'type_id': 'FIXED_CATEG',
        'description': 'chopolin colorado!',
        'notes': 'Quem contava com a minha astúcia?',
        'dimentions': {'movie': 'chopolin colorado'},
        'extra_info': {'var_1': 'b',  'var_2': 'c'},
    },
    'baleia branca': {
        'type_id': 'TIME_ORDIN',
        'description': 'baleia branca',
        'notes': 'Hey stupid prank...',
        'dimentions': {'movie': 'not a movie'},
        'extra_info': {'var_1': 'c',  'var_2': 'b'},
    }
}


class TestGeoAttributeResolver(TestPumpWood):
    """Test GeoAttribute resolvers."""

    load_balancer_address = 'http://localhost:8080/'
    apps_to_regenerate = ['pumpwood-datalake-app']

    # session = requests.session()
    # session.headers.update(
    #     {'Authorization': '6cab4ba54bb8f92af7e135d7ad8c7d70d4555f7f'})

    def test_new_geoattributes(self):
        """Test adding new geoattributes with resolver."""
        microservice = PumpWoodMicroService(**TEST_CONFIG['MICROSERVICE'])
        microservice.login()

        queue_data = microservice.list_one(
            model_class='DatabaseETLQueue', pk=4)
        job_data = microservice.list_one(
            model_class='DatabaseETLJob', pk=queue_data['job_id'])

        att_resolver = GeoAttributeResolver(
            microservice=microservice, queue_data=queue_data,
            job_data=job_data, add_new_dimention=True,
            add_new_categorical='add', add_hierarchy='replace')
        new_atts = att_resolver.resolve_dimentions(
            dim_dict=new_geoattributes)

        for key, item in new_atts.items():
            item.pop('model_class', None)
            item.pop('pk', None)
            item.pop('last_update_at', None)
            item.pop('created_by_id', None)

            item['extra_info'].pop('pusher_job_data', None)
            item['extra_info'].pop('pusher_queue_data', None)
            self.assertTrue(is_dict_equal(item, new_geoattributes[key]))

    def test_new_update_geoattributes(self):
        """Check update of geoattributes."""
        microservice = PumpWoodMicroService(**TEST_CONFIG['MICROSERVICE'])
        microservice.login()

        queue_data = microservice.list_one(
            model_class='DatabaseETLQueue', pk=4)
        job_data = microservice.list_one(
            model_class='DatabaseETLJob', pk=queue_data['job_id'])

        atts_server = microservice.list('DescriptionGeoAttribute',
                                        order_by=['id'])[:4]
        old_geoattributes = dict(
            [(att['description'], copy.deepcopy(att)) for att in atts_server])

        atts_server[0]['notes'] = 'Eita lelele'
        atts_server[1]['extra_info'] = {'chubaca': 'eh legal'}
        atts_server[2]['dimentions'] = {'upa lele': 'a waka waka'}
        atts_server[3]['type_id'] = 'TIME_ORDIN'

        update_geoattributes = dict(
            [(att['description'], copy.deepcopy(att)) for att in atts_server])

        att_resolver = GeoAttributeResolver(
            microservice=microservice, queue_data=queue_data,
            job_data=job_data, add_new_dimention=True,
            add_new_categorical='add', add_hierarchy='replace')
        resolved_atts = att_resolver.resolve_dimentions(
            dim_dict=update_geoattributes)

        for key in resolved_atts.keys():
            resolved_att = resolved_atts[key]
            old_att = old_geoattributes[key]
            update_att = update_geoattributes[key]

            self.assertEqual(
                resolved_att['description'], update_att['description'])
            self.assertEqual(
                resolved_att['notes'], update_att['notes'])
            self.assertEqual(
                resolved_att['type_id'], update_att['type_id'])

            old_extra_info = old_att['extra_info'] or {}
            update_extra_info = update_att['extra_info'] or {}
            resolved_extra_info = resolved_att['extra_info'] or {}
            old_extra_info.update(update_extra_info)
            resolved_extra_info.pop('pusher_job_data')
            resolved_extra_info.pop('pusher_queue_data')
            self.assertTrue(is_dict_equal(resolved_extra_info, old_extra_info))

            old_dimentions = old_att['dimentions'] or {}
            update_dimentions = update_att['dimentions'] or {}
            resolved_dimentions = resolved_att['dimentions'] or {}
            old_dimentions.update(update_dimentions)
            self.assertTrue(is_dict_equal(resolved_dimentions, old_dimentions))

    def test_no_change_geoattributes(self):
        """Check when no changes are done on geoattributes."""
        microservice = PumpWoodMicroService(**TEST_CONFIG['MICROSERVICE'])
        microservice.login()

        queue_data = microservice.list_one(
            model_class='DatabaseETLQueue', pk=4)
        job_data = microservice.list_one(
            model_class='DatabaseETLJob', pk=queue_data['job_id'])

        atts_server = microservice.list('DescriptionGeoAttribute',
                                        order_by=['id'])[:4]
        old_geoattributes = dict(
            [(att['description'], copy.deepcopy(att)) for att in atts_server])

        update_geoattributes = dict(
            [(att['description'], copy.deepcopy(att)) for att in atts_server])

        att_resolver = GeoAttributeResolver(
            microservice=microservice, queue_data=queue_data,
            job_data=job_data, add_new_dimention=True,
            add_new_categorical='add', add_hierarchy='replace')
        resolved_atts = att_resolver.resolve_dimentions(
            dim_dict=update_geoattributes)

        for key in resolved_atts.keys():
            resolved_att = resolved_atts[key]
            old_att = old_geoattributes[key]
            update_att = update_geoattributes[key]

            self.assertEqual(
                resolved_att['description'], update_att['description'])
            self.assertEqual(
                resolved_att['notes'], update_att['notes'])
            self.assertEqual(
                resolved_att['type_id'], update_att['type_id'])

            old_extra_info = old_att['extra_info'] or {}
            resolved_extra_info = resolved_att['extra_info'] or {}
            self.assertTrue(is_dict_equal(resolved_extra_info, old_extra_info))

            old_dimentions = old_att['dimentions'] or {}
            resolved_dimentions = resolved_att['dimentions'] or {}
            self.assertTrue(is_dict_equal(resolved_dimentions, old_dimentions))

    def test_error_on_new(self):
        """Check if when add_new_dimention=False raises error when add att."""
        microservice = PumpWoodMicroService(**TEST_CONFIG['MICROSERVICE'])
        microservice.login()

        queue_data = microservice.list_one(
            model_class='DatabaseETLQueue', pk=4)
        job_data = microservice.list_one(
            model_class='DatabaseETLJob', pk=queue_data['job_id'])

        att_resolver = GeoAttributeResolver(
            microservice=microservice, queue_data=queue_data,
            job_data=job_data, add_new_dimention=False,
            add_new_categorical='add', add_hierarchy='replace')

        with self.assertRaises(Exception):
            att_resolver.resolve_dimentions(dim_dict=new_geoattributes)

    def test_error_on_update(self):
        """Check if when add_new_dimention=False raises when update att."""
        microservice = PumpWoodMicroService(**TEST_CONFIG['MICROSERVICE'])
        microservice.login()

        queue_data = microservice.list_one(
            model_class='DatabaseETLQueue', pk=4)
        job_data = microservice.list_one(
            model_class='DatabaseETLJob', pk=queue_data['job_id'])

        atts_server = microservice.list('DescriptionGeoAttribute',
                                        order_by=['id'])[:4]

        atts_server[0]['notes'] = 'Eita lelele'
        atts_server[1]['extra_info'] = {'chubaca': 'eh legal'}
        atts_server[2]['dimentions'] = {'upa lele': 'a waka waka'}
        atts_server[3]['type_id'] = 'TIME_ORDIN'

        update_geoattributes = dict(
            [(att['description'], copy.deepcopy(att)) for att in atts_server])

        att_resolver = GeoAttributeResolver(
            microservice=microservice, queue_data=queue_data,
            job_data=job_data, add_new_dimention=False,
            add_new_categorical='add', add_hierarchy='replace')

        with self.assertRaises(Exception):
            att_resolver.resolve_dimentions(dim_dict=update_geoattributes)
