"""Module to test attribute resolvers."""
# -*- coding: utf-8 -*-
# import requests
# import copy
import pandas as pd
from pumpwood_flaskmisc.testing import TestPumpWood, is_dict_equal
from pusher_resolvers.resolvers import AttributeResolver
from pumpwood_communication.microservices import PumpWoodMicroService
from tests.config.settings import TEST_CONFIG

new_attributes = pd.DataFrame([
    {
        'type_id': 'FIXED_CATEG',
        'description': 'chubaca',
        'notes': 'Ahhhhhhh!!!',
        'dimentions': {'movie': 'star_wars'},
        'extra_info': {'var_1': 'a',  'var_2': 'd'},
    }, {
        'type_id': 'FIXED_CATEG',
        'description': 'chopolin colorado!',
        'notes': 'Quem contava com a minha astúcia?',
        'dimentions': {'movie': 'chopolin colorado'},
        'extra_info': {'var_1': 'b',  'var_2': 'c'},
    }, {
        'type_id': 'TIME_ORDIN',
        'description': 'baleia branca',
        'notes': 'Hey stupid prank...',
        'dimentions': {'movie': 'not a movie'},
        'extra_info': {'var_1': 'c',  'var_2': 'b'},
    }])


update_attributes = pd.DataFrame([
    {
        # Change type
        "type_id": "TIME_ORDIN",
        "description": "Categoria vazia.",
        "notes": "Variável para testar inserção pelo crawler.",
        "dimentions": {"vazia": True},
        "extra_info": {"teste": "chubaca"}
    }, {
        "type_id": "FIXED_CATEG",
        "description": "Tipos de ação.",
        "notes": "Variável para testar inserção pelo crawler, colocando tipos aleatórios de ações.",
        "dimentions": {"acoes": "bovespa"},
        "extra_info": {"teste": "qq coisa"}
    }, {
        # Change notes
        "type_id": "TIME_ORDIN",
        "description": "Sentimento frente a empresa",
        "notes": "Chubacaaaaa",
        "dimentions": {"tipo": "sentimento"},
        "extra_info": {"teste": "alguma coisa"},
    }, {
        "type_id": "FIXED_CATEG",
        "description": "Variavel FIXED_CATEG",
        "notes": "1, 2, 3, 1000",
        "dimentions": {},
        "extra_info": {"e_pouco": False}
    }, {
        # Notes local in server
        "type_id": "FIXED_ORDIN",
        "description": "Variavel FIXED_ORDIN",
        "notes": "Variavel FIXED_ORDIN",
        "dimentions": {},
        "extra_info": {"e_pouco": False}
    }, {
        # Change dimention
        "type_id": "FIXED_DISCR",
        "description": "Variavel FIXED_DISCR",
        "notes": "Variavel FIXED_DISCR: Um gato",
        "dimentions": {"teste": 1},
        "extra_info": {"e_pouco": True}
    }, {
        # Change extra info
        "type_id": "FIXED_CONTI",
        "description": "Variavel FIXED_CONTI",
        "notes": "Variavel FIXED_CONTI: Um pato",
        "dimentions": {},
        "extra_info": {"e_pouco": True}
    }, ])


class TestAddAttributeDimentionResolver(TestPumpWood):
    """Test Attribute resolvers."""

    load_balancer_address = 'http://localhost:8080/'
    apps_to_regenerate = ['pumpwood-datalake-app']

    # session = requests.session()
    # session.headers.update(
    #     {'Authorization': '6cab4ba54bb8f92af7e135d7ad8c7d70d4555f7f'})

    def test_new_attributes(self):
        """Test adding new attributes with resolver."""
        microservice = PumpWoodMicroService(**TEST_CONFIG['MICROSERVICE'])
        microservice.login()

        queue_data = microservice.list_one(
            model_class='DatabaseETLQueue', pk=4)
        job_data = microservice.list_one(
            model_class='DatabaseETLJob', pk=queue_data['job_id'])

        att_resolver = AttributeResolver(
            microservice=microservice, queue_data=queue_data,
            job_data=job_data, add_new_dimention=True,
            add_new_categorical='add', add_hierarchy='replace')
        new_atts = att_resolver.resolve_dimentions(
            dimention_table=new_attributes.copy())

        unique_desc = new_attributes["description"].unique().tolist()
        server_dimentions = pd.DataFrame(microservice.list_without_pag(
            "DescriptionAttribute",
            filter_dict={"description__in": unique_desc}))
        merged = server_dimentions.merge(new_atts, on=[
            "description", "type_id", "notes"])
        self.assertEqual(len(merged), len(new_atts))

    def test_new_update_attributes(self):
        """Check update of attributes."""
        microservice = PumpWoodMicroService(**TEST_CONFIG['MICROSERVICE'])
        microservice.login()

        queue_data = microservice.list_one(
            model_class='DatabaseETLQueue', pk=4)
        job_data = microservice.list_one(
            model_class='DatabaseETLJob', pk=queue_data['job_id'])

        att_resolver = AttributeResolver(
            microservice=microservice, queue_data=queue_data,
            job_data=job_data, add_new_dimention=True,
            add_new_categorical='add', add_hierarchy='replace')
        resolved_atts = att_resolver.resolve_dimentions(
            dimention_table=update_attributes.copy())

        for index, row in resolved_atts.iterrows():
            row['extra_info'].pop('pusher_queue_data', None)
            row['extra_info'].pop('pusher_job_data', None)

        merged = resolved_atts.merge(
            update_attributes, on=["description", "notes", "type_id"],
            suffixes=["__local", "__server"])
        self.assertTrue(len(merged), len(update_attributes) - 1)

    def test_no_change_attributes(self):
        """Check when no changes are done on attributes."""
        microservice = PumpWoodMicroService(**TEST_CONFIG['MICROSERVICE'])
        microservice.login()

        queue_data = microservice.list_one(
            model_class='DatabaseETLQueue', pk=4)
        job_data = microservice.list_one(
            model_class='DatabaseETLJob', pk=queue_data['job_id'])

        atts_server = microservice.list_without_pag(
            'DescriptionAttribute', filter_dict={"pk__in": [1, 2, 3, 4]})
        df_atts_server = pd.DataFrame(atts_server, columns=[
            "type_id", "description", "notes", "dimentions", "extra_info"])

        att_resolver = AttributeResolver(
            microservice=microservice, queue_data=queue_data,
            job_data=job_data, add_new_dimention=True,
            add_new_categorical='add', add_hierarchy='replace')
        resolved_atts = att_resolver.resolve_dimentions(
            dimention_table=df_atts_server)

        merged = resolved_atts.merge(
            df_atts_server, on=["description", "notes", "type_id"],
            suffixes=["__local", "__server"])
        self.assertEqual(len(merged), len(df_atts_server))

        iqual_extra_info = merged[
            ['extra_info__local', 'extra_info__server']].apply(
                lambda row: is_dict_equal(
                    row['extra_info__local'], row['extra_info__server']),
                axis=1)
        self.assertTrue(iqual_extra_info.all())

        iqual_dimentions = merged[
            ['dimentions__local', 'dimentions__server']].apply(
                lambda row: is_dict_equal(
                    row['dimentions__local'], row['dimentions__server']),
                axis=1)
        self.assertTrue(iqual_dimentions.all())


class TestFailAttributeDimentionResolver(TestPumpWood):
    """Test Attribute resolvers."""

    load_balancer_address = 'http://localhost:8080/'
    apps_to_regenerate = ['pumpwood-datalake-app']

    # session = requests.session()
    # session.headers.update(
    #     {'Authorization': '6cab4ba54bb8f92af7e135d7ad8c7d70d4555f7f'})

    def test_new_attributes(self):
        """Test fail new attributes with resolver: add_new_dimention=False."""
        microservice = PumpWoodMicroService(**TEST_CONFIG['MICROSERVICE'])
        microservice.login()

        queue_data = microservice.list_one(
            model_class='DatabaseETLQueue', pk=4)
        job_data = microservice.list_one(
            model_class='DatabaseETLJob', pk=queue_data['job_id'])

        att_resolver = AttributeResolver(
            microservice=microservice, queue_data=queue_data,
            job_data=job_data, add_new_dimention=False,
            add_new_categorical='add', add_hierarchy='replace')
        with self.assertRaises(Exception):
            att_resolver.resolve_dimentions(
                dimention_table=new_attributes.copy())

    def test_new_update_attributes(self):
        """Fail update of attributes: add_new_dimention=False."""
        microservice = PumpWoodMicroService(**TEST_CONFIG['MICROSERVICE'])
        microservice.login()

        queue_data = microservice.list_one(
            model_class='DatabaseETLQueue', pk=4)
        job_data = microservice.list_one(
            model_class='DatabaseETLJob', pk=queue_data['job_id'])

        att_resolver = AttributeResolver(
            microservice=microservice, queue_data=queue_data,
            job_data=job_data, add_new_dimention=False,
            add_new_categorical='add', add_hierarchy='replace')
        with self.assertRaises(Exception):
            att_resolver.resolve_dimentions(
                dimention_table=new_attributes.copy())

    def test_no_change_attributes(self):
        """No changes are done on attributes; add_new_dimention=False."""
        microservice = PumpWoodMicroService(**TEST_CONFIG['MICROSERVICE'])
        microservice.login()

        queue_data = microservice.list_one(
            model_class='DatabaseETLQueue', pk=4)
        job_data = microservice.list_one(
            model_class='DatabaseETLJob', pk=queue_data['job_id'])

        atts_server = microservice.list_without_pag(
            'DescriptionAttribute', filter_dict={"pk__in": [1, 2, 3, 4]})
        df_atts_server = pd.DataFrame(atts_server, columns=[
            "type_id", "description", "notes", "dimentions", "extra_info"])

        att_resolver = AttributeResolver(
            microservice=microservice, queue_data=queue_data,
            job_data=job_data, add_new_dimention=False,
            add_new_categorical='add', add_hierarchy='replace')
        resolved_atts = att_resolver.resolve_dimentions(
            dimention_table=df_atts_server)

        merged = resolved_atts.merge(
            df_atts_server, on=["description", "notes", "type_id"],
            suffixes=["__local", "__server"])
        self.assertEqual(len(merged), len(df_atts_server))

        iqual_extra_info = merged[
            ['extra_info__local', 'extra_info__server']].apply(
                lambda row: is_dict_equal(
                    row['extra_info__local'], row['extra_info__server']),
                axis=1)
        self.assertTrue(iqual_extra_info.all())

        iqual_dimentions = merged[
            ['dimentions__local', 'dimentions__server']].apply(
                lambda row: is_dict_equal(
                    row['dimentions__local'], row['dimentions__server']),
                axis=1)
        self.assertTrue(iqual_dimentions.all())
