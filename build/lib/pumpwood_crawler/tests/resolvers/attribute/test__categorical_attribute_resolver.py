# -*- coding: utf-8 -*-
"""Module to test attribute resolvers."""
# import requests
# import copy
import pandas as pd
from pumpwood_flaskmisc.testing import TestPumpWood
from pusher_resolvers.resolvers import AttributeResolver
from pumpwood_communication.microservices import PumpWoodMicroService
from tests.config.settings import TEST_CONFIG


new_categories = pd.DataFrame([
    {'attribute': "Tipos de ação.",
     'value': 3, "ordering": 3,
     'description': 'Ação que é uma blz',
     'extra_info': {"chubaca chcuhu": True}},
    {'attribute': "Tipos de ação.",
     'value': 4, "ordering": 4,
     'description': 'Ação que é um chuchu',
     'extra_info': {"chubaca chcuhu": 10}},
    {'attribute': "Sentimento frente a empresa",
     'value': 6, "ordering": 6,
     'description': 'Um chuchu',
     'extra_info': {"chubaca não chuchu": 1}},
    {'attribute': "Sentimento frente a empresa",
     'value': 7, "ordering": 7,
     'description': 'Que blz meu',
     'extra_info': {"chubaca não chuchu": 2}},
    {'attribute': "Sentimento frente a empresa",
     'value': 8, "ordering": 8,
     'description': 'Realmente uma gracinha',
     'extra_info': {"chubaca não chuchu": 3}},
    {'attribute': "Sentimento frente a empresa",
     'value': 9, "ordering": 9,
     'description': 'Melhor que queijo',
     'extra_info': {"chubaca não chuchu": 4}}, ])

update_categories = pd.DataFrame([
    # Swipe
    {'attribute': "Sentimento frente a empresa",
     'value': 6, "ordering": 1,
     'description': 'Péssimo',
     'extra_info': {"chubaca não chuchu": 1}},
    {'attribute': "Sentimento frente a empresa",
     'value': 1,  "ordering": 2,
     'description': 'Muito ruim',
     'extra_info': {"chubaca não chuchu": 2}},
    {'attribute': "Sentimento frente a empresa",
     'value': 2,  "ordering": 3,
     'description': 'Ruim',
     'extra_info': {"chubaca não chuchu": 3}},
    {'attribute': "Sentimento frente a empresa",
     'value': 3,  "ordering": 4,
     'description': 'Médio',
     'extra_info': {"chubaca não chuchu": 4}},
    {'attribute': "Sentimento frente a empresa",
     'value': 4,  "ordering": 5,
     'description': 'Bom',
     'extra_info': {"chubaca não chuchu": 4}},
    {'attribute': "Sentimento frente a empresa",
     'value': 5,  "ordering": 6,
     'description': 'Muito bom',
     'extra_info': {"chubaca não chuchu": 4}},
    {'attribute': "Sentimento frente a empresa",
     'value': 7,  "ordering": 7,
     'description': 'Ótimo',
     'extra_info': {"chubaca não chuchu": 4}},
    # Change description
    {'attribute': "Tipos de ação.",
     'value': 1,  "ordering": 1,
     'description': 'Ações que não valem nada',
     'extra_info': {"chubaca não chuchu": "testando 123"}}, ])


class TestCatericalAttributeResolver(TestPumpWood):
    """Test Hierarchy Attribute resolvers."""

    load_balancer_address = 'http://localhost:8080/'
    apps_to_regenerate = ['pumpwood-datalake-app']

    # session = requests.session()
    # session.headers.update(
    #     {'Authorization': '6cab4ba54bb8f92af7e135d7ad8c7d70d4555f7f'})
    def test_add_category(self):
        """Test adding new categories with resolver."""
        microservice = PumpWoodMicroService(**TEST_CONFIG['MICROSERVICE'])
        microservice.login()

        queue_data = microservice.list_one(
            model_class='DatabaseETLQueue', pk=4)
        job_data = microservice.list_one(
            model_class='DatabaseETLJob', pk=queue_data['job_id'])

        att_resolver = AttributeResolver(
            microservice=microservice, queue_data=queue_data,
            job_data=job_data, add_new_dimention=True,
            add_new_categorical='add', add_hierarchy='add')
        result_resolver = att_resolver.resolve_categories(
            categories=new_categories.copy())

        descriptions = microservice.list_without_pag(
            model_class="DescriptionAttribute",
            filter_dict={"description__in": new_categories[
                'attribute'].unique().tolist()})

        desc_pk = dict([
            (x['description'], x['pk']) for x in descriptions])
        temp_new_categories = new_categories.copy()
        temp_new_categories['attribute_id'] = temp_new_categories[
            'attribute'].map(desc_pk)
        temp_new_categories = temp_new_categories[[
            'ordering', 'value', 'attribute_id', 'description']]
        merge_result = result_resolver.merge(temp_new_categories)
        self.assertEqual(len(merge_result), len(new_categories))

    def test_fail_add_category(self):
        """Test faling when adding new categories with resolver."""
        microservice = PumpWoodMicroService(**TEST_CONFIG['MICROSERVICE'])
        microservice.login()

        queue_data = microservice.list_one(
            model_class='DatabaseETLQueue', pk=4)
        job_data = microservice.list_one(
            model_class='DatabaseETLJob', pk=queue_data['job_id'])

        att_resolver = AttributeResolver(
            microservice=microservice, queue_data=queue_data,
            job_data=job_data, add_new_dimention=True,
            add_new_categorical='fail', add_hierarchy='add')

        with self.assertRaises(Exception):
            att_resolver.resolve_categories(
                categories=new_categories.copy())

    def test_update_category(self):
        """Test updating new categories with resolver."""
        microservice = PumpWoodMicroService(**TEST_CONFIG['MICROSERVICE'])
        microservice.login()

        queue_data = microservice.list_one(
            model_class='DatabaseETLQueue', pk=4)
        job_data = microservice.list_one(
            model_class='DatabaseETLJob', pk=queue_data['job_id'])

        att_resolver = AttributeResolver(
            microservice=microservice, queue_data=queue_data,
            job_data=job_data, add_new_dimention=True,
            add_new_categorical='add', add_hierarchy='add')
        result_resolver = att_resolver.resolve_categories(
            categories=update_categories.copy())

        descriptions = microservice.list_without_pag(
            model_class="DescriptionAttribute",
            filter_dict={"description__in": update_categories[
                'attribute'].unique().tolist()})

        desc_pk = dict([
            (x['description'], x['pk']) for x in descriptions])
        temp_update_categories = update_categories.copy()
        temp_update_categories['attribute_id'] = temp_update_categories[
            'attribute'].map(desc_pk)
        temp_update_categories = temp_update_categories[[
            'ordering', 'value', 'attribute_id', 'description']]
        merge_result = result_resolver.merge(temp_update_categories)
        self.assertEqual(len(merge_result), len(update_categories))

    def test_fail_update_category(self):
        """Test fail when updating new categories with resolver."""
        microservice = PumpWoodMicroService(**TEST_CONFIG['MICROSERVICE'])
        microservice.login()

        queue_data = microservice.list_one(
            model_class='DatabaseETLQueue', pk=4)
        job_data = microservice.list_one(
            model_class='DatabaseETLJob', pk=queue_data['job_id'])

        att_resolver = AttributeResolver(
            microservice=microservice, queue_data=queue_data,
            job_data=job_data, add_new_dimention=True,
            add_new_categorical='fail', add_hierarchy='add')

        with self.assertRaises(Exception):
            att_resolver.resolve_categories(
                categories=update_categories.copy())
