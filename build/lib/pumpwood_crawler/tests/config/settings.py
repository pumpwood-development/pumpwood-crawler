# -*- coding: utf-8 -*-
import os

TEST_CONFIG = {
    'DATABASE': {
        'dialect': os.environ.get('DB_DIALECT'),
        'driver': os.environ.get('DB_DRIVER'),
        'username': os.environ.get('DB_USERNAME'),
        'password': os.environ.get('DB_PASSWORD'),
        'host': os.environ.get('DB_HOST'),
        'port': os.environ.get('DB_PORT'),
        'database': os.environ.get('DB_DATABASE'),
    },
    'STORAGE': {
        'storage_type': 'local_bucket',
        'folder_path': 'tests/media/'
    },
    'MICROSERVICE': {
        'name': os.environ.get('MICROSERVICE_NAME'),
        'server_url': os.environ.get('MICROSERVICE_URL'),
        'username': os.environ.get('MICROSERVICE_USERNAME'),
        'password': os.environ.get('MICROSERVICE_PASSWORD'),
        'verify_ssl': os.environ.get('MICROSERVICE_SSL', 'False') == 'True'
    }
}
