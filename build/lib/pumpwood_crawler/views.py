"""View to crawler."""
import simplejson as json
from flask import request
from werkzeug.utils import secure_filename
from datetime import datetime
from slugify import slugify

from pumpwoodflask_views.views import PumpWoodFlaskView
from pumpwood_communication import exceptions

from settings.settings import storage_object
from abc import ABC


class PullerJobView(PumpWoodFlaskView, ABC):
    """Pumpwood views for Puller Job."""

    model_class = None
    list_serializer = None
    retrieve_serializer = None


class PullerQueueView(PumpWoodFlaskView, ABC):
    """Pumpwood views for Puller Queue."""

    model_class = None
    list_serializer = None
    retrieve_serializer = None

    def save(self, data):
        """Save object."""
        saved_queue = super().save(data=data)
        if saved_queue["status"] == "finish_pulling":
            obj = self.model_class.query.get(saved_queue["pk"])
            obj.run_binded_pushers()
        return saved_queue


class PusherJobView(PumpWoodFlaskView, ABC):
    """Pumpwood views for Pusher Job."""

    model_class = None
    list_serializer = None
    retrieve_serializer = None
    file_fields = {
        "etl_file": ["py"]}


class PusherQueueView(PumpWoodFlaskView, ABC):
    """Pumpwood views for Pusher Queue."""

    model_class = None
    list_serializer = None
    retrieve_serializer = None
