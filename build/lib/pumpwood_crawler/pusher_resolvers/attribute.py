"""Resolver module for attibutes."""
import pandas as pd


class AttributeResolver:
    """Attribute resolver for attribute variables."""

    def __init__(self, sql_alchemy_engine, storage, microservice,
                 add_new, add_new_categorical,
                 add_hierarchy):
        """
        __init__.

        Args:
            sql_alchemy_engine (sqlalchemy.engine): Engine of connection to
                                                   database.
            microservice (pumpwood microservice): Microservice object for
                                                 Pumpwood.
            storage (pumpwood storage): Storage object for PumpWood.
            add_new (bool): If resolver can add new attributes.
            add_new_categorical (bool): If resolver can add
                                                 categories.
            add_hierarchy (bool): If resolver can add hierarchy to
                                           attributes.
        """
        self.sql_alchemy_engine = sql_alchemy_engine
        self.storage = storage
        self.microservice = microservice
        self.add_new = add_new
        self.add_new_categorical = add_new_categorical
        self.add_hierarchy = add_hierarchy

        self.cacheds = None
        self.cached_categories = None

    def resolve_dimentions(self, att_dict: dict, queue_data: dict,
                           job_data: dict):
        """
        Resolve attribute dimentions: add a new or fetch from PumpWood.

        Resolve attribute, get its pks by the descriptions
        and if the class attribute add_new_attribute is set
        True, it will create a new attribute with the dictionary
        key value.

        Args:
            att_dict (dict): Dictionary with the attribute caracteristics
                ('description', 'notes', 'extra_field'). The key is the name
                used in results at data key of the process_message result.
            queue_data (dict): Serialized queue object.
            job_data (dict): Serialized job object.

        Kwargs:
            No Kwargs.

        Returns:
            dict: A dictionary with same keys with the pk at server.

        Raises:
            Exception( 'Error when creating new attribute:--Payload:
            %s--Msg:%s'):
                If the is an error creating a new attribute

            Exception('Pusher not adding new attribute and some
            descriptions not found at server:%s'):
                If the pusher is not allowed to create new attributes and
                sobre descriptions are not at the server.

        Example:
            test = {
                'att_1': {'description': 'Att 1', 'notes': 'Notes Att 1'
                        , 'extra_fields': {'att_1': True}}
              , 'att_2': {'description': 'Att 2', 'notes': 'Notes Att 2'
                        , 'extra_fields': {'att_1': False}}
              , 'mu_3': {'description': 'Att 3', 'notes': 'Notes Att 3'
                        , 'extra_fields': {}}
            }
            pusher_object.resolve_attribute(test)

        """
        if len(att_dict) == 0:
            return {}

        att_descriptions = [x[1]['description'] for x in att_dict.items()]
        server_attributes = self._microservice.list_without_pag(
            'DescriptionAttribute', {'description__in': att_descriptions})

        dict_server_attributes = {}
        for x in server_attributes:
            extra_info = x.get('extra_info', {})
            extra_info.pop('push_queue_id', None)
            extra_info.pop('push_job_id', None)
            extra_info.pop('push_job_name', None)
            x['extra_info'] = extra_info
            dict_server_attributes[x['description']] = x

        on_server = {}
        erro_descriptions = []
        for k, x in att_dict.items():
            server_data = dict_server_attributes.get(x['description'])
            if server_data is not None:
                if self.add_new_attribute:
                    if x['notes'] != server_data['notes'] or\
                            x['extra_info'] != server_data['extra_info']:
                        x['pk'] = server_data['pk']
                        x['model_class'] = server_data['model_class']
                        x['extra_info'] = self.add_queue_and_job_info(
                            queue_data=queue_data, job_data=job_data,
                            extra_info=x.get('extra_info', {}))

                        server_data = self._microservice.save(obj_dict=x)
                on_server[k] = server_data
            else:
                if self.add_new_attribute:
                    try:
                        x['model_class'] = 'DescriptionAttribute'
                        x['extra_info'] = self.add_queue_and_job_info(
                            queue_data=queue_data, job_data=job_data,
                            extra_info=x.get('extra_info', {}))
                        server_data = self._microservice.save(obj_dict=x)
                        on_server[k] = server_data
                    except Exception as err:
                        template = 'Error when creating new attribute:\n--' + \
                            'Payload:\n%s\n--Msg:%s'
                        raise Exception(template % (str(x), str(err)))
                else:
                    erro_descriptions.append(x['description'])

        if len(erro_descriptions) != 0:
            template = 'Pusher not adding new attributes and some ' + \
                'descriptions not found at server:\n%s'
            raise Exception(template % (str(erro_descriptions), ))

        self.cached_attributes = on_server
        return self.cached_attributes

    def resolve_hierarchy(self, hierarchy_table: pd.DataFrame):
        """
        Resolve attribute hierarchy: If not equal substitute.

        Args:
            hierarchy_table (pd.DataFrame): Dataframe with the colums
                node_id, child_node_id, extra_info setting hierarchy for
                node_id.

        Kwargs:
            No Kwargs.

        Returns:
            pd.DataFrame: With saved hierarchies.

        Raises:
            Exception('Pusher not adding new hierarchy and hierarchy_table with
                    len !=0 '):
                If the pusher is not allowed to create new hierarchy and
                hierarchy_table with len != 0.

        Example:
            No Example

        """
        pass

    def resolve_categories(self):
        """
        Resolve attribute categories for categorical data.

        Args:
            hierarchy_table (pd.DataFrame): Dataframe with the colums
                node_id, child_node_id, extra_info setting hierarchy for
                node_id.

        Kwargs:
            No Kwargs.

        Returns:
            pd.DataFrame: With saved hierarchies.

        Raises:
            Exception('Pusher not adding new hierarchy and hierarchy_table with
                    len !=0 '):
                If the pusher is not allowed to create new hierarchy and
                hierarchy_table with len != 0.

        Example:
            No Example

        """
        pass
