"""Puller module."""
import datetime
import pandas as pd
# import itertools
# import numpy as np
from pumpwood_flaskmisc.database import build_engine_string
from pumpwood_flaskmisc.storage import PumpWoodStorage
from pumpwood_communication.microservices import PumpWoodMicroService
# from pumpwood_flaskmisc.pandas import break_in_chunks

from sqlalchemy import create_engine


class PullerClass:
    """Base class to create a puller worker object."""

    queue_model_class = None
    '''Queue model_class name'''
    job_model_class = None
    '''Job model_class name'''

    def __init__(self, database_config, microservice_config,
                 storage_config, **kwargs):
        """
        Create a default PullerClass object.

        Args:
          database_config (dict): Dictionary containing database config.
          microservice_config (dict): Dictionary containing microservice
                                      config.
          storage_config(dict): Dictionary containing storage_config
                                     config.
        Kwargs:
          No extra fields.
        Raises:
          No especific raises.
        Example:
          No example yet.

        """
        database_url = build_engine_string(**database_config)
        self._sql_alchemy_engine = create_engine(database_url)
        self._storage = PumpWoodStorage(**storage_config)
        self._microservice = PumpWoodMicroService(**microservice_config)
        self._microservice.login()

    def get_queue(self, pk):
        """
        Retrieve queue data for the crawler.

        Args:
          pk (int): Pk of the queue to be retrieved

        Kwargs:
          No Kwargs

        Returns:
          dict: Serialized queue object.

        Raises:
          No especific raises.

        Example:
          queue_obj = pusher_class_obj.get_queue(pk=10)

        """
        return self._microservice.retrieve(
            model_class=self.queue_model_class, pk=pk)

    def get_job(self, pk):
        """
        Retrieve job data for the crawler.

        Args:
          pk (int): Pk of the job to be retrieved

        Kwargs:
          No Kwargs

        Returns:
          dict: Serialized job object.

        Raises:
          No especific raises.

        Example:
          job_obj = pusher_class_obj.get_job(pk=10)

        """
        return self._microservice.retrieve(
            model_class=self.job_model_class, pk=pk)

    def process_message(self, queue_data: dict):
        """
        Process queue message and download data.

        Args:
            msg_data(dict): Serialized queue data obejct.

        Kwargs:
            No kwargs

        Returns:
          dict: Data to be saved dictonary

        Raises:
          No especific raises.

        Example:
          No example yet.

        """
        queue_data = self.get_queue(pk=queue_data["pk"])
        if queue_data['status'] != 'idle':
            raise Exception('Queue is not idle')

        print('!!!!pulling queue: %s' % (queue_data['pk'], ))
        queue_data['status'] = 'pulling'
        queue_data['start_time'] = datetime.datetime.utcnow().isoformat()
        self._microservice.save(obj_dict=queue_data)

        job_data = None
        pulled_data = None
        try:
            job_data = self.get_job(pk=queue_data["job_id"])
            pulled_data = self.pull_data(queue_data=queue_data,
                                         job_data=job_data)
        except Exception as e:
            queue_data['status'] = 'error_pulling'
            queue_data['results'].update({'error_pulling': str(e)})
            queue_data['end_time'] = datetime.datetime.utcnow().isoformat()
            self._microservice.save(obj_dict=queue_data)
            raise e

        print('!!!!saving queue: %s' % (queue_data['pk'], ))
        queue_data['status'] = 'saving'
        self._microservice.save(obj_dict=queue_data)

        save_result = None
        try:
            save_result = self.save_pulled_data(
                pulled_data=pulled_data, queue_data=queue_data,
                job_data=job_data)
        except Exception as e:
            queue_data['status'] = 'error_saving'
            queue_data['results'].update({'error_saving': str(e)})
            queue_data['end_time'] = datetime.datetime.utcnow().isoformat()
            self._microservice.save(obj_dict=queue_data)
            raise e

        queue_data['results'].update(save_result)
        queue_data['status'] = 'finish_pulling'
        queue_data['end_time'] = datetime.datetime.utcnow().isoformat()
        resp = self._microservice.save(obj_dict=queue_data)

        print('!!!!finish queue: %s' % (queue_data['pk'], ))
        return resp

    def pull_data(self, queue_data: dict, job_data: dict) -> dict:
        """
        Pull data from source.

        This function must be implemented for each puller.

        Args:
            queue_data(dict): queue serialized object.
            job_data(dict): job serialized object.
        Return:
            dict: Return a dict of dataframes to be saved at the datapond.
        """
        raise Exception('pull_data not implemented')

    def save_pulled_data(self, pulled_data: dict, queue_data: dict,
                         job_data: dict):
        """
        Save pulled data at data pond.

        This function must be implemented for each puller.

        Args:
            pulled_data(dict): pulled data result.
            queue_data(dict): queue serialized object.
            job_data(dict): job serialized object.
        Return:
            dict: Return a dict of dataframes to be saved at the datapond.
        """
        raise Exception('pull_data not implemented')
