from marshmallow import (
    Schema, pre_load, pre_dump, post_load, validates_schema,
    validates, fields, ValidationError
)

from pumpwoodflask_views import serializers
from pumpwoodflask_views.auth import AuthFactory


######
# List
######
class SerializerPullerJob(serializers.PumpWoodSerializer):
    """Base serialization for PullerJob."""
    created_by_id = fields.Integer(dump_only=True)
    created_at = fields.DateTime(dump_only=True)
    last_on_queue = fields.DateTime(dump_only=True)
    model = None

    class Meta:
        """Meta."""
        fields = [
            "pk", "model_class", "name", "last_on_queue", "default_parameters",
            "default_files", "created_at", "created_by_id", "notes",
            "binded_pusher_job_set", "queue_set"]
        dump_only = ["created_by_id", "default_files"]

    @post_load(pass_many=False)
    def get_created_by_id(self, data):
        """Get logged user for serializer."""
        current_user = AuthFactory.retrieve_authenticated_user()
        data['created_by_id'] = current_user['pk']
        return data


class SerializerPullerQueue(serializers.PumpWoodSerializer):
    """Base serialization for PullerQueue."""
    start_time = fields.DateTime(allow_none=True)
    end_time = fields.DateTime(allow_none=True)
    created_by_id = fields.Integer(dump_only=True)
    created_at = fields.DateTime(dump_only=True)
    model = None

    class Meta:
        """Meta."""
        fields = [
            'pk', 'model_class', 'created_at', 'start_time',
            'end_time', 'status', 'results', 'extra_parameters',
            'extra_files', "job_id"]

    @post_load(pass_many=False)
    def get_created_by_id(self, data):
        """Get logged user for serializer."""
        current_user = AuthFactory.retrieve_authenticated_user()
        data['created_by_id'] = current_user['pk']
        return data


class SerializerPusherJob(serializers.PumpWoodSerializer):
    """Base serialization for PusherJob."""
    created_by_id = fields.Integer(dump_only=True)
    created_at = fields.DateTime(dump_only=True)
    last_on_queue = fields.DateTime(dump_only=True)
    etl_file = fields.String(required=False)
    default_extra_files = fields.Dict(required=False)

    model = None

    class Meta:
        fields = [
            'pk', 'model_class', "name", "last_on_queue", "etl_file",
            "default_parameters", "default_extra_files", "created_at",
            "created_by_id", "notes", "binded_puller_job_set", "queue_set"]

    @post_load(pass_many=False)
    def get_created_by_id(self, data):
        """Get logged user for serializer."""
        current_user = AuthFactory.retrieve_authenticated_user()
        data['created_by_id'] = current_user['pk']
        return data


class SerializerPusherQueue(serializers.PumpWoodSerializer):
    """Base serialization for PusherQueue."""
    start_time = fields.DateTime(allow_none=True)
    end_time = fields.DateTime(allow_none=True)
    puller_queue_id = fields.Integer(allow_none=True)
    created_by_id = fields.Integer(dump_only=True)
    created_at = fields.DateTime(dump_only=True)

    model = None

    class Meta:
        fields = [
            'pk', 'model_class', "created_at", "start_time",
            "end_time", "puller_queue_id", "status", "results",
            "extra_parameters", "extra_files", "created_by_id", "job_id"]

    @post_load(pass_many=False)
    def get_created_by_id(self, data):
        """Get logged user for serializer."""
        current_user = AuthFactory.retrieve_authenticated_user()
        data['created_by_id'] = current_user['pk']
        return data
