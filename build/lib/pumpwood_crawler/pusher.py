"""Pusher module."""

import datetime
import pandas as pd
import inspect
# import itertools
# import numpy as np
from abc import ABC
from sqlalchemy import create_engine
from pumpwood_flaskmisc.database import build_engine_string
from pumpwood_flaskmisc.storage import PumpWoodStorage
from pumpwood_communication.microservices import PumpWoodMicroService
from pumpwood_communication.serializers import pumpJsonDump

from pumpwood_crawler.pusher_resolvers.resolvers import (
    AttributeResolver, CalendarResolver, GeoAreaResolver,
    GeoAttributeResolver, ModelingUnitResolver)


class PusherClass(ABC):
    """Class to help building pushers to datalake."""

    def __init__(self, database_config: dict, microservice_config: dict,
                 storage_config: dict, pusher_config: dict):
        """
        __init__.

        Args:
            database_config (dict): Configuration for database connectionself.
                See SQLAlchemy connection configuration. Keys:
                = dialect: Database dialect (postgres, sqlite).
                = driver: Driver to be used with dialect (can be None).
                = username: Username
                = password: Connection password.
                = host: Database host.
                = port: Database port.
                = database: Database name.
            microservice_config(dict): Configuration of the microservice
                connection. See pumpwood_communication.microservice
                = name: Name of the connection (debug purposes)
                = server_url: Url of the microservice.
                = username: Microservice username.
                = password: Microservice password.
                = verify_ssl: If the microservice will accept self-assigned
                    ssl.
            storage_config (dict): Configuration of the storage, see
                pumpwood-flaskmisc.storage. Keys:
                = storage_type: Storage type.
                = bucket_name: Bucket name.
                = base_path: Storage base_path
            pusher_config (dict): Configuration of the pusher, if not set at
                class attibutes.
                = job_model_class: Model class of the pusher job.
                = queue_model_class: Model class of the pusher queue.
        """
        database_url = build_engine_string(**database_config)
        self._sql_alchemy_engine = create_engine(database_url,
                                                 json_serializer=pumpJsonDump)
        self._storage = PumpWoodStorage(**storage_config)
        self._microservice = PumpWoodMicroService(**microservice_config)
        self._microservice.login()

        self.queue_model_class = pusher_config["queue_model_class"]
        self.job_model_class = pusher_config["job_model_class"]

    @staticmethod
    def add_queue_and_job_info(queue_data, job_data, extra_info: dict):
        """Add queue and job data to extra_info."""
        if queue_data is not None:
            extra_info['push_queue_id'] = queue_data.get('pk')

        if job_data is not None:
            extra_info['push_job_id'] = job_data.get('pk')
            extra_info['push_job_name'] = job_data.get('name')

        return extra_info

    def get_queue(self, pk):
        """
        Retrieve queue data for the crawler.

        Args:
          pk (int): Pk of the queue to be retrieved

        Kwargs:
          No Kwargs

        Returns:
          dict: Serialized queue object.

        Raises:
          No especific raises.

        Example:
          queue_obj = pusher_class_obj.get_queue(pk=10)

        """
        return self._microservice.list_one(
            model_class=self.queue_model_class, pk=pk)

    def get_job(self, pk):
        """
        Retrieve job data for the crawler.

        Args:
          pk (int): Pk of the job to be retrieved

        Kwargs:
          No Kwargs

        Returns:
          dict: Serialized job object.

        Raises:
          No especific raises.

        Example:
          job_obj = pusher_class_obj.get_job(pk=10)

        """
        return self._microservice.list_one(
            model_class=self.job_model_class, pk=pk)

    def process_message(self, queue_data: dict):
        """
        Process rabbitMQ message at pusher queue.

        Args:
          queue_data (dict): rabbitMQ message. Normaly a serialization of
          the queue object.

        Returns:
          dict: Serialized final queue object.

        Raises:
          Raises errors from the different process at pusher.

        Example:
          pusher_class_obj.process_message(queue_data)

        """
        queue_data = self.get_queue(pk=queue_data['pk'])
        if queue_data['status'] != 'idle':
            raise Exception('Status not idle')
        job_data = self.get_job(pk=queue_data['job_id'])

        ##################
        # Processing data#
        print('# pushing queue: %s' % (queue_data['pk'], ))
        queue_data['status'] = 'processing'
        queue_data['start_push_time'] = datetime.datetime.utcnow()
        self._microservice.save(obj_dict=queue_data)
        queue_data = self.get_queue(pk=queue_data['pk'])
        try:
            processed_data = self.process_data(
                queue_data=queue_data, job_data=job_data)
        except Exception as e:
            queue_data['status'] = 'error_processing'
            queue_data['results'].update({'error_processing': str(e)})
            self._microservice.save(obj_dict=queue_data)
            raise e
        #################

        queue_data = self.get_queue(pk=queue_data['pk'])
        queue_data['results'].update({
            'processing_results': processed_data.pop(
                'processing_results', {})})
        queue_data['status'] = 'pushing'
        self._microservice.save(obj_dict=queue_data)

        ###############
        # Pushing data#
        try:
            processed_data_results = self.push_data(
                processed_data=processed_data,
                queue_data=queue_data, job_data=job_data)
        except Exception as e:
            queue_data['status'] = 'error_pushing'
            queue_data['results'].update({'error_pushing': str(e)})
            queue_data['end_push_time'] = datetime.datetime.utcnow(
                ).isoformat()
            self._microservice.save(obj_dict=queue_data)
            raise e
        ##############

        queue_data = self.get_queue(pk=queue_data['pk'])
        queue_data['status'] = 'finish_pushing'
        queue_data['end_push_time'] = datetime.datetime.utcnow().isoformat()
        queue_data['results'].update(processed_data_results)
        resp = self._microservice.save(obj_dict=queue_data)
        print('!!!!finish queue: %s' % (queue_data['pk'], ))
        return resp

    def process_data(self, queue_data: dict, job_data: dict):
        """
        Process crawler data to put in pumpwood format.

        Args:
            queue_data (dict): rabbitMQ message, usually a queue serialized
            object.
            job_data(dict): Job serialized data.

        Kwargs:
            No Kwargs.

        Returns:
            A dictionary with some of the keys bellow;
            -- Dimentions
            modeling_unit_dimention (pandas.DataFrame):
                Modeling Units to be added or updated.
            modeling_unit_hierarchy (pandas.DataFrame):
                Modeling Units Hierarchy to be added or replaced.

            geoarea_dimention (pandas.DataFrame):
                Geoarea to be added or updated.
            geoarea_hierarchy (pandas.DataFrame):
                Geoarea to be added or updated.

            attribute_dimention (pandas.DataFrame):
                Attribute to be added or updated.
            attribute_categorical (pandas.DataFrame):
                Attribute categories to be added or updated.
            attribute_hierarchy (pandas.DataFrame):
                Attribute Hierarchy to be added or replaced.

            geoattribute_dimention (pandas.DataFrame):
                GeoAttribute to be added or updated.
            geoattribute_categorical (pandas.DataFrame):
                GeoAttribute categories to be added or updated.
            geoattribute_hierarchy (pandas.DataFrame):
                GeoAttribute Hierarchy to be added or replaced.

            calendar_dimention (pandas.DataFrame):
                Calendar to be added or updated.
            calendar_categorical (pandas.DataFrame):
                Calendar categories to be added or updated.
            calendar_hierarchy (pandas.DataFrame):
                Calendar Hierarchy to be added or replaced.

            database_categorical_data (pandas.DataFrame):
                Categoric data for database, must have [time, modeling_unit,
                geoarea, attribute, description] as columns. Description must
                match a previus described CategoricalAttributeDescription.
            database_data (pandas.DataFrame):
                Number data for database, must have [time, modeling_unit,
                geoarea, attribute, value] as columns

            calendar_categorical_data (pandas.DataFrame):
                Categoric data for database, must have [time, calendar,
                description] as columns. Description must
                match a previus described CategoricalCalendarDescription.
            calendar_data (pandas.DataFrame):
                Number data for database, must have [time, calendar, value]
                as columns.

            geodatabase_categorical_data (pandas.DataFrame):
                Categoric data for database, must have [time, geoarea,
                geoattribute, description] as columns. Description must
                match a previus described CategoricalGeoAttributeDescription.
            geodatabase_data (pandas.DataFrame):
                Number data for database, must have [time, geoarea,
                geoattribute, value] as columns.
        Raises:
            Many raises from resolvers.

        Example:
            ---

        """
        file_content = self._storage.read_file(job_data['etl_file'])
        #
        #
        #
        # queue_data
        # job_data

        ns = {}
        code = compile(file_content['data'], '<string>', 'exec')
        exec(code, ns)
        if 'etl_process' not in ns.keys():
            raise Exception('etl_process function not found at etl_file.')

        etl_process_fn = ns['etl_process']
        run_etl_call_arguments = set(
            inspect.signature(etl_process_fn).parameters.keys())

        expected_args = set(
            ['storage', 'microservice', 'db_engine', 'queue_data', 'job_data'])
        if run_etl_call_arguments != expected_args:
            template = 'etl_process function must have %s as arguments. ' + \
                'etl_process args: %s'
            raise Exception(template % (
                set(run_etl_call_arguments.keys()), run_etl_call_arguments))

        data_to_save = etl_process_fn(
            storage=self._storage, microservice=self._microservice,
            db_engine=self._sql_alchemy_engine, queue_data=queue_data,
            job_data=job_data)
        return data_to_save

    def get_elt_variables(self, queue_data: dict):
        """Make ETL function parameters."""
        self._microservice.login()

        fun_parameters = {'queue_id': queue_data['pk'],
                          'job_id': queue_data['job_id']}

        return [self._storage, self._microservice,
                self._sql_alchemy_engine, fun_parameters,
                queue_data['extra_files']]

    @classmethod
    def _validate_data(cls, dataframe: pd.DataFrame, unique_cols: list,
                       dataframe_type: str):
        validation_dataframe = dataframe.groupby(unique_cols).size()
        index_val_dataframe = validation_dataframe > 1
        if index_val_dataframe.any():
            index_val_dataframe = index_val_dataframe.reset_index()
            index_val_dataframe = index_val_dataframe[
                index_val_dataframe[0]]

            val_table = str(index_val_dataframe)
            template = "{df_type} with not unique {unique_cols}:\n{data}"
            return template.format(
                df_type=dataframe_type, unique_cols=str(unique_cols),
                data=val_table)
        else:
            return None

    def push_data(self, processed_data: dict, queue_data: dict={},
                  job_data: dict={}):
        """
        Push processed data to pumpwood server.

        Args:
            processed_data (dict): Dictionary result from process_data
            function.

        Kwargs:
            No Kwargs.

        Returns:
            dict: Returns the final queue serialized object.

        Raises:
            Exception('dataframe to load with duplicates: %s'): if the
            data processed have more than one value for each time
            , modeling_unit, geoarea, attribute

        Example:
            ---

        """
        # modeling_unit
        modeling_unit_dimention = processed_data.get(
            'modeling_unit_dimention',
            pd.DataFrame([], columns=[
                'description', 'notes', 'dimentions', 'extra_info']))
        modeling_unit_hierarchy = processed_data.get(
            'modeling_unit_hierarchy',
            pd.DataFrame([], columns=[
                'node', 'child_node', 'extra_info']))

        # geoarea
        geoarea_dimention = processed_data.get(
            'geoarea_dimention',
            pd.DataFrame([], columns=[
                'description', 'notes', 'dimentions', 'extra_info']))
        geoarea_hierarchy = processed_data.get(
            'geoarea_hierarchy',
            pd.DataFrame([], columns=[
                'node', 'child_node', 'extra_info']))

        # Attributes
        attribute_dimention = processed_data.get(
            'attribute_dimention',
            pd.DataFrame([], columns=[
                'type_id', 'description', 'notes', 'dimentions',
                'extra_info']))
        attribute_categorical = processed_data.get(
            'attribute_categorical',
            pd.DataFrame([], columns=[
                'attribute', 'value', 'description', 'ordering',
                'extra_info']))
        attribute_hierarchy = processed_data.get(
            'attribute_hierarchy',
            pd.DataFrame([], columns=[
                'node', 'child_node', 'extra_info']))
        database_categorical_data = processed_data.get(
            'database_categorical_data',
            pd.DataFrame([], columns=[
                'time', 'modeling_unit', 'geoarea', 'attribute',
                'description']))
        database_data = processed_data.get(
            'database_data',
            pd.DataFrame([], columns=[
                'time', 'modeling_unit', 'geoarea', 'attribute', 'value']))

        # GeoAttribute
        geoattribute_dimention = processed_data.get(
            'geoattribute_dimention',
            pd.DataFrame([], columns=[
                'type_id', 'description', 'notes', 'dimentions',
                'extra_info']))
        geoattribute_categorical = processed_data.get(
            'geoattribute_categorical',
            pd.DataFrame([], columns=[
                'geoattribute', 'value', 'description', 'ordering',
                'extra_info']))
        geoattribute_hierarchy = processed_data.get(
            'geoattribute_hierarchy',
            pd.DataFrame([], columns=[
                'node', 'child_node', 'extra_info']))
        geodatabase_categorical_data = processed_data.get(
            'geodatabase_categorical_data',
            pd.DataFrame([], columns=[
                'time', 'geoarea', 'geoattribute', 'description']))
        geodatabase_data = processed_data.get(
            'geodatabase_data',
            pd.DataFrame([], columns=[
                'time', 'geoarea', 'geoattribute', 'value']))

        # Calendar
        calendar_dimention = processed_data.get(
            'calendar_dimention',
            pd.DataFrame([], columns=[
                'type_id', 'description', 'notes', 'dimentions',
                'extra_info']))
        calendar_categorical = processed_data.get(
            'calendar_categorical',
            pd.DataFrame([], columns=[
                'calendar', 'value', 'description', 'ordering',
                'extra_info']))
        calendar_hierarchy = processed_data.get(
            'calendar_hierarchy',
            pd.DataFrame([], columns=[
                'node', 'child_node', 'extra_info']))
        calendar_categorical_data = processed_data.get(
            'calendar_categorical_data',
            pd.DataFrame([], columns=['time', 'calendar', 'description']))
        calendar_data = processed_data.get(
            'calendar_data',
            pd.DataFrame([], columns=['time', 'calendar', 'value']))

        ##############
        # Validate data
        validate_error = []
        validate_error.append(self._validate_data(
            dataframe=database_categorical_data,
            unique_cols=['time', 'modeling_unit', 'geoarea', 'attribute'],
            dataframe_type="Database Categoric"))
        validate_error.append(self._validate_data(
            dataframe=database_data,
            unique_cols=['time', 'modeling_unit', 'geoarea', 'attribute'],
            dataframe_type="Database"))
        validate_error.append(self._validate_data(
            dataframe=geodatabase_categorical_data,
            unique_cols=['time', 'geoarea', 'geoattribute'],
            dataframe_type="GeoDatabase Categoric"))
        validate_error.append(self._validate_data(
            dataframe=geodatabase_data,
            unique_cols=['time', 'geoarea', 'geoattribute'],
            dataframe_type="GeoDatabase"))
        validate_error.append(self._validate_data(
            dataframe=calendar_categorical_data,
            unique_cols=['time', 'calendar'],
            dataframe_type="Calendar Categoric"))
        validate_error.append(self._validate_data(
            dataframe=calendar_data,
            unique_cols=['time', 'calendar'],
            dataframe_type="Calendar"))
        validate_error = [x for x in validate_error if x is not None]
        if len(validate_error) > 0:
            template = "Some data not unique:\n{data}"
            raise Exception(template.format(data="\n".join(validate_error)))

        ##################
        # Resolver options
        # Modeling Unit
        add_new_modeling_unit = job_data["default_parameters"].get(
            "add_new_modeling_unit", False)
        add_new_modeling_unit = queue_data.get('extra_parameters').get(
            'add_new_modeling_unit', add_new_modeling_unit)

        add_hierarchy_modeling_unit = job_data["default_parameters"].get(
            "add_hierarchy_modeling_unit", 'fail')
        add_hierarchy_modeling_unit = queue_data.get('extra_parameters').get(
            'add_hierarchy_modeling_unit', add_hierarchy_modeling_unit)

        # Geoarea
        add_new_geoarea = job_data["default_parameters"].get(
            "add_new_geoarea", False)
        add_new_geoarea = queue_data.get('extra_parameters').get(
            'add_new_geoarea', add_new_geoarea)

        add_hierarchy_geoarea = job_data["default_parameters"].get(
            "add_hierarchy_geoarea", 'fail')
        add_hierarchy_geoarea = queue_data.get('extra_parameters').get(
            'add_hierarchy_geoarea', add_hierarchy_geoarea)

        ############
        # Attributes
        add_new_attribute = job_data["default_parameters"].get(
            "add_new_attribute", False)
        add_new_attribute = queue_data.get('extra_parameters').get(
            'add_new_attribute', add_new_attribute)

        add_new_categorical_attribute = job_data["default_parameters"].get(
            "add_new_categorical_attribute", 'fail')
        add_new_categorical_attribute = queue_data.get('extra_parameters').get(
            'add_new_categorical_attribute', add_new_categorical_attribute)

        add_hierarchy_attribute = job_data["default_parameters"].get(
            "add_hierarchy_attribute", 'fail')
        add_hierarchy_attribute = queue_data.get('extra_parameters').get(
            'add_hierarchy_attribute', add_hierarchy_attribute)

        ##########
        # Calendar
        add_new_calendar = job_data["default_parameters"].get(
            "add_new_calendar", False)
        add_new_calendar = queue_data.get('extra_parameters').get(
            'add_new_calendar', add_new_calendar)

        add_new_categorical_calendar = job_data["default_parameters"].get(
            "add_new_categorical_calendar", 'fail')
        add_new_categorical_calendar = queue_data.get('extra_parameters').get(
            'add_new_categorical_calendar', add_new_categorical_calendar)

        add_hierarchy_calendar = job_data["default_parameters"].get(
            "add_hierarchy_calendar", 'fail')
        add_hierarchy_calendar = queue_data.get('extra_parameters').get(
            'add_hierarchy_calendar', add_hierarchy_calendar)

        ##############
        # Geoattribute
        add_new_geoatribute = job_data["default_parameters"].get(
            "add_new_geoatribute", False)
        add_new_geoatribute = queue_data.get('extra_parameters').get(
            'add_new_geoatribute', add_new_geoatribute)

        add_new_categorical_geoatribute = job_data["default_parameters"].get(
            "add_new_categorical_geoatribute", 'fail')
        add_new_categorical_geoatribute = queue_data.get(
            'extra_parameters').get(
            'add_new_categorical_geoatribute', add_new_categorical_geoatribute)

        add_hierarchy_geoatribute = job_data["default_parameters"].get(
            "add_hierarchy_geoatribute", 'fail')
        add_hierarchy_geoatribute = queue_data.get('extra_parameters').get(
            'add_hierarchy_geoatribute', add_hierarchy_geoatribute)
        #######################
        #######################
        # Dimentions
        modeling_unit_resolver = ModelingUnitResolver(
            microservice=self._microservice,
            queue_data=queue_data, job_data=job_data,
            add_new_dimention=add_new_modeling_unit,
            add_hierarchy=add_hierarchy_modeling_unit,
            add_new_categorical='fail')
        geoarea_resolver = GeoAreaResolver(
            microservice=self._microservice,
            queue_data=queue_data, job_data=job_data,
            add_new_dimention=add_new_geoarea,
            add_hierarchy=add_hierarchy_geoarea,
            add_new_categorical='fail')
        # Variables
        attribute_resolver = AttributeResolver(
            microservice=self._microservice,
            queue_data=queue_data, job_data=job_data,
            add_new_dimention=add_new_attribute,
            add_hierarchy=add_hierarchy_attribute,
            add_new_categorical=add_new_categorical_attribute)
        calendar_resolver = CalendarResolver(
            microservice=self._microservice,
            queue_data=queue_data, job_data=job_data,
            add_new_dimention=add_new_calendar,
            add_hierarchy=add_hierarchy_calendar,
            add_new_categorical=add_new_categorical_calendar)
        geoattribute_resolver = GeoAttributeResolver(
            microservice=self._microservice,
            queue_data=queue_data, job_data=job_data,
            add_new_dimention=add_new_geoatribute,
            add_hierarchy=add_hierarchy_geoatribute,
            add_new_categorical=add_new_categorical_geoatribute)

        modeling_unit_resolver.resolve_dimentions(
            dimention_table=modeling_unit_dimention.copy())
        modeling_unit_resolver.resolve_hierarchy(
            hierarchy_table=modeling_unit_hierarchy.copy())

        geoarea_resolver.resolve_dimentions(
            dimention_table=geoarea_dimention.copy())
        geoarea_resolver.resolve_hierarchy(
            hierarchy_table=geoarea_hierarchy.copy())

        attribute_resolver.resolve_dimentions(
            dimention_table=attribute_dimention.copy())
        attribute_resolver.resolve_hierarchy(
            hierarchy_table=attribute_hierarchy.copy())
        attribute_resolver.resolve_categories(
            categories=attribute_categorical.copy())

        calendar_resolver.resolve_dimentions(
            dimention_table=calendar_dimention.copy())
        calendar_resolver.resolve_hierarchy(
            hierarchy_table=calendar_hierarchy.copy())
        calendar_resolver.resolve_categories(
            categories=calendar_categorical.copy())

        geoattribute_resolver.resolve_dimentions(
            dimention_table=geoattribute_dimention.copy())
        geoattribute_resolver.resolve_hierarchy(
            hierarchy_table=geoattribute_hierarchy.copy())
        geoattribute_resolver.resolve_categories(
            categories=geoattribute_categorical.copy())

        geoareas = \
            database_categorical_data['geoarea'].unique().tolist() + \
            database_data['geoarea'].unique().tolist() + \
            geodatabase_categorical_data['geoarea'].unique().tolist() + \
            geodatabase_data['geoarea'].unique().tolist()
        geoareas = list(set(geoareas))

        modeling_units = \
            database_categorical_data['modeling_unit'].unique().tolist() + \
            database_data['modeling_unit'].unique().tolist()
        modeling_units = list(set(modeling_units))

        attributes = \
            database_categorical_data['attribute'].unique().tolist() + \
            database_data['attribute'].unique().tolist()
        attributes = list(set(attributes))

        geoattributes = \
            geodatabase_categorical_data['geoattribute'].unique().tolist() + \
            geodatabase_data['geoattribute'].unique().tolist()
        geoattributes = list(set(geoattributes))

        calendars = \
            calendar_categorical_data['calendar'].unique().tolist() + \
            calendar_data['calendar'].unique().tolist()
        calendars = list(set(calendars))

        server_desc_geoareas = self._microservice.list_without_pag(
            "DescriptionGeoArea",
            filter_dict={"description__in": geoareas})
        server_desc_modeling_units = self._microservice.list_without_pag(
            "DescriptionModelingUnit",
            filter_dict={"description__in": modeling_units})
        server_desc_attributes = self._microservice.list_without_pag(
            "DescriptionAttribute",
            filter_dict={"description__in": attributes})
        server_desc_geoattributes = self._microservice.list_without_pag(
            "DescriptionGeoAttribute",
            filter_dict={"description__in": geoattributes})
        server_desc_calendars = self._microservice.list_without_pag(
            "DescriptionCalendar",
            filter_dict={"description__in": calendars})

        pk_desc_geoareas = dict([
            (x["description"], x["pk"])
            for x in server_desc_geoareas])
        pk_desc_modeling_units = dict([
            (x["description"], x["pk"])
            for x in server_desc_modeling_units])
        pk_desc_attributes = dict([
            (x["description"], x["pk"])
            for x in server_desc_attributes])
        pk_desc_geoattributes = dict([
            (x["description"], x["pk"])
            for x in server_desc_geoattributes])
        pk_desc_calendars = dict([
            (x["description"], x["pk"])
            for x in server_desc_calendars])

        # Check if all dimentions are at database
        miss_geoareas = list(set(geoareas) -
                             set(pk_desc_geoareas.keys()))
        miss_modeling_units = list(set(modeling_units) -
                                   set(pk_desc_modeling_units.keys()))
        miss_attributes = list(set(attributes) -
                               set(pk_desc_attributes.keys()))
        miss_geoattributes = list(set(geoattributes) -
                                  set(pk_desc_geoattributes.keys()))
        miss_calendars = list(set(calendars) -
                              set(pk_desc_calendars.keys()))
        if len(miss_geoareas) + len(miss_modeling_units) + \
           len(miss_attributes) + len(miss_geoattributes) + \
           len(miss_calendars) > 0:
            template = "Some dimentions are missing:"
            if len(miss_geoareas) > 0:
                template = template + \
                    "\nGeoareas: ['" + "', '".join(miss_geoareas) + \
                    "']"
            if len(miss_modeling_units) > 0:
                template = template + \
                    "\nModelingUnit: ['" + "', '".join(miss_modeling_units) + \
                    "']"
            if len(miss_attributes) > 0:
                template = template + \
                    "\nAttributes: ['" + "', '".join(miss_attributes) + \
                    "']"
            if len(miss_geoattributes) > 0:
                template = template + \
                    "\nGeoAttributes: ['" + "', '".join(miss_geoattributes) + \
                    "']"
            if len(miss_calendars) > 0:
                template = template + \
                    "\nCalendars: ['" + "', '".join(miss_calendars) + \
                    "']"
            raise Exception(template)

        database_categorical_data['attribute_id'] = database_categorical_data[
            'attribute'].map(pk_desc_attributes)
        database_categorical_data['geoarea_id'] = database_categorical_data[
            'geoarea'].map(pk_desc_geoareas)
        database_categorical_data['modeling_unit_id'] = \
            database_categorical_data[
                'modeling_unit'].map(pk_desc_modeling_units)

        database_data['attribute_id'] = database_data[
            'attribute'].map(pk_desc_attributes)
        database_data['geoarea_id'] = database_data[
            'geoarea'].map(pk_desc_geoareas)
        database_data['modeling_unit_id'] = database_data[
            'modeling_unit'].map(pk_desc_modeling_units)

        geodatabase_categorical_data['geoarea_id'] = \
            geodatabase_categorical_data[
                'geoarea'].map(pk_desc_geoareas)
        geodatabase_categorical_data['geoattribute_id'] = \
            geodatabase_categorical_data[
                'geoattribute'].map(pk_desc_geoattributes)

        geodatabase_data['geoarea_id'] = geodatabase_data[
            'geoarea'].map(pk_desc_geoareas)
        geodatabase_data['geoattribute_id'] = geodatabase_data[
            'geoattribute'].map(pk_desc_geoattributes)

        calendar_categorical_data['calendar_id'] = calendar_categorical_data[
            'calendar'].map(pk_desc_calendars)
        calendar_data['calendar_id'] = calendar_data[
            'calendar'].map(pk_desc_calendars)

        categ_att = database_categorical_data[
            'attribute_id'].unique().tolist()
        categ_geo = geodatabase_categorical_data[
            'geoattribute_id'].unique().tolist()
        categ_cal = calendar_categorical_data[
            'calendar_id'].unique().tolist()

        server_categ_att = pd.DataFrame(
            self._microservice.list_without_pag(
                "CategoricalAttributeDescription",
                filter_dict={"attribute_id__in": categ_att}),
            columns=["attribute_id", "description", "value"])
        server_categ_geo = pd.DataFrame(
            self._microservice.list_without_pag(
                "CategoricalGeoAttributeDescription",
                filter_dict={"geoattribute_id__in": categ_geo}),
            columns=["geoattribute_id", "description", "value"])
        server_categ_cal = pd.DataFrame(
            self._microservice.list_without_pag(
                "CategoricalCalendarDescription",
                filter_dict={"calendar_id__in": categ_cal}),
            columns=["calendar_id", "description", "value"])

        database_categorical_data['description'] = \
            database_categorical_data['description'].astype('str')
        geodatabase_categorical_data['description'] = \
            geodatabase_categorical_data['description'].astype('str')
        calendar_categorical_data['description'] = \
            calendar_categorical_data['description'].astype('str')

        merged_database_categorical_data = None
        if len(database_categorical_data) == 0 and len(server_categ_att) == 0:
            merged_database_categorical_data = pd.DataFrame(
                [], columns=[
                    'time', 'modeling_unit_id', 'geoarea_id', 'attribute_id'
                    'value', 'description'])
        else:
            merged_database_categorical_data = database_categorical_data.merge(
                server_categ_att, how="left",
                on=["attribute_id", "description"])

        ##################################
        # merged_database_categorical_data
        merged_database_categorical_data = None
        if len(database_categorical_data) == 0 and len(server_categ_att) == 0:
            merged_database_categorical_data = pd.DataFrame(
                [], columns=[
                    'time', 'modeling_unit_id', 'geoarea_id', 'attribute_id',
                    'value', 'description'])
        else:
            merged_database_categorical_data = database_categorical_data.merge(
                server_categ_att, how="left",
                on=["attribute_id", "description"])

        #####################################
        # merged_geodatabase_categorical_data
        merged_geodatabase_categorical_data = None
        if len(geodatabase_categorical_data) == 0 and \
                len(server_categ_geo) == 0:
            merged_geodatabase_categorical_data = pd.DataFrame(
                [], columns=[
                    'time', 'geoarea_id', 'geoattribute_id', 'value',
                    'description'])
        else:
            merged_geodatabase_categorical_data = \
                geodatabase_categorical_data.merge(
                    server_categ_geo, how="left", on=[
                        "geoattribute_id", "description"])

        #####################################
        # merged_calendar_categorical_data
        merged_calendar_categorical_data = None
        if len(calendar_categorical_data) == 0 and len(server_categ_cal) == 0:
            merged_calendar_categorical_data = pd.DataFrame(
                [], columns=[
                    'time', 'calendar_id', 'value', 'description'])
        else:
            merged_calendar_categorical_data = calendar_categorical_data.merge(
                server_categ_cal, how="left", on=[
                    'calendar_id', "description"])

        value_data_na = merged_database_categorical_data['value'].isna()
        value_geo_na = merged_geodatabase_categorical_data['value'].isna()
        value_cal_na = merged_calendar_categorical_data['value'].isna()

        error_categorical_list = []
        if value_data_na.any():
            to_error = merged_database_categorical_data.loc[
                value_data_na, ['description', 'attribute']].drop_duplicates()
            template = "Some categoric attributes not at datalake:\n{cat}"
            error_categorical_list.append(template.format(cat=str(to_error)))

        if value_geo_na.any():
            to_error = merged_geodatabase_categorical_data.loc[
                value_geo_na, ['description', 'geoattribute']
            ].drop_duplicates()
            template = "Some categoric geoattributes not at datalake:\n{cat}"
            error_categorical_list.append(template.format(cat=str(to_error)))

        if value_cal_na.any():
            to_error = merged_calendar_categorical_data.loc[
                value_cal_na, ['description', 'calendar']].drop_duplicates()
            template = "Some categoric calendars not at datalake:\n{cat}"
            error_categorical_list.append(template.format(cat=str(to_error)))

        if len(error_categorical_list):
            raise Exception('\n'.join(error_categorical_list))
        database_columns = [
            'time', 'attribute_id', 'modeling_unit_id', 'geoarea_id', 'value']
        geoattribute_columns = [
            'time', 'geoattribute_id', 'geoarea_id', 'value']
        calendar_columns = ['time', 'calendar_id', 'value']

        merged_database_categorical_data = merged_database_categorical_data[
            database_columns]
        database_data = database_data[database_columns]

        merged_geodatabase_categorical_data = \
            merged_geodatabase_categorical_data[geoattribute_columns]
        geodatabase_data = geodatabase_data[geoattribute_columns]

        merged_calendar_categorical_data = \
            merged_calendar_categorical_data[calendar_columns]
        calendar_data = calendar_data[calendar_columns]

        database_data = database_data.append(
            merged_database_categorical_data, ignore_index=True)
        geodatabase_data = geodatabase_data.append(
            merged_geodatabase_categorical_data, ignore_index=True)
        calendar_data = calendar_data.append(
            merged_calendar_categorical_data, ignore_index=True)

        process_results = {}
        if len(database_data):
            new_datainput = self._microservice.save({
                "model_class": "DataInputDatabaseVariable", "origin": "REST"})
            database_data["datainput_origin_id"] = new_datainput["pk"]
            self._microservice.parallel_bulk_save(
                model_class="ToLoadDataBaseVariable",
                data_to_save=database_data)
            self._microservice.execute_action(
                model_class="DataInputDatabaseVariable",
                pk=new_datainput["pk"], action="process_input")
            process_results["DataBaseVariable"] = {
                "length": len(database_data), "datainput": new_datainput}

        if len(geodatabase_data):
            new_datainput = self._microservice.save({
                "model_class": "DataInputGeoDatabaseVariable",
                "origin": "REST"})
            geodatabase_data["datainput_origin_id"] = new_datainput["pk"]
            self._microservice.parallel_bulk_save(
                model_class="ToLoadGeoDataBaseVariable",
                data_to_save=geodatabase_data)
            self._microservice.execute_action(
                model_class="DataInputGeoDatabaseVariable",
                pk=new_datainput["pk"], action="process_input")
            process_results["GeoDataBaseVariable"] = {
                "length": len(database_data), "datainput": new_datainput}

        if len(calendar_data):
            new_datainput = self._microservice.save({
                "model_class": "DataInputCalendar", "origin": "REST"})
            calendar_data["datainput_origin_id"] = new_datainput["pk"]
            self._microservice.parallel_bulk_save(
                model_class="ToLoadCalendar",
                data_to_save=calendar_data)
            self._microservice.execute_action(
                model_class="DataInputCalendar",
                pk=new_datainput["pk"], action="process_input")
            process_results["Calendar"] = {
                "length": len(database_data), "datainput": new_datainput}

        return {"pushing_results": process_results}
