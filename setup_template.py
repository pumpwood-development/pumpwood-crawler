#!/usr/bin/python
# -*- coding: utf-8 -*-
"""module setup."""


import os
from setuptools import find_packages, setup

# with open(os.path.join(os.path.dirname(__file__), 'README.rst')) as readme:
#     README = readme.read()

# allow setup.py to be run from any path
os.chdir(os.path.normpath(os.path.join(os.path.abspath(__file__), os.pardir)))

setup(
    name='pumpwood_crawler',
    version='{VERSION}',
    packages=find_packages(),
    include_package_data=True,
    license='',
    description='Crawler package',
    url='',
    author='Andre Baceti',
    author_email='a.baceti@murabei.com',
    classifiers=[
    ],
    install_requires=['Flask-SQLAlchemy',
                      'sqlalchemy',
                      'Flask-Testing',
                      'Flask-Migrate',
                      'psycopg2-binary',
                      'python-slugify==1.2.5',
                      'pumpwood-flaskmisc',
                      'pumpwood-communication'],
    dependency_links=[]
)
