"""Resolver module for attibutes."""
import math
import copy
import pandas as pd
import numpy as np
from abc import ABC
from pumpwood_communication.microservices import PumpWoodMicroService


def is_dict_equal(dict1: dict, dict2: dict):
    """Check if two dictionaries are equal."""
    keys_dict1 = dict1.keys()
    keys_dict2 = dict2.keys()
    if keys_dict1 != keys_dict2:
        return False

    for k in keys_dict1:
        value_key1 = dict1[k]
        value_key2 = dict2[k]

        type_1 = type(value_key1)
        type_2 = type(value_key2)
        if type_1 != type_2:
            return False
        if type_1 == dict:
            return is_dict_equal(value_key1, value_key2)
        if value_key1 != value_key2:
            return False
    return True


class AbstractResolver(ABC):
    """Abstract resolver for pusher variables."""

    queue_data = None
    """Queue data."""
    job_data = None
    """Job bata."""

    dimention_model_class = None
    """Model class of the dimention object, if null do not exist."""
    categorical_value_model_class = None
    """Model class of the categorical value, if null do not exist."""
    hierarchy_model_class = None
    """Model class of the hierarchy, if null do not exist."""

    dimention_extra_attributes = None
    """Dimention extra attributes that are not description, notes,
       extra_info and dimention."""

    def __init__(self, microservice: PumpWoodMicroService,
                 queue_data: dict, job_data: dict, add_new_dimention: bool,
                 add_hierarchy: str, add_new_categorical: bool=None):
        """
        __init__.

        Args:
            sql_alchemy_engine (sqlalchemy.engine): Engine of connection to
                                                   database.
            _microservice (pumpwood _microservice): Microservice object for
                                                 Pumpwood.
            storage (pumpwood storage): Storage object for PumpWood.
            queue_data (dict): Serialized queue object.
            job_data (dict): Serialized job object.
            add_new_dimention (bool): If resolver can add new attributes.
            add_new_categorical ['add', 'substitute', 'fail']:
                If resolver can add categories.
                = add: Add new categories.
                = fail: If new categories are not in server, raise an
                    Exception.
            add_hierarchy ['add', 'substitute', 'fail']:
                If resolver can add hierarchy to attributes.
                = add: Add new hierarchy.
                = replace: Replace existing hierarchy with new one.
                = fail: If new hierarchy are not in server, raise an
                    Exception.

        """
        self._microservice = microservice
        self.job_data = job_data
        self.queue_data = queue_data
        self.add_new_dimention = add_new_dimention
        self.add_hierarchy = add_hierarchy
        self.add_new_categorical = add_new_categorical

        self.cached_dimentions = None
        self.cached_category = None
        self.cached_hierarchy = None

    def add_queue_and_job_info(self, extra_info: dict):
        """Add queue and job data to extra_info."""
        extra_info['pusher_queue_data'] = self.queue_data
        extra_info['pusher_job_data'] = self.job_data
        return extra_info

    def resolve_dimentions(self, dimention_table: pd.DataFrame):
        """
        Resolve dimentions: add a new or fetch from PumpWood.

        Resolve dimentions, get its pks by the descriptions
        and if the add_new is set True, it will create a new dimention
        with the dictionary key value.

        Args:
            dimention_df (pd.DataFrame): Dictionary with the dimention
                caracteristics (DescriptionAttribute, DescriptionModelingUnit,
                 DescriptionGeoArea, ...)..
            queue_data (dict): Serialized queue object.
            job_data (dict): Serialized job object.

        Kwargs:
            No Kwargs.

        Returns:
            dict: A dictionary with same keys with the pk at server.

        Raises:
            Exception( 'Error when creating new [model_class]:--Payload:
            %s--Msg:%s'):
                If the is an error creating a new attribute

            Exception('Pusher not adding new [model_class] and some
            descriptions not found at server:%s'):
                If the pusher is not allowed to create new attributes and
                sobre descriptions are not at the server.

        Example:
            No example

        """
        if type(dimention_table) != pd.DataFrame:
            template = "Argument dimention_table for {model_class} " + \
                "resolver must be a pandas.DataFrame"
            raise Exception(template.format(
                model_class=self.dimention_model_class))

        dimention_columns = ["description", "notes", "dimentions",
                             "extra_info"] + self.dimention_extra_attributes
        if len(dimention_table) == 0:
            return pd.DataFrame([], columns=dimention_columns)

        if set(dimention_table.columns) != set(dimention_columns):
            template = "Dimentions table for {model_class} do not have " + \
                "right columns:\ndimention_table.columns:{columns_set}\n" + \
                "needed: {need_cols}"
            raise Exception(
                template.format(
                    model_class=self.dimention_model_class,
                    columns_set=set(dimention_table.columns),
                    need_cols=set(dimention_columns), ))

        validate_dimention = dimention_table.groupby(['description']).size()
        if (validate_dimention > 1).any():
            not_unique = validate_dimention[
                validate_dimention != 1].reset_index()
            not_unique_dimentions = dimention_table[
                dimention_table["description"].isin(not_unique["description"])]
            template = "Some description are not unique in dimentions:\n{dim}"
            raise Exception(template.format(dim=str(not_unique_dimentions)))

        dim_descriptions = dimention_table["description"].unique().tolist()
        server_dimentions = self._microservice.list_without_pag(
            self.dimention_model_class, {'description__in': dim_descriptions})
        df_server_dimentions = pd.DataFrame(
            server_dimentions,
            columns=["pk", "model_class"] + dimention_columns)

        merged_dimentions = dimention_table.merge(
            df_server_dimentions, how="left",
            on=["description"], suffixes=["__local", "__server"])

        # Model class
        merged_dimentions.loc[
            merged_dimentions['model_class'].isna(), "model_class"] = \
            self.dimention_model_class

        # Extra info
        na_extra_info__server = merged_dimentions['extra_info__server'].isna()
        merged_dimentions.loc[na_extra_info__server, 'extra_info__server'] = \
            [{}]*na_extra_info__server.sum()
        na_extra_info__local = merged_dimentions['extra_info__local'].isna()
        merged_dimentions.loc[na_extra_info__local, 'extra_info__local'] = \
            [{}]*na_extra_info__local.sum()

        # Dimention
        na_dimentions__server = merged_dimentions['dimentions__server'].isna()
        merged_dimentions.loc[na_dimentions__server, 'dimentions__server'] = \
            [{}]*na_dimentions__server.sum()
        na_dimentions__local = merged_dimentions['dimentions__local'].isna()
        merged_dimentions.loc[na_dimentions__local, 'dimentions__local'] = \
            [{}]*na_dimentions__local.sum()

        # Notes
        na_dimentions__server = merged_dimentions['notes__server'].isna()
        merged_dimentions.loc[na_dimentions__server, 'notes__server'] = ""
        na_dimentions__local = merged_dimentions['notes__local'].isna()
        merged_dimentions.loc[na_dimentions__local, 'notes__local'] = ""

        to_save = []
        no_change = []
        for obj in merged_dimentions.to_dict("reccords"):
            update = False
            temp_save_obj = {"model_class": obj["model_class"],
                             "description": obj["description"], }

            if math.isnan(obj["pk"]):
                update = True
            else:
                temp_save_obj["pk"] = obj["pk"]

            if obj['notes__local'] in obj['notes__server']:
                temp_save_obj["notes"] = obj['notes__server']
            else:
                temp_save_obj["notes"] = obj['notes__local']
                update = True

            if is_dict_equal(obj['dimentions__local'],
                             obj['dimentions__server']):
                temp_save_obj["dimentions"] = copy.deepcopy(
                    obj["dimentions__server"])
            else:
                dimentions_temp = copy.deepcopy(obj['dimentions__server'])
                dimentions_temp.update(obj['dimentions__local'])
                temp_save_obj["dimentions"] = dimentions_temp
                update = True

            if is_dict_equal(obj['extra_info__local'],
                             obj['extra_info__server']):
                temp_save_obj["extra_info"] = copy.deepcopy(
                    obj["extra_info__server"])
            else:
                temp_extra = copy.deepcopy(obj['extra_info__local'])
                temp_extra_info = self.add_queue_and_job_info(temp_extra)
                extra_info__server_temp = copy.deepcopy(
                    obj['extra_info__server'])
                extra_info__server_temp.update(temp_extra_info)
                temp_save_obj["extra_info"] = extra_info__server_temp
                update = True

            for extra_att in self.dimention_extra_attributes:
                if obj[extra_att + "__local"] == obj[extra_att + "__server"]:
                    temp_save_obj[extra_att] = obj[extra_att + "__server"]
                else:
                    temp_save_obj[extra_att] = obj[extra_att + "__local"]
                    update = True

            if update:
                to_save.append(temp_save_obj)
            else:
                no_change.append(temp_save_obj)

        if len(to_save) != 0 and not self.add_new_dimention:
            template = "Dimention resolver {dim} add_new_dimention = False" + \
                ", but trying to save/update dimentions."
            raise Exception(template.format(dim=self.dimention_model_class))

        new_dimentions = self._microservice.parallel_save(
            to_save)
        new_dimentions.extend(no_change)
        self.cached_dimentions = pd.DataFrame(new_dimentions)
        return self.cached_dimentions

    def resolve_hierarchy(self, hierarchy_table: pd.DataFrame):
        """
        Resolve attribute hierarchy: If not equal substitute.

        Args:
            hierarchy_table (pd.DataFrame): Dataframe with the colums
                node_id, child_node_id, extra_info setting hierarchy for
                node_id.

        Kwargs:
            No Kwargs.

        Returns:
            pd.DataFrame: With saved hierarchies.

        Raises:
            Exception('Pusher not adding new hierarchy and hierarchy_table with
                    len !=0 '):
                If the pusher is not allowed to create new hierarchy and
                hierarchy_table with len != 0.

        Example:
            No Example

        """
        if type(hierarchy_table) != pd.DataFrame:
            template = "Argument hierarchy_table for {model_class} " + \
                "resolver must be a pandas.DataFrame"
            raise Exception(template.format(
                model_class=self.hierarchy_model_class))

        validation_hier = hierarchy_table[['node', 'child_node']].groupby(
            ['node', 'child_node']).size().reset_index()
        validation_hier = validation_hier[validation_hier[0] > 1]
        if len(validation_hier):
            validation_hier.rename(columns={0: "count"}, inplace=True)
            template = "There are duplicate node/child_node pairs:\n{values}"
            raise Exception(template.format(values=str(validation_hier)))

        hier_nodes_description = hierarchy_table['node'].tolist()
        server_hier_nodes = self._microservice.list_without_pag(
            self.dimention_model_class, {
                'description__in': hier_nodes_description})
        desc_pk_nodes = dict(
            [(x['description'], x['pk']) for x in server_hier_nodes])

        hier_children_description = hierarchy_table['child_node'].tolist()
        server_hier_children = self._microservice.list_without_pag(
            self.dimention_model_class, {
                'description__in': hier_children_description})
        desc_pk_children = dict(
            [(x['description'], x['pk']) for x in server_hier_children])

        set_hier_nodes_description = set(hier_nodes_description)
        set_hier_children_description = set(hier_children_description)

        not_in_server_nodes = set_hier_nodes_description - \
            set(desc_pk_nodes.keys())
        not_in_server_children = set_hier_children_description - \
            set(desc_pk_children.keys())

        if len(not_in_server_nodes) != 0 or len(not_in_server_children) != 0:
            template = "Some descriptions not found in server:\n" + \
                "not_in_server_nodes: {nodes}\n" + \
                "not_in_server_children: {children}"
            raise Exception(template.format(
                nodes=str(not_in_server_nodes),
                children=str(not_in_server_children)))

        hierarchy_table['node_id'] = hierarchy_table['node'].map(
            desc_pk_nodes)
        hierarchy_table['child_node_id'] = hierarchy_table['child_node'].map(
            desc_pk_children)
        hierarchy_table = hierarchy_table[
            ['node_id', 'child_node_id', 'extra_info']]

        server_hiers = pd.DataFrame(self._microservice.list_without_pag(
            self.hierarchy_model_class, filter_dict={
                'node_id__in': hierarchy_table['node_id'].tolist()}),
            columns=[
                'pk', 'model_class', 'node_id', 'child_node_id', 'extra_info'])

        # Empty hierarchy_table
        if len(server_hiers) == 0 and len(hierarchy_table) == 0:
            return pd.DataFrame([], columns=[
                'pk', 'model_class', 'node_id', 'child_node_id',
                'extra_info'])

        merged_hierarchy = server_hiers.merge(
            hierarchy_table, how="right", on=['node_id', 'child_node_id'],
            suffixes=["__server", "__local"])
        merged_hierarchy.loc[
            merged_hierarchy['model_class'].isna(), "model_class"] = \
            self.hierarchy_model_class

        na_extra_info__server = merged_hierarchy['extra_info__server'].isna()
        merged_hierarchy.loc[na_extra_info__server, 'extra_info__server'] = \
            [{}]*na_extra_info__server.sum()
        na_extra_info__local = merged_hierarchy['extra_info__local'].isna()
        merged_hierarchy.loc[na_extra_info__local, 'extra_info__local'] = \
            [{}]*na_extra_info__local.sum()

        iqual_extra_info = merged_hierarchy[
            ['extra_info__local', 'extra_info__server']].apply(
                lambda row: is_dict_equal(
                    row['extra_info__local'], row['extra_info__server']),
                axis=1)

        # If replace clear all hierarchy that will no be updated
        if self.add_hierarchy == 'replace':
            server_pks = merged_hierarchy['pk'].unique()
            print("server_pks: ", server_pks)
            index_pk = ~pd.isnull(server_pks)
            if index_pk.any():
                server_pks = server_pks[index_pk].tolist()
                to_clear_node_id = hierarchy_table['node_id'].unique().tolist()
                self._microservice.delete_many(
                    self.hierarchy_model_class,
                    filter_dict={'node_id__in': to_clear_node_id},
                    exclude_dict={'pk__in': server_pks})

        # only update hierarchies that have different extra info
        # or are new
        merged_hierarchy.rename(columns={'extra_info__server': 'extra_info'},
                                inplace=True)
        to_add = merged_hierarchy['pk'].isna()
        to_update_add = merged_hierarchy[to_add | ~iqual_extra_info]
        objects_to_post = []
        for obj in to_update_add.to_dict('reccords'):
            obj['extra_info__local'] = self.add_queue_and_job_info(
                obj['extra_info__local'])
            obj['extra_info'].update(obj['extra_info__local'])
            del obj['extra_info__local']

            if math.isnan(obj['pk']):
                del obj['pk']

            objects_to_post.append(obj)

        if self.add_hierarchy == 'fail' and len(objects_to_post):
            template = "Hierarchies being added and updated, " + \
                "but add_hierarchy is fail."
            raise Exception(template)

        self._microservice.parallel_save(objects_to_post)
        server_hiers = pd.DataFrame(self._microservice.list_without_pag(
            self.hierarchy_model_class, filter_dict={
                'node_id__in': hierarchy_table['node_id'].tolist()}),
            columns=[
                'pk', 'model_class', 'node_id', 'child_node_id', 'extra_info'])

        self.cached_hierarchy = pd.DataFrame(server_hiers)
        return self.cached_hierarchy

    def resolve_categories(self, categories: pd.DataFrame):
        """
        Resolve attribute categories for categorical data.

        Args:
            categories (pd.DataFrame): Dataframe with the colums
                value, description, extra_info setting categories for
                categorical variables.

        Kwargs:
            No Kwargs.

        Returns:
            pd.DataFrame: With saved hierarchies.

        Raises:
            Exception('Pusher not adding new hierarchy and hierarchy_table with
                    len !=0 '):
                If the pusher is not allowed to create new hierarchy and
                hierarchy_table with len != 0.

        Example:
            No Example

        """
        if type(categories) != pd.DataFrame:
            template = "Argument categories for {model_class} " + \
                "resolver must be a pandas.DataFrame"
            raise Exception(template.format(
                model_class=self.categorical_value_model_class))

        needed_columns = set(
            ['description', 'extra_info', 'value', 'ordering']) - \
            set(categories.columns)
        if len(needed_columns):
            template = "Categories dataframe missing columns:\natual:{act}" + \
                "; needed: ['description', 'extra_info', 'value', 'ordering']"
            raise Exception(template.format(act=list(categories.columns)))

        dimention = list(set(categories.columns) - set(
            ['description', 'extra_info', 'value', 'ordering']))
        if len(dimention) != 1:
            template = "Categories dataframe with more colums:\n" + \
                "expected: [dimention], description, extra_info, value" + \
                "collums: {collums}"
            raise Exception(
                template.format(collums=list(categories.columns)))

        dimention = dimention[0]
        if dimention not in [
                'attribute', 'geoattribute', 'calendar']:
            template = "Dimention ({dim}) must be in [attribute , " + \
                "geoattribute, calendar]"
            raise Exception(template.format(dim=dimention))

        # if self.add_new_categorical != 'fail':
        #     raise Exception('Add categorical variables not implemented.')

        unique_dimentions = (categories[dimention].unique()).tolist()
        server_dimentions = self._microservice.list_without_pag(
            self.dimention_model_class, filter_dict={
                'description__in': unique_dimentions})
        server_desc_pk = dict([
            (x['description'], x['pk']) for x in server_dimentions])

        # Check if all descriptions are in server.
        not_in_server = set(unique_dimentions) - set(server_desc_pk)
        if len(not_in_server):
            template = "Some categoric dimentions are not at server: {dim}"
            raise Exception(template.format(dim=not_in_server))

        # Check if all descriptions are categorical
        not_categorical = []
        for dim in server_dimentions:
            if dim['type_id'] not in [
                    'FIXED_CATEG', 'FIXED_ORDIN', 'TIME_CATEG', 'TIME_ORDIN']:
                not_categorical.append(dim['description'])
        if len(not_categorical):
            template = "Some of the dimentions are not categorical: {dim}"
            raise Exception(template.format(dim=not_categorical))

        dimention_id = dimention + '_id'
        categories_columns = [
            'pk', 'model_class', dimention_id, 'value', 'ordering',
            'description', 'extra_info']

        categories[dimention_id] = categories[dimention].map(
            server_desc_pk)
        categories = categories[[
            x for x in categories.columns if x in categories_columns]]

        server_pks = [x['pk'] for x in server_dimentions]
        server_categories = self._microservice.list_without_pag(
            self.categorical_value_model_class, filter_dict={
                dimention_id + '__in': server_pks})

        pd_server_categories = pd.DataFrame(
            server_categories, columns=categories_columns)

        if len(categories) == 0 and len(pd_server_categories) == 0:
            return pd.DataFrame([], columns=[
                'pk', 'model_class', dimention_id, 'value', 'ordering',
                'description', 'extra_info'])

        merged_categories = categories.merge(
            pd_server_categories, how="outer",
            on=[dimention_id, 'value'],
            suffixes=['__local', '__server'])

        #######################
        # Fill extra info local
        na_extra_info__local = merged_categories['extra_info__local'].isna()
        merged_categories.loc[
            na_extra_info__local,
            'extra_info__local'] = [{}]*na_extra_info__local.sum()
        # Fill extra info server
        na_extra_info__server = merged_categories['extra_info__server'].isna()
        merged_categories.loc[
            na_extra_info__server,
            'extra_info__server'] = [{}]*na_extra_info__server.sum()
        #######################

        iqual_description = merged_categories[
            'description__local'] == merged_categories['description__server']
        na__desc__local = merged_categories['description__local'].isna()

        iqual_ordering = merged_categories[
            'ordering__local'] == merged_categories['ordering__server']
        na__order__local = merged_categories['ordering__local'].isna()

        iqual_extra_info = merged_categories[
            ['extra_info__local', 'extra_info__server']].apply(
                lambda row: is_dict_equal(
                    row['extra_info__local'], row['extra_info__server']),
                axis=1)

        merged_categories.loc[~na__desc__local, 'description__server'] = \
            merged_categories.loc[~na__desc__local, 'description__local']

        merged_categories.loc[~na__order__local, 'ordering__server'] = \
            merged_categories.loc[~na__order__local, 'ordering__local']

        def update_extra_info(base, update):
            temp = base.copy()
            temp.update(update)
            return temp

        merged_categories['extra_info__server'] = merged_categories[
            ['extra_info__local', 'extra_info__server']].apply(
                lambda row: update_extra_info(
                    row['extra_info__server'], row['extra_info__local']),
                axis=1)

        to_return = merged_categories[[
            'pk', 'model_class', dimention_id, 'value',
            'ordering__server', 'description__server', 'extra_info__server',
        ]].copy()

        to_return.loc[to_return['model_class'].isna(), 'model_class'] = \
            self.categorical_value_model_class
        to_return.rename(columns={
            'ordering__server': 'ordering',
            'description__server': 'description',
            'extra_info__server': 'extra_info'}, inplace=True)

        val_description = to_return[[dimention_id, 'description']].groupby(
            [dimention_id, 'description']).size().reset_index()
        val_value = to_return[[dimention_id, 'value']].groupby(
            [dimention_id, 'value']).size().reset_index()
        val_ordering = to_return[[dimention_id, 'ordering']].groupby(
            [dimention_id, 'ordering']).size().reset_index()

        validation_errors = []
        if (val_description[0] != 1).any():
            val_error = val_description.loc[val_description[0] != 1, :]
            template = "There is duplicated {dim_name} [{dim}] and " + \
                "description [{desc}]"
            for row in val_error.to_dict('reccords'):
                validation_errors.append(template.format(
                    dim_name=dimention_id, dim=row[dimention_id],
                    desc=row['description']))

        if (val_value[0] != 1).any():
            val_error = val_value.loc[val_value[0] != 1, :]
            template = "There is duplicated {dim_name} [{dim}] and " + \
                "value [{desc}]"
            for row in val_error.to_dict('reccords'):
                validation_errors.append(template.format(
                    dim_name=dimention_id, dim=row[dimention_id],
                    desc=row['value']))

        if (val_ordering[0] != 1).any():
            val_error = val_ordering.loc[val_ordering[0] != 1, :]
            template = "There is duplicated {dim_name} [{dim}] and " + \
                "ordering [{desc}]"
            for row in val_error.to_dict('reccords'):
                validation_errors.append(template.format(
                    dim_name=dimention_id, dim=row[dimention_id],
                    desc=row['ordering']))

        if len(validation_errors):
            raise Exception('\n'.join(validation_errors))

        to_add_or_update = ~iqual_description | ~iqual_ordering | \
            ~iqual_extra_info

        if self.add_new_categorical == 'fail' and to_add_or_update.any():
            template = "add_new_categorical is set fail and process is " + \
                "adding/updating categories."
            raise Exception(template)

        nochange_df = to_return.loc[~to_add_or_update, :]
        to_add_update_df = to_return.loc[to_add_or_update, :]

        # avoid conflict when swiping ordering between categories
        to_update = to_add_update_df.loc[~to_add_update_df['pk'].isna(), :]
        for temp_dict in to_update.to_dict('reccords'):
            temp_dict['extra_info'] = self.add_queue_and_job_info(
                temp_dict['extra_info'])
            temp_dict['ordering'] = temp_dict['ordering'] * -1
            self._microservice.save(temp_dict)

        new_updated_categories = []
        for temp_dict in to_add_update_df.to_dict('reccords'):
            if math.isnan(temp_dict['pk']):
                del temp_dict['pk']
            temp_dict['extra_info'] = self.add_queue_and_job_info(
                temp_dict['extra_info'])
            new_updated_categories.append(
                self._microservice.save(temp_dict))
        new_df = pd.DataFrame(new_updated_categories)

        self.cached_category = pd.concat([nochange_df, new_df], sort=False)
        return self.cached_category
