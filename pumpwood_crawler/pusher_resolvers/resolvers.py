"""Resolver module for attibutes."""
from .abstract_resolver import AbstractResolver


class AttributeResolver(AbstractResolver):
    """Attribute resolver."""

    dimention_model_class = 'DescriptionAttribute'
    dimention_extra_attributes = ['type_id']
    categorical_value_model_class = 'CategoricalAttributeDescription'
    hierarchy_model_class = 'AttributeHierarchy'


class CalendarResolver(AbstractResolver):
    """Calendar resolver."""

    dimention_model_class = 'DescriptionCalendar'
    dimention_extra_attributes = ['type_id']
    categorical_value_model_class = 'CategoricalCalendarDescription'
    hierarchy_model_class = 'CalendarHierarchy'


class GeoAreaResolver(AbstractResolver):
    """GeoArea resolver."""

    dimention_model_class = 'DescriptionGeoArea'
    dimention_extra_attributes = []
    categorical_value_model_class = None
    hierarchy_model_class = 'GeoAreaHierarchy'


class GeoAttributeResolver(AbstractResolver):
    """GeoAttribute resolver."""

    dimention_model_class = 'DescriptionGeoAttribute'
    dimention_extra_attributes = ['type_id']
    categorical_value_model_class = 'CategoricalGeoAttributeDescription'
    hierarchy_model_class = 'GeoAttributeHierarchy'


class ModelingUnitResolver(AbstractResolver):
    """ModelingUnit resolver."""

    dimention_model_class = 'DescriptionModelingUnit'
    dimention_extra_attributes = []
    categorical_value_model_class = None
    hierarchy_model_class = 'ModelingUnitHierarchy'
