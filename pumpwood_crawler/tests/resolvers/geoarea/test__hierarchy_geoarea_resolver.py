"""Module to test attribute resolvers."""
# -*- coding: utf-8 -*-
# import requests
# import copy
import pandas as pd
from pumpwood_flaskmisc.testing import TestPumpWood, is_dict_equal
from pusher_resolvers.resolvers import AttributeResolver
from pumpwood_communication.microservices import PumpWoodMicroService
from tests.config.settings import TEST_CONFIG


new_hierarchy = pd.DataFrame([
    {'node': "Hierarquia vazia",
     "child_node": "Shares: Dividends",
     "extra_info": {'chubaca': 'Ahhhhh'}},
    {'node': "Hierarquia vazia",
     "child_node": "Shares: Volume",
     "extra_info": {'teste': 'valor de casa'}},
    {'node': "Hierarquia vazia",
     "child_node": "Shares: High",
     "extra_info": {'upa lala': 'obalala'}},
    {'node': "Hierarquia vazia",
     "child_node": "Shares: Close",
     "extra_info": {'times they are': 'a changing'}},
    {'node': "Hierarquia vazia",
     "child_node": "Shares: Open",
     "extra_info": {}}, ])


update_hierarchy = pd.DataFrame([
    # Change extra info
    {"node": "Hier 1",
     "child_node": "Hier 1_1",
     "extra_info": {"var bool": True}},
    {"node": "Hier 1",
     "child_node": "Hier 1_2",
     "extra_info": {"var string": "tudo bem"}},
    # Add new child
    {"node": "Hier 1_1",
     "child_node": "Shares: High",
     "extra_info": {}},
    {"node": "Hier 1_1",
     "child_node": "Hierarquia vazia",
     "extra_info": {}},
    {"node": "Hier 1_1_1",
     "child_node": "Shares: Open",
     "extra_info": {}},
    # Add new father
    {"node": "Hierarquia vazia",
     "child_node": "Shares: High",
     "extra_info": {}},
    {"node": "Hierarquia vazia",
     "child_node": "Shares: Open",
     "extra_info": {}}, ])


class TestHierarchyAttributeResolverAdd(TestPumpWood):
    """Test Hierarchy Attribute resolvers."""

    load_balancer_address = 'http://localhost:8080/'
    apps_to_regenerate = ['pumpwood-datalake-app']

    def test_new_hierarchy(self):
        """Test adding new hierarchy with resolver: add_hierarchy='add'."""
        microservice = PumpWoodMicroService(**TEST_CONFIG['MICROSERVICE'])
        microservice.login()

        queue_data = microservice.list_one(
            model_class='DatabaseETLQueue', pk=4)
        job_data = microservice.list_one(
            model_class='DatabaseETLJob', pk=queue_data['job_id'])

        att_resolver = AttributeResolver(
            microservice=microservice, queue_data=queue_data,
            job_data=job_data, add_new_dimention=True,
            add_new_categorical='add', add_hierarchy='add')
        resolver_result = att_resolver.resolve_hierarchy(
            hierarchy_table=new_hierarchy.copy())

        pks = resolver_result['node_id'].append(
            resolver_result['child_node_id']).unique().tolist()
        descriptions = microservice.list_without_pag(
            "DescriptionAttribute", filter_dict={'pk__in': pks})
        desc_pk = dict([(x['description'], x['pk']) for x in descriptions])
        temp_new_hierarchy = new_hierarchy.copy()
        temp_new_hierarchy['node_id'] = temp_new_hierarchy['node'].map(
            desc_pk)
        temp_new_hierarchy['child_node_id'] = temp_new_hierarchy[
            'child_node'].map(desc_pk)
        temp_new_hierarchy = temp_new_hierarchy[[
            'node_id', 'child_node_id']]

        merged = resolver_result.merge(temp_new_hierarchy, how="left")
        self.assertEqual(len(merged), len(temp_new_hierarchy))

    def test_update_hierarchy(self):
        """Test adding new hierarchy with resolver: add_hierarchy='add'."""
        microservice = PumpWoodMicroService(**TEST_CONFIG['MICROSERVICE'])
        microservice.login()

        queue_data = microservice.list_one(
            model_class='DatabaseETLQueue', pk=4)
        job_data = microservice.list_one(
            model_class='DatabaseETLJob', pk=queue_data['job_id'])

        att_resolver = AttributeResolver(
            microservice=microservice, queue_data=queue_data,
            job_data=job_data, add_new_dimention=True,
            add_new_categorical='add', add_hierarchy='add')
        resolver_result = att_resolver.resolve_hierarchy(
            hierarchy_table=update_hierarchy.copy())

        pks = resolver_result['node_id'].append(
            resolver_result['child_node_id']).unique().tolist()
        descriptions = microservice.list_without_pag(
            "DescriptionAttribute", filter_dict={'pk__in': pks})
        desc_pk = dict([(x['description'], x['pk']) for x in descriptions])
        temp_update_hierarchy = update_hierarchy.copy()
        temp_update_hierarchy['node_id'] = temp_update_hierarchy['node'].map(
            desc_pk)
        temp_update_hierarchy['child_node_id'] = temp_update_hierarchy[
            'child_node'].map(desc_pk)
        temp_update_hierarchy = temp_update_hierarchy[[
            'node_id', 'child_node_id']]

        merged = resolver_result.merge(temp_update_hierarchy, how="left")
        self.assertGreater(len(merged), len(temp_update_hierarchy))

    def test_nochange_hierarchy(self):
        """Test adding nochange hierarchy with resolver:add_hierarchy='add'."""
        microservice = PumpWoodMicroService(**TEST_CONFIG['MICROSERVICE'])
        microservice.login()

        queue_data = microservice.list_one(
            model_class='DatabaseETLQueue', pk=4)
        job_data = microservice.list_one(
            model_class='DatabaseETLJob', pk=queue_data['job_id'])

        nochange_hierarchy = microservice.list_without_pag(
            "AttributeHierarchy", filter_dict={"node_id__in": [11, 12]})
        pd_nochange_hierarchy = pd.DataFrame(nochange_hierarchy)
        node_id = pd_nochange_hierarchy['node_id']
        child_node_id = pd_nochange_hierarchy['child_node_id']
        to_desc_pk = (node_id.append(child_node_id)).unique().tolist()
        desc_hierarchy = microservice.list_without_pag(
            "DescriptionAttribute", filter_dict={"pk__in": to_desc_pk})
        pk_desc = dict([(x['pk'], x['description'])for x in desc_hierarchy])
        pd_nochange_hierarchy['node'] = pd_nochange_hierarchy[
            'node_id'].map(pk_desc)
        pd_nochange_hierarchy['child_node'] = pd_nochange_hierarchy[
            'child_node_id'].map(pk_desc)

        pd_nochange_hierarchy = pd_nochange_hierarchy[
            ['node', 'child_node', 'extra_info']]

        att_resolver = AttributeResolver(
            microservice=microservice, queue_data=queue_data,
            job_data=job_data, add_new_dimention=True,
            add_new_categorical='add', add_hierarchy='add')
        resolver_result = att_resolver.resolve_hierarchy(
            hierarchy_table=pd_nochange_hierarchy.copy())

        pks = resolver_result['node_id'].append(
            resolver_result['child_node_id']).unique().tolist()
        descriptions = microservice.list_without_pag(
            "DescriptionAttribute", filter_dict={'pk__in': pks})
        desc_pk = dict([(x['description'], x['pk']) for x in descriptions])
        temp_pd_nochange_hierarchy = pd_nochange_hierarchy.copy()
        temp_pd_nochange_hierarchy['node_id'] = temp_pd_nochange_hierarchy[
            'node'].map(desc_pk)
        temp_pd_nochange_hierarchy['child_node_id'] = \
            temp_pd_nochange_hierarchy['child_node'].map(desc_pk)
        temp_pd_nochange_hierarchy = temp_pd_nochange_hierarchy[[
            'node_id', 'child_node_id', 'extra_info']]

        merged = temp_pd_nochange_hierarchy.merge(
            resolver_result, on=['node_id', 'child_node_id'],
            suffixes=["__temp", "__resolver"])
        self.assertEqual(len(merged), len(temp_pd_nochange_hierarchy))

        iqual_extra_info = merged[
            ['extra_info__temp', 'extra_info__resolver']].apply(
                lambda row: is_dict_equal(
                    row['extra_info__temp'], row['extra_info__resolver']),
                axis=1)
        self.assertTrue(iqual_extra_info.all())


class TestHierarchyAttributeResolverReplace(TestPumpWood):
    """Test Hierarchy Attribute resolvers to replace."""

    load_balancer_address = 'http://localhost:8080/'
    apps_to_regenerate = ['pumpwood-datalake-app']

    def test_new_hierarchy(self):
        """Test adding new hierarchy with resolver:add_hierarchy='replace'."""
        microservice = PumpWoodMicroService(**TEST_CONFIG['MICROSERVICE'])
        microservice.login()

        queue_data = microservice.list_one(
            model_class='DatabaseETLQueue', pk=4)
        job_data = microservice.list_one(
            model_class='DatabaseETLJob', pk=queue_data['job_id'])

        att_resolver = AttributeResolver(
            microservice=microservice, queue_data=queue_data,
            job_data=job_data, add_new_dimention=True,
            add_new_categorical='add', add_hierarchy='replace')
        resolver_result = att_resolver.resolve_hierarchy(
            hierarchy_table=new_hierarchy.copy())

        pks = resolver_result['node_id'].append(
            resolver_result['child_node_id']).unique().tolist()
        descriptions = microservice.list_without_pag(
            "DescriptionAttribute", filter_dict={'pk__in': pks})
        desc_pk = dict([(x['description'], x['pk']) for x in descriptions])
        temp_new_hierarchy = new_hierarchy.copy()
        temp_new_hierarchy['node_id'] = temp_new_hierarchy['node'].map(
            desc_pk)
        temp_new_hierarchy['child_node_id'] = temp_new_hierarchy[
            'child_node'].map(desc_pk)
        temp_new_hierarchy = temp_new_hierarchy[[
            'node_id', 'child_node_id']]

        merged = resolver_result.merge(temp_new_hierarchy, how="left")
        self.assertEqual(len(merged), len(temp_new_hierarchy))

    def test_update_hierarchy(self):
        """Test update hierarchy with resolver:add_hierarchy='replace'."""
        microservice = PumpWoodMicroService(**TEST_CONFIG['MICROSERVICE'])
        microservice.login()

        queue_data = microservice.list_one(
            model_class='DatabaseETLQueue', pk=4)
        job_data = microservice.list_one(
            model_class='DatabaseETLJob', pk=queue_data['job_id'])

        att_resolver = AttributeResolver(
            microservice=microservice, queue_data=queue_data,
            job_data=job_data, add_new_dimention=True,
            add_new_categorical='add', add_hierarchy='replace')
        resolver_result = att_resolver.resolve_hierarchy(
            hierarchy_table=update_hierarchy.copy())

        pks = resolver_result['node_id'].append(
            resolver_result['child_node_id']).unique().tolist()
        descriptions = microservice.list_without_pag(
            "DescriptionAttribute", filter_dict={'pk__in': pks})
        desc_pk = dict([(x['description'], x['pk']) for x in descriptions])
        temp_update_hierarchy = update_hierarchy.copy()
        temp_update_hierarchy['node_id'] = temp_update_hierarchy['node'].map(
            desc_pk)
        temp_update_hierarchy['child_node_id'] = temp_update_hierarchy[
            'child_node'].map(desc_pk)
        temp_update_hierarchy = temp_update_hierarchy[[
            'node_id', 'child_node_id']]

        merged = resolver_result.merge(temp_update_hierarchy, how="left")
        self.assertEqual(len(merged), len(temp_update_hierarchy))

    def test_nochange_hierarchy(self):
        """Test nochange hierarchy with resolver:add_hierarchy='replace'."""
        microservice = PumpWoodMicroService(**TEST_CONFIG['MICROSERVICE'])
        microservice.login()

        queue_data = microservice.list_one(
            model_class='DatabaseETLQueue', pk=4)
        job_data = microservice.list_one(
            model_class='DatabaseETLJob', pk=queue_data['job_id'])

        nochange_hierarchy = microservice.list_without_pag(
            "AttributeHierarchy", filter_dict={"node_id__in": [11, 12]})
        pd_nochange_hierarchy = pd.DataFrame(nochange_hierarchy)
        node_id = pd_nochange_hierarchy['node_id']
        child_node_id = pd_nochange_hierarchy['child_node_id']
        to_desc_pk = (node_id.append(child_node_id)).unique().tolist()
        desc_hierarchy = microservice.list_without_pag(
            "DescriptionAttribute", filter_dict={"pk__in": to_desc_pk})
        pk_desc = dict([(x['pk'], x['description'])for x in desc_hierarchy])
        pd_nochange_hierarchy['node'] = pd_nochange_hierarchy[
            'node_id'].map(pk_desc)
        pd_nochange_hierarchy['child_node'] = pd_nochange_hierarchy[
            'child_node_id'].map(pk_desc)

        pd_nochange_hierarchy = pd_nochange_hierarchy[
            ['node', 'child_node', 'extra_info']]

        att_resolver = AttributeResolver(
            microservice=microservice, queue_data=queue_data,
            job_data=job_data, add_new_dimention=True,
            add_new_categorical='add', add_hierarchy='replace')
        resolver_result = att_resolver.resolve_hierarchy(
            hierarchy_table=pd_nochange_hierarchy.copy())

        pks = resolver_result['node_id'].append(
            resolver_result['child_node_id']).unique().tolist()
        descriptions = microservice.list_without_pag(
            "DescriptionAttribute", filter_dict={'pk__in': pks})
        desc_pk = dict([(x['description'], x['pk']) for x in descriptions])
        temp_pd_nochange_hierarchy = pd_nochange_hierarchy.copy()
        temp_pd_nochange_hierarchy['node_id'] = temp_pd_nochange_hierarchy[
            'node'].map(desc_pk)
        temp_pd_nochange_hierarchy['child_node_id'] = \
            temp_pd_nochange_hierarchy['child_node'].map(desc_pk)
        temp_pd_nochange_hierarchy = temp_pd_nochange_hierarchy[[
            'node_id', 'child_node_id', 'extra_info']]

        merged = temp_pd_nochange_hierarchy.merge(
            resolver_result, on=['node_id', 'child_node_id'],
            suffixes=["__temp", "__resolver"])
        self.assertEqual(len(merged), len(temp_pd_nochange_hierarchy))

        iqual_extra_info = merged[
            ['extra_info__temp', 'extra_info__resolver']].apply(
                lambda row: is_dict_equal(
                    row['extra_info__temp'], row['extra_info__resolver']),
                axis=1)
        self.assertTrue(iqual_extra_info.all())


class TestHierarchyAttributeResolverFail(TestPumpWood):
    """Test Hierarchy Attribute resolvers to fail."""

    load_balancer_address = 'http://localhost:8080/'
    apps_to_regenerate = ['pumpwood-datalake-app']

    def test_new_hierarchy(self):
        """Test adding new hierarchy with resolver:add_hierarchy='fail'."""
        microservice = PumpWoodMicroService(**TEST_CONFIG['MICROSERVICE'])
        microservice.login()

        queue_data = microservice.list_one(
            model_class='DatabaseETLQueue', pk=4)
        job_data = microservice.list_one(
            model_class='DatabaseETLJob', pk=queue_data['job_id'])

        att_resolver = AttributeResolver(
            microservice=microservice, queue_data=queue_data,
            job_data=job_data, add_new_dimention=True,
            add_new_categorical='add', add_hierarchy='fail')

        with self.assertRaises(Exception):
            att_resolver.resolve_hierarchy(
                hierarchy_table=new_hierarchy.copy())

    def test_update_hierarchy(self):
        """Test update new hierarchy with resolver:add_hierarchy='fail'."""
        microservice = PumpWoodMicroService(**TEST_CONFIG['MICROSERVICE'])
        microservice.login()

        queue_data = microservice.list_one(
            model_class='DatabaseETLQueue', pk=4)
        job_data = microservice.list_one(
            model_class='DatabaseETLJob', pk=queue_data['job_id'])

        att_resolver = AttributeResolver(
            microservice=microservice, queue_data=queue_data,
            job_data=job_data, add_new_dimention=True,
            add_new_categorical='add', add_hierarchy='fail')

        with self.assertRaises(Exception):
            att_resolver.resolve_hierarchy(
                hierarchy_table=update_hierarchy.copy())

    def test_nochange_hierarchy(self):
        """Test nochange hierarchy with resolver:add_hierarchy='fail'."""
        microservice = PumpWoodMicroService(**TEST_CONFIG['MICROSERVICE'])
        microservice.login()

        queue_data = microservice.list_one(
            model_class='DatabaseETLQueue', pk=4)
        job_data = microservice.list_one(
            model_class='DatabaseETLJob', pk=queue_data['job_id'])

        nochange_hierarchy = microservice.list_without_pag(
            "AttributeHierarchy", filter_dict={"node_id__in": [11, 12]})
        pd_nochange_hierarchy = pd.DataFrame(nochange_hierarchy)
        node_id = pd_nochange_hierarchy['node_id']
        child_node_id = pd_nochange_hierarchy['child_node_id']
        to_desc_pk = (node_id.append(child_node_id)).unique().tolist()
        desc_hierarchy = microservice.list_without_pag(
            "DescriptionAttribute", filter_dict={"pk__in": to_desc_pk})
        pk_desc = dict([(x['pk'], x['description'])for x in desc_hierarchy])
        pd_nochange_hierarchy['node'] = pd_nochange_hierarchy[
            'node_id'].map(pk_desc)
        pd_nochange_hierarchy['child_node'] = pd_nochange_hierarchy[
            'child_node_id'].map(pk_desc)

        pd_nochange_hierarchy = pd_nochange_hierarchy[
            ['node', 'child_node', 'extra_info']]

        att_resolver = AttributeResolver(
            microservice=microservice, queue_data=queue_data,
            job_data=job_data, add_new_dimention=True,
            add_new_categorical='add', add_hierarchy='fail')
        resolver_result = att_resolver.resolve_hierarchy(
            hierarchy_table=pd_nochange_hierarchy.copy())

        pks = resolver_result['node_id'].append(
            resolver_result['child_node_id']).unique().tolist()
        descriptions = microservice.list_without_pag(
            "DescriptionAttribute", filter_dict={'pk__in': pks})
        desc_pk = dict([(x['description'], x['pk']) for x in descriptions])
        temp_pd_nochange_hierarchy = pd_nochange_hierarchy.copy()
        temp_pd_nochange_hierarchy['node_id'] = temp_pd_nochange_hierarchy[
            'node'].map(desc_pk)
        temp_pd_nochange_hierarchy['child_node_id'] = \
            temp_pd_nochange_hierarchy['child_node'].map(desc_pk)
        temp_pd_nochange_hierarchy = temp_pd_nochange_hierarchy[[
            'node_id', 'child_node_id', 'extra_info']]

        merged = temp_pd_nochange_hierarchy.merge(
            resolver_result, on=['node_id', 'child_node_id'],
            suffixes=["__temp", "__resolver"])
        self.assertEqual(len(merged), len(temp_pd_nochange_hierarchy))

        iqual_extra_info = merged[
            ['extra_info__temp', 'extra_info__resolver']].apply(
                lambda row: is_dict_equal(
                    row['extra_info__temp'], row['extra_info__resolver']),
                axis=1)
        self.assertTrue(iqual_extra_info.all())
