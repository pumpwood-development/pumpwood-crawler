"""Module to test calendar resolvers."""
# -*- coding: utf-8 -*-
# import requests
# import copy
import pandas as pd
from pumpwood_flaskmisc.testing import TestPumpWood
from pusher_resolvers.resolvers import CalendarResolver
from pumpwood_communication.microservices import PumpWoodMicroService
from tests.config.settings import TEST_CONFIG


new_categories = pd.DataFrame([
    {'calendar': "Categoria vazia.",
     'value': 1, "ordering": 1,
     'description': 'Uma data 1',
     'extra_info': {"chubaca chcuhu": True}},
    {'calendar': "Categoria vazia.",
     'value': 2, "ordering": 2,
     'description': 'Uma data 2',
     'extra_info': {"chubaca chcuhu": 10}},
    {'calendar': "Categoria vazia.",
     'value': 3, "ordering": 3,
     'description': 'Uma data 3',
     'extra_info': {"chubaca não chuchu": 1}},
    {'calendar': "Categoria vazia.",
     'value': 4, "ordering": 4,
     'description': 'Uma data 4',
     'extra_info': {"chubaca não chuchu": 2}}, ])

update_categories = pd.DataFrame([
    # Swipe
    {'calendar': "Sazonalidade aditiva mensal.",
     'value': 1, "ordering": 2,
     'description': 'Janeiro',
     'extra_info': {"sazonalidade": "mesal"}},
    {'calendar': "Sazonalidade aditiva mensal.",
     'value': 2, "ordering": 1,
     'description': 'Fevereiro',
     'extra_info': {"sazonalidade": "mesal"}},

    # Change extra info
    {'calendar': "Sazonalidade aditiva mensal.",
     'value': 3, "ordering": 3,
     'description': 'Março',
     'extra_info': {"chubaca não chuchu": 3}},

    # Send to new ordering value
    {'calendar': "Sazonalidade aditiva mensal.",
     'value': 4, "ordering": 20,
     'description': 'Abril',
     'extra_info': {"chubaca não chuchu": 4}}, ])


class TestCatericalCalendarResolver(TestPumpWood):
    """Test Hierarchy Attribute resolvers."""

    load_balancer_address = 'http://localhost:8080/'
    apps_to_regenerate = ['pumpwood-datalake-app']

    # session = requests.session()
    # session.headers.update(
    #     {'Authorization': '6cab4ba54bb8f92af7e135d7ad8c7d70d4555f7f'})
    def test_add_category(self):
        """Test adding new calendar categories with resolver."""
        microservice = PumpWoodMicroService(**TEST_CONFIG['MICROSERVICE'])
        microservice.login()

        queue_data = microservice.list_one(
            model_class='DatabaseETLQueue', pk=4)
        job_data = microservice.list_one(
            model_class='DatabaseETLJob', pk=queue_data['job_id'])

        cal_resolver = CalendarResolver(
            microservice=microservice, queue_data=queue_data,
            job_data=job_data, add_new_dimention=True,
            add_new_categorical='add', add_hierarchy='add')

        result_resolver = cal_resolver.resolve_categories(
            categories=new_categories.copy())

        descriptions = microservice.list_without_pag(
            model_class="DescriptionCalendar",
            filter_dict={"description__in": new_categories[
                'calendar'].unique().tolist()})

        desc_pk = dict([
            (x['description'], x['pk']) for x in descriptions])
        temp_new_categories = new_categories.copy()
        temp_new_categories['calendar_id'] = temp_new_categories[
            'calendar'].map(desc_pk)
        temp_new_categories = temp_new_categories[[
            'ordering', 'value', 'calendar_id', 'description']]
        merge_result = result_resolver.merge(temp_new_categories)
        self.assertEqual(len(merge_result), len(new_categories))

    def test_fail_add_category(self):
        """Test fail adding new calendar categories with resolver."""
        microservice = PumpWoodMicroService(**TEST_CONFIG['MICROSERVICE'])
        microservice.login()

        queue_data = microservice.list_one(
            model_class='DatabaseETLQueue', pk=4)
        job_data = microservice.list_one(
            model_class='DatabaseETLJob', pk=queue_data['job_id'])

        cal_resolver = CalendarResolver(
            microservice=microservice, queue_data=queue_data,
            job_data=job_data, add_new_dimention=True,
            add_new_categorical='fail', add_hierarchy='add')

        with self.assertRaises(Exception):
            cal_resolver.resolve_categories(
                categories=new_categories.copy())

    def test_update_category(self):
        """Test update calendar categories with resolver."""
        microservice = PumpWoodMicroService(**TEST_CONFIG['MICROSERVICE'])
        microservice.login()

        queue_data = microservice.list_one(
            model_class='DatabaseETLQueue', pk=4)
        job_data = microservice.list_one(
            model_class='DatabaseETLJob', pk=queue_data['job_id'])

        cal_resolver = CalendarResolver(
            microservice=microservice, queue_data=queue_data,
            job_data=job_data, add_new_dimention=True,
            add_new_categorical='add', add_hierarchy='add')
        result_resolver = cal_resolver.resolve_categories(
            categories=update_categories.copy())

        descriptions = microservice.list_without_pag(
            model_class="DescriptionCalendar",
            filter_dict={"description__in": update_categories[
                'calendar'].unique().tolist()})

        desc_pk = dict([
            (x['description'], x['pk']) for x in descriptions])
        temp_update_categories = update_categories.copy()
        temp_update_categories['calendar_id'] = temp_update_categories[
            'calendar'].map(desc_pk)
        temp_update_categories = temp_update_categories[[
            'ordering', 'value', 'calendar_id', 'description']]
        merge_result = result_resolver.merge(temp_update_categories)
        self.assertEqual(len(merge_result), len(update_categories))

    def test_fail_update_category(self):
        """Test fail updating calendar categories with resolver."""
        microservice = PumpWoodMicroService(**TEST_CONFIG['MICROSERVICE'])
        microservice.login()

        queue_data = microservice.list_one(
            model_class='DatabaseETLQueue', pk=4)
        job_data = microservice.list_one(
            model_class='DatabaseETLJob', pk=queue_data['job_id'])

        cal_resolver = CalendarResolver(
            microservice=microservice, queue_data=queue_data,
            job_data=job_data, add_new_dimention=True,
            add_new_categorical='fail', add_hierarchy='add')

        with self.assertRaises(Exception):
            cal_resolver.resolve_categories(
                categories=update_categories.copy())

    def test_nochange_category(self):
        """Test no changing calendar categories with resolver."""
        microservice = PumpWoodMicroService(**TEST_CONFIG['MICROSERVICE'])
        microservice.login()

        queue_data = microservice.list_one(
            model_class='DatabaseETLQueue', pk=4)
        job_data = microservice.list_one(
            model_class='DatabaseETLJob', pk=queue_data['job_id'])

        cat_calendars = microservice.list_without_pag(
            model_class="CategoricalCalendarDescription",
            filter_dict={"calendar_id": 35})
        df_cat_calendars = pd.DataFrame(cat_calendars)
        to_list = df_cat_calendars['calendar_id'].unique().tolist()

        cat_calendars = microservice.list_without_pag(
            model_class="DescriptionCalendar",
            filter_dict={"pk__in": to_list})
        map_calendar_id = dict([
            (x['pk'], x['description'])
            for x in cat_calendars])
        df_cat_calendars['calendar'] = df_cat_calendars[
            'calendar_id'].map(map_calendar_id)
        nochange_categories = df_cat_calendars[[
            'calendar', 'description', 'value', 'ordering', 'extra_info']]

        cal_resolver = CalendarResolver(
            microservice=microservice, queue_data=queue_data,
            job_data=job_data, add_new_dimention=True,
            add_new_categorical='add', add_hierarchy='add')
        result_resolver = cal_resolver.resolve_categories(
            categories=nochange_categories.copy())

        descriptions = microservice.list_without_pag(
            model_class="DescriptionCalendar",
            filter_dict={"description__in": nochange_categories[
                'calendar'].unique().tolist()})

        desc_pk = dict([
            (x['description'], x['pk']) for x in descriptions])
        temp_nochange_categories = nochange_categories.copy()
        temp_nochange_categories['calendar_id'] = temp_nochange_categories[
            'calendar'].map(desc_pk)
        temp_nochange_categories = temp_nochange_categories[[
            'ordering', 'value', 'calendar_id', 'description']]
        merge_result = result_resolver.merge(temp_nochange_categories)
        self.assertEqual(len(merge_result), len(nochange_categories))

    def test_notfail_nochange_category(self):
        """Test not fail no changing calendar categories with resolver."""
        microservice = PumpWoodMicroService(**TEST_CONFIG['MICROSERVICE'])
        microservice.login()

        queue_data = microservice.list_one(
            model_class='DatabaseETLQueue', pk=4)
        job_data = microservice.list_one(
            model_class='DatabaseETLJob', pk=queue_data['job_id'])

        cat_calendars = microservice.list_without_pag(
            model_class="CategoricalCalendarDescription",
            filter_dict={"calendar_id": 35})
        df_cat_calendars = pd.DataFrame(cat_calendars)
        to_list = df_cat_calendars['calendar_id'].unique().tolist()

        cat_calendars = microservice.list_without_pag(
            model_class="DescriptionCalendar",
            filter_dict={"pk__in": to_list})
        map_calendar_id = dict([
            (x['pk'], x['description'])
            for x in cat_calendars])
        df_cat_calendars['calendar'] = df_cat_calendars[
            'calendar_id'].map(map_calendar_id)
        nochange_categories = df_cat_calendars[[
            'calendar', 'description', 'value', 'ordering', 'extra_info']]

        cal_resolver = CalendarResolver(
            microservice=microservice, queue_data=queue_data,
            job_data=job_data, add_new_dimention=True,
            add_new_categorical='fail', add_hierarchy='add')
        result_resolver = cal_resolver.resolve_categories(
            categories=nochange_categories.copy())

        descriptions = microservice.list_without_pag(
            model_class="DescriptionCalendar",
            filter_dict={"description__in": nochange_categories[
                'calendar'].unique().tolist()})

        desc_pk = dict([
            (x['description'], x['pk']) for x in descriptions])
        temp_nochange_categories = nochange_categories.copy()
        temp_nochange_categories['calendar_id'] = temp_nochange_categories[
            'calendar'].map(desc_pk)
        temp_nochange_categories = temp_nochange_categories[[
            'ordering', 'value', 'calendar_id', 'description']]
        merge_result = result_resolver.merge(temp_nochange_categories)
        self.assertEqual(len(merge_result), len(nochange_categories))
