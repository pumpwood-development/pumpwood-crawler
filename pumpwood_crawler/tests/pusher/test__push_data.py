# -*- coding: utf-8 -*-
"""Module to test attribute resolvers."""
# import requests
# import copy
import pandas as pd
import simplejson as json
from pumpwood_flaskmisc.testing import TestPumpWood
from pumpwood_communication.microservices import PumpWoodMicroService
from tests.config.settings import TEST_CONFIG

from pusher import PusherClass


class CheckPusherClass(PusherClass):
    """Pusher to test."""

    job_model_class = "DatabaseETLJob"
    queue_model_class = "DatabaseETLQueue"


class TestPushData(TestPumpWood):
    """Test Hierarchy Attribute resolvers."""

    load_balancer_address = 'http://localhost:8080/'
    apps_to_regenerate = ['pumpwood-datalake-app']

    def load_processed_data(self, list_of_data):
        """Create the processed data."""
        processed_data = {}
        for data in list_of_data:
            print("loading {file} data...".format(file=data))
            file_path = "tests/pusher/database/{file_name}.xlsx"
            df = pd.read_excel(file_path.format(file_name=data),
                               sheetname='Sheet1')
            to_loads = [
                col for col in df.columns
                if col in ["dimentions", "extra_info"]]

            for x in to_loads:
                df[x] = df[x].apply(json.loads)

            processed_data[data] = df
        return processed_data

    def test_push_all_variables(self):
        """Test push all data."""
        microservice = PumpWoodMicroService(**TEST_CONFIG['MICROSERVICE'])
        microservice.login()

        list_of_data = [
            'modeling_unit_dimention',
            'modeling_unit_hierarchy',
            'geoarea_dimention',
            'geoarea_hierarchy',
            'attribute_dimention',
            'attribute_categorical',
            'attribute_hierarchy',
            'database_categorical_data',
            'database_data',
            'geoattribute_dimention',
            'geoattribute_categorical',
            'geoattribute_hierarchy',
            'geodatabase_categorical_data',
            'geodatabase_data',
            'calendar_dimention',
            'calendar_categorical',
            'calendar_hierarchy',
            'calendar_categorical_data',
            'calendar_data']
        processed_data = self.load_processed_data(list_of_data)

        queue_data = microservice.list_one(
            model_class='DatabaseETLQueue', pk=4)
        job_data = microservice.list_one(
            model_class='DatabaseETLJob', pk=queue_data['job_id'])

        # Dimentions
        job_data["default_parameters"]["add_new_modeling_unit"] = True
        job_data["default_parameters"]["add_new_geoarea"] = True
        job_data["default_parameters"]["add_new_attribute"] = True
        job_data["default_parameters"]["add_new_geoatribute"] = True
        job_data["default_parameters"]["add_new_calendar"] = True

        # Hierarchies
        job_data["default_parameters"]["add_hierarchy_modeling_unit"] = 'add'
        job_data["default_parameters"]["add_hierarchy_geoarea"] = 'add'
        job_data["default_parameters"]["add_hierarchy_attribute"] = 'add'
        job_data["default_parameters"]["add_hierarchy_calendar"] = 'add'
        job_data["default_parameters"]["add_hierarchy_geoatribute"] = 'add'

        # Categories
        job_data["default_parameters"]["add_new_categorical_attribute"] = 'add'
        job_data["default_parameters"]["add_new_categorical_calendar"] = 'add'
        job_data["default_parameters"][
            "add_new_categorical_geoatribute"] = 'add'

        pusher = CheckPusherClass(
            database_config=TEST_CONFIG["DATABASE"],
            microservice_config=TEST_CONFIG["MICROSERVICE"],
            storage_credencials=TEST_CONFIG["STORAGE"], )

        pusher.push_data(
            processed_data=processed_data,
            queue_data=queue_data,
            job_data=job_data,)

    def test_push_geo_variables(self):
        """Test push only geography data."""
        microservice = PumpWoodMicroService(**TEST_CONFIG['MICROSERVICE'])
        microservice.login()

        list_of_data = [
            'geoarea_dimention',
            'geoarea_hierarchy',
            'geoattribute_dimention',
            'geoattribute_categorical',
            'geoattribute_hierarchy',
            'geodatabase_categorical_data',
            'geodatabase_data', ]
        processed_data = self.load_processed_data(list_of_data)

        queue_data = microservice.list_one(
            model_class='DatabaseETLQueue', pk=4)
        job_data = microservice.list_one(
            model_class='DatabaseETLJob', pk=queue_data['job_id'])

        # Dimentions
        # job_data["default_parameters"]["add_new_modeling_unit"] = True
        job_data["default_parameters"]["add_new_geoarea"] = True
        # job_data["default_parameters"]["add_new_attribute"] = True
        job_data["default_parameters"]["add_new_geoatribute"] = True
        # job_data["default_parameters"]["add_new_calendar"] = True

        # Hierarchies
        # job_data["default_parameters"]["add_hierarchy_modeling_unit"] = 'add'
        job_data["default_parameters"]["add_hierarchy_geoarea"] = 'add'
        # job_data["default_parameters"]["add_hierarchy_attribute"] = 'add'
        # job_data["default_parameters"]["add_hierarchy_calendar"] = 'add'
        job_data["default_parameters"]["add_hierarchy_geoatribute"] = 'add'

        # Categories
        # job_data["default_parameters"][
        #   "add_new_categorical_attribute"] = 'add'
        # job_data["default_parameters"][
        #   "add_new_categorical_calendar"] = 'add'
        job_data["default_parameters"][
            "add_new_categorical_geoatribute"] = 'add'

        pusher = CheckPusherClass(
            database_config=TEST_CONFIG["DATABASE"],
            microservice_config=TEST_CONFIG["MICROSERVICE"],
            storage_credencials=TEST_CONFIG["STORAGE"], )

        pusher.push_data(
            processed_data=processed_data,
            queue_data=queue_data,
            job_data=job_data,)

    def test_push_att_variables(self):
        """Test push attribute data."""
        microservice = PumpWoodMicroService(**TEST_CONFIG['MICROSERVICE'])
        microservice.login()

        list_of_data = [
            'modeling_unit_dimention',
            'modeling_unit_hierarchy',
            'geoarea_dimention',
            'geoarea_hierarchy',
            'attribute_dimention',
            'attribute_categorical',
            'attribute_hierarchy',
            'database_categorical_data',
            'database_data', ]
        processed_data = self.load_processed_data(list_of_data)

        queue_data = microservice.list_one(
            model_class='DatabaseETLQueue', pk=4)
        job_data = microservice.list_one(
            model_class='DatabaseETLJob', pk=queue_data['job_id'])

        # Dimentions
        job_data["default_parameters"]["add_new_modeling_unit"] = True
        job_data["default_parameters"]["add_new_geoarea"] = True
        job_data["default_parameters"]["add_new_attribute"] = True
        # job_data["default_parameters"]["add_new_geoatribute"] = True
        # job_data["default_parameters"]["add_new_calendar"] = True

        # Hierarchies
        job_data["default_parameters"]["add_hierarchy_modeling_unit"] = 'add'
        job_data["default_parameters"]["add_hierarchy_geoarea"] = 'add'
        job_data["default_parameters"]["add_hierarchy_attribute"] = 'add'
        # job_data["default_parameters"]["add_hierarchy_calendar"] = 'add'
        # job_data["default_parameters"]["add_hierarchy_geoatribute"] = 'add'

        # Categories
        job_data["default_parameters"]["add_new_categorical_attribute"] = 'add'
        # job_data["default_parameters"][
        #   "add_new_categorical_calendar"] = 'add'
        # job_data["default_parameters"][
        #   "add_new_categorical_geoatribute"] = 'add'

        pusher = CheckPusherClass(
            database_config=TEST_CONFIG["DATABASE"],
            microservice_config=TEST_CONFIG["MICROSERVICE"],
            storage_credencials=TEST_CONFIG["STORAGE"], )

        pusher.push_data(
            processed_data=processed_data,
            queue_data=queue_data,
            job_data=job_data,)

    def test_push_calendar_variables(self):
        """Test push calendar data."""
        microservice = PumpWoodMicroService(**TEST_CONFIG['MICROSERVICE'])
        microservice.login()

        list_of_data = [
            'calendar_dimention',
            'calendar_categorical',
            'calendar_hierarchy',
            'calendar_categorical_data',
            'calendar_data']
        processed_data = self.load_processed_data(list_of_data)

        queue_data = microservice.list_one(
            model_class='DatabaseETLQueue', pk=4)
        job_data = microservice.list_one(
            model_class='DatabaseETLJob', pk=queue_data['job_id'])

        # Dimentions
        # job_data["default_parameters"]["add_new_modeling_unit"] = True
        # job_data["default_parameters"]["add_new_geoarea"] = True
        # job_data["default_parameters"]["add_new_attribute"] = True
        # job_data["default_parameters"]["add_new_geoatribute"] = True
        job_data["default_parameters"]["add_new_calendar"] = True

        # Hierarchies
        # job_data["default_parameters"]["add_hierarchy_modeling_unit"] = 'add'
        # job_data["default_parameters"]["add_hierarchy_geoarea"] = 'add'
        # job_data["default_parameters"]["add_hierarchy_attribute"] = 'add'
        job_data["default_parameters"]["add_hierarchy_calendar"] = 'add'
        # job_data["default_parameters"]["add_hierarchy_geoatribute"] = 'add'

        # Categories
        # job_data["default_parameters"][
        #   "add_new_categorical_attribute"] = 'add'
        job_data["default_parameters"]["add_new_categorical_calendar"] = 'add'
        # job_data["default_parameters"][
        #   "add_new_categorical_geoatribute"] = 'add'

        pusher = CheckPusherClass(
            database_config=TEST_CONFIG["DATABASE"],
            microservice_config=TEST_CONFIG["MICROSERVICE"],
            storage_credencials=TEST_CONFIG["STORAGE"], )

        pusher.push_data(
            processed_data=processed_data,
            queue_data=queue_data,
            job_data=job_data,)


class TestFailPushData(TestPumpWood):
    """Test Hierarchy Attribute resolvers."""

    load_balancer_address = 'http://localhost:8080/'
    apps_to_regenerate = ['pumpwood-datalake-app']

    def load_processed_data(self, list_of_data):
        """Create the processed data."""
        processed_data = {}
        for data in list_of_data:
            print("loading {file} data...".format(file=data))
            file_path = "tests/pusher/database/{file_name}.xlsx"
            df = pd.read_excel(file_path.format(file_name=data),
                               sheetname='Sheet1')
            to_loads = [
                col for col in df.columns
                if col in ["dimentions", "extra_info"]]

            for x in to_loads:
                df[x] = df[x].apply(json.loads)

            processed_data[data] = df
        return processed_data

    def test_fail_add_attribute(self):
        """Test fail try to add new attribute."""
        microservice = PumpWoodMicroService(**TEST_CONFIG['MICROSERVICE'])
        microservice.login()

        list_of_data = [
            'modeling_unit_dimention',
            'modeling_unit_hierarchy',
            'geoarea_dimention',
            'geoarea_hierarchy',
            'attribute_dimention',
            'attribute_categorical',
            'attribute_hierarchy',
            'database_categorical_data',
            'database_data', ]
        processed_data = self.load_processed_data(list_of_data)

        queue_data = microservice.list_one(
            model_class='DatabaseETLQueue', pk=4)
        job_data = microservice.list_one(
            model_class='DatabaseETLJob', pk=queue_data['job_id'])

        # Dimentions
        job_data["default_parameters"]["add_new_modeling_unit"] = True
        job_data["default_parameters"]["add_new_geoarea"] = True
        # job_data["default_parameters"]["add_new_attribute"] = True
        # job_data["default_parameters"]["add_new_geoatribute"] = True
        # job_data["default_parameters"]["add_new_calendar"] = True

        # Hierarchies
        job_data["default_parameters"]["add_hierarchy_modeling_unit"] = 'add'
        job_data["default_parameters"]["add_hierarchy_geoarea"] = 'add'
        job_data["default_parameters"]["add_hierarchy_attribute"] = 'add'
        # job_data["default_parameters"]["add_hierarchy_calendar"] = 'add'
        # job_data["default_parameters"]["add_hierarchy_geoatribute"] = 'add'

        # Categories
        job_data["default_parameters"]["add_new_categorical_attribute"] = 'add'
        job_data["default_parameters"]["add_new_categorical_calendar"] = 'add'
        job_data["default_parameters"][
            "add_new_categorical_geoatribute"] = 'add'

        pusher = CheckPusherClass(
            database_config=TEST_CONFIG["DATABASE"],
            microservice_config=TEST_CONFIG["MICROSERVICE"],
            storage_credencials=TEST_CONFIG["STORAGE"], )

        with self.assertRaises(Exception):
            pusher.push_data(
                processed_data=processed_data,
                queue_data=queue_data,
                job_data=job_data,)

    def test_push_geo_variables(self):
        """Test fail when try to add GeoAttribute hierarchy."""
        microservice = PumpWoodMicroService(**TEST_CONFIG['MICROSERVICE'])
        microservice.login()

        list_of_data = [
            # 'modeling_unit_dimention',
            # 'modeling_unit_hierarchy',
            'geoarea_dimention',
            'geoarea_hierarchy',
            'geoattribute_dimention',
            'geoattribute_categorical',
            'geoattribute_hierarchy',
            'geodatabase_categorical_data',
            'geodatabase_data', ]
        processed_data = self.load_processed_data(list_of_data)

        queue_data = microservice.list_one(
            model_class='DatabaseETLQueue', pk=4)
        job_data = microservice.list_one(
            model_class='DatabaseETLJob', pk=queue_data['job_id'])

        # Dimentions
        job_data["default_parameters"]["add_new_modeling_unit"] = True
        job_data["default_parameters"]["add_new_geoarea"] = True
        job_data["default_parameters"]["add_new_attribute"] = True
        job_data["default_parameters"]["add_new_geoatribute"] = True
        # job_data["default_parameters"]["add_new_calendar"] = True

        # Hierarchies
        job_data["default_parameters"]["add_hierarchy_modeling_unit"] = 'add'
        job_data["default_parameters"]["add_hierarchy_geoarea"] = 'add'
        # job_data["default_parameters"]["add_hierarchy_attribute"] = 'add'
        # job_data["default_parameters"]["add_hierarchy_calendar"] = 'add'
        # job_data["default_parameters"]["add_hierarchy_geoatribute"] = 'add'

        # Categories
        # job_data["default_parameters"][
        #   "add_new_categorical_attribute"] = 'add'
        # job_data["default_parameters"][
        #   "add_new_categorical_calendar"] = 'add'
        job_data["default_parameters"][
            "add_new_categorical_geoatribute"] = 'add'

        pusher = CheckPusherClass(
            database_config=TEST_CONFIG["DATABASE"],
            microservice_config=TEST_CONFIG["MICROSERVICE"],
            storage_credencials=TEST_CONFIG["STORAGE"], )

        with self.assertRaises(Exception):
            pusher.push_data(
                processed_data=processed_data,
                queue_data=queue_data,
                job_data=job_data,)

    def test_fail_add_modeling_unit(self):
        """Test fail when try to add modeling_unit."""
        microservice = PumpWoodMicroService(**TEST_CONFIG['MICROSERVICE'])
        microservice.login()

        list_of_data = [
            'modeling_unit_dimention',
            'modeling_unit_hierarchy',
            'geoarea_dimention',
            'geoarea_hierarchy',
            'attribute_dimention',
            'attribute_categorical',
            'attribute_hierarchy',
            'database_categorical_data',
            'database_data', ]
        processed_data = self.load_processed_data(list_of_data)

        queue_data = microservice.list_one(
            model_class='DatabaseETLQueue', pk=4)
        job_data = microservice.list_one(
            model_class='DatabaseETLJob', pk=queue_data['job_id'])

        # Dimentions
        # job_data["default_parameters"]["add_new_modeling_unit"] = True
        job_data["default_parameters"]["add_new_geoarea"] = True
        job_data["default_parameters"]["add_new_attribute"] = True
        # job_data["default_parameters"]["add_new_geoatribute"] = True
        # job_data["default_parameters"]["add_new_calendar"] = True

        # Hierarchies
        job_data["default_parameters"]["add_hierarchy_modeling_unit"] = 'add'
        job_data["default_parameters"]["add_hierarchy_geoarea"] = 'add'
        job_data["default_parameters"]["add_hierarchy_attribute"] = 'add'
        # job_data["default_parameters"]["add_hierarchy_calendar"] = 'add'
        # job_data["default_parameters"]["add_hierarchy_geoatribute"] = 'add'

        # Categories
        job_data["default_parameters"]["add_new_categorical_attribute"] = 'add'
        # job_data["default_parameters"][
        #   "add_new_categorical_calendar"] = 'add'
        # job_data["default_parameters"][
        #   "add_new_categorical_geoatribute"] = 'add'

        pusher = CheckPusherClass(
            database_config=TEST_CONFIG["DATABASE"],
            microservice_config=TEST_CONFIG["MICROSERVICE"],
            storage_credencials=TEST_CONFIG["STORAGE"], )

        with self.assertRaises(Exception):
            pusher.push_data(
                processed_data=processed_data,
                queue_data=queue_data,
                job_data=job_data,)


class TestPushFailDuplicateData(TestPumpWood):
    """Test Hierarchy Attribute resolvers."""

    load_balancer_address = 'http://localhost:8080/'
    apps_to_regenerate = ['pumpwood-datalake-app']

    def load_processed_data(self, list_of_data):
        """Create the processed data."""
        processed_data = {}
        for data in list_of_data:
            print("loading {file} data...".format(file=data))
            file_path = "tests/pusher/database/{file_name}.xlsx"
            df = pd.read_excel(file_path.format(file_name=data),
                               sheetname='Sheet1')
            to_loads = [
                col for col in df.columns
                if col in ["dimentions", "extra_info"]]

            for x in to_loads:
                df[x] = df[x].apply(json.loads)

            processed_data[data] = df
        return processed_data

    def test_fail_duplicate_geo_data(self):
        """Test fail to push duplicated geodata."""
        microservice = PumpWoodMicroService(**TEST_CONFIG['MICROSERVICE'])
        microservice.login()

        list_of_data = [
            'geoarea_dimention',
            'geoarea_hierarchy',
            'geoattribute_dimention',
            'geoattribute_categorical',
            'geoattribute_hierarchy',
            'geodatabase_categorical_data',
            'geodatabase_data', ]
        processed_data = self.load_processed_data(list_of_data)
        processed_data['geodatabase_data'] = \
            processed_data['geodatabase_data'].append(
                processed_data['geodatabase_data'][1:15])

        queue_data = microservice.list_one(
            model_class='DatabaseETLQueue', pk=4)
        job_data = microservice.list_one(
            model_class='DatabaseETLJob', pk=queue_data['job_id'])

        job_data["default_parameters"]["add_new_geoarea"] = True
        job_data["default_parameters"]["add_new_geoatribute"] = True

        job_data["default_parameters"]["add_hierarchy_geoarea"] = 'add'
        job_data["default_parameters"]["add_hierarchy_geoatribute"] = 'add'

        job_data["default_parameters"][
            "add_new_categorical_geoatribute"] = 'add'

        pusher = CheckPusherClass(
            database_config=TEST_CONFIG["DATABASE"],
            microservice_config=TEST_CONFIG["MICROSERVICE"],
            storage_credencials=TEST_CONFIG["STORAGE"], )

        with self.assertRaises(Exception):
            pusher.push_data(
                processed_data=processed_data,
                queue_data=queue_data,
                job_data=job_data,)

    def test_fail_duplicate_geo_cat(self):
        """Test fail to push duplicated geo categoric."""
        microservice = PumpWoodMicroService(**TEST_CONFIG['MICROSERVICE'])
        microservice.login()

        list_of_data = [
            'geoarea_dimention',
            'geoarea_hierarchy',
            'geoattribute_dimention',
            'geoattribute_categorical',
            'geoattribute_hierarchy',
            'geodatabase_categorical_data',
            'geodatabase_data', ]
        processed_data = self.load_processed_data(list_of_data)
        processed_data['geodatabase_categorical_data'] = \
            processed_data['geodatabase_categorical_data'].append(
                processed_data['geodatabase_categorical_data'][1:15])

        queue_data = microservice.list_one(
            model_class='DatabaseETLQueue', pk=4)
        job_data = microservice.list_one(
            model_class='DatabaseETLJob', pk=queue_data['job_id'])

        job_data["default_parameters"]["add_new_geoarea"] = True
        job_data["default_parameters"]["add_new_geoatribute"] = True

        job_data["default_parameters"]["add_hierarchy_geoarea"] = 'add'
        job_data["default_parameters"]["add_hierarchy_geoatribute"] = 'add'

        job_data["default_parameters"][
            "add_new_categorical_geoatribute"] = 'add'

        pusher = CheckPusherClass(
            database_config=TEST_CONFIG["DATABASE"],
            microservice_config=TEST_CONFIG["MICROSERVICE"],
            storage_credencials=TEST_CONFIG["STORAGE"], )

        with self.assertRaises(Exception):
            pusher.push_data(
                processed_data=processed_data,
                queue_data=queue_data,
                job_data=job_data,)

    def test_fail_duplicate_att_data(self):
        """Test fail to push duplicated attribute data."""
        microservice = PumpWoodMicroService(**TEST_CONFIG['MICROSERVICE'])
        microservice.login()

        list_of_data = [
            'modeling_unit_dimention',
            'modeling_unit_hierarchy',
            'geoarea_dimention',
            'geoarea_hierarchy',
            'attribute_dimention',
            'attribute_categorical',
            'attribute_hierarchy',
            'database_categorical_data',
            'database_data', ]
        processed_data = self.load_processed_data(list_of_data)
        processed_data['database_data'] = \
            processed_data['database_data'].append(
                processed_data['database_data'][1:15])

        queue_data = microservice.list_one(
            model_class='DatabaseETLQueue', pk=4)
        job_data = microservice.list_one(
            model_class='DatabaseETLJob', pk=queue_data['job_id'])

        # Dimentions
        job_data["default_parameters"]["add_new_modeling_unit"] = True
        job_data["default_parameters"]["add_new_geoarea"] = True
        job_data["default_parameters"]["add_new_attribute"] = True
        # Hierarchies
        job_data["default_parameters"]["add_hierarchy_modeling_unit"] = 'add'
        job_data["default_parameters"]["add_hierarchy_geoarea"] = 'add'
        job_data["default_parameters"]["add_hierarchy_attribute"] = 'add'
        # Categories
        job_data["default_parameters"]["add_new_categorical_attribute"] = 'add'

        pusher = CheckPusherClass(
            database_config=TEST_CONFIG["DATABASE"],
            microservice_config=TEST_CONFIG["MICROSERVICE"],
            storage_credencials=TEST_CONFIG["STORAGE"], )

        with self.assertRaises(Exception):
            pusher.push_data(
                processed_data=processed_data,
                queue_data=queue_data,
                job_data=job_data,)

    def test_fail_duplicate_att_cat(self):
        """Test fail push duplicated attribute categoric."""
        microservice = PumpWoodMicroService(**TEST_CONFIG['MICROSERVICE'])
        microservice.login()

        list_of_data = [
            'modeling_unit_dimention',
            'modeling_unit_hierarchy',
            'geoarea_dimention',
            'geoarea_hierarchy',
            'attribute_dimention',
            'attribute_categorical',
            'attribute_hierarchy',
            'database_categorical_data',
            'database_data', ]
        processed_data = self.load_processed_data(list_of_data)
        processed_data['database_categorical_data'] = \
            processed_data['database_categorical_data'].append(
                processed_data['database_categorical_data'][1:15])

        queue_data = microservice.list_one(
            model_class='DatabaseETLQueue', pk=4)
        job_data = microservice.list_one(
            model_class='DatabaseETLJob', pk=queue_data['job_id'])

        # Dimentions
        job_data["default_parameters"]["add_new_modeling_unit"] = True
        job_data["default_parameters"]["add_new_geoarea"] = True
        job_data["default_parameters"]["add_new_attribute"] = True
        # Hierarchies
        job_data["default_parameters"]["add_hierarchy_modeling_unit"] = 'add'
        job_data["default_parameters"]["add_hierarchy_geoarea"] = 'add'
        job_data["default_parameters"]["add_hierarchy_attribute"] = 'add'
        # Categories
        job_data["default_parameters"]["add_new_categorical_attribute"] = 'add'

        pusher = CheckPusherClass(
            database_config=TEST_CONFIG["DATABASE"],
            microservice_config=TEST_CONFIG["MICROSERVICE"],
            storage_credencials=TEST_CONFIG["STORAGE"], )

        with self.assertRaises(Exception):
            pusher.push_data(
                processed_data=processed_data,
                queue_data=queue_data,
                job_data=job_data,)

    def test_fail_duplicate_cal_data(self):
        """Test fail to push duplicated calendar data."""
        microservice = PumpWoodMicroService(**TEST_CONFIG['MICROSERVICE'])
        microservice.login()

        list_of_data = [
            'calendar_dimention',
            'calendar_categorical',
            'calendar_hierarchy',
            'calendar_categorical_data',
            'calendar_data']
        processed_data = self.load_processed_data(list_of_data)
        processed_data['calendar_data'] = \
            processed_data['calendar_data'].append(
                processed_data['calendar_data'][1:15])

        queue_data = microservice.list_one(
            model_class='DatabaseETLQueue', pk=4)
        job_data = microservice.list_one(
            model_class='DatabaseETLJob', pk=queue_data['job_id'])

        # Dimentions
        job_data["default_parameters"]["add_new_calendar"] = True
        # Hierarchies
        job_data["default_parameters"]["add_hierarchy_calendar"] = 'add'
        # Categories
        job_data["default_parameters"]["add_new_categorical_calendar"] = 'add'

        pusher = CheckPusherClass(
            database_config=TEST_CONFIG["DATABASE"],
            microservice_config=TEST_CONFIG["MICROSERVICE"],
            storage_credencials=TEST_CONFIG["STORAGE"], )

        with self.assertRaises(Exception):
            pusher.push_data(
                processed_data=processed_data,
                queue_data=queue_data,
                job_data=job_data,)

    def test_fail_duplicate_cal_cat(self):
        """Test fail to push duplicated calendar categoric."""
        microservice = PumpWoodMicroService(**TEST_CONFIG['MICROSERVICE'])
        microservice.login()

        list_of_data = [
            'calendar_dimention',
            'calendar_categorical',
            'calendar_hierarchy',
            'calendar_categorical_data',
            'calendar_data']
        processed_data = self.load_processed_data(list_of_data)
        processed_data['calendar_categorical_data'] = \
            processed_data['calendar_categorical_data'].append(
                processed_data['calendar_categorical_data'][1:15])

        queue_data = microservice.list_one(
            model_class='DatabaseETLQueue', pk=4)
        job_data = microservice.list_one(
            model_class='DatabaseETLJob', pk=queue_data['job_id'])

        # Dimentions
        job_data["default_parameters"]["add_new_calendar"] = True
        # Hierarchies
        job_data["default_parameters"]["add_hierarchy_calendar"] = 'add'
        # Categories
        job_data["default_parameters"]["add_new_categorical_calendar"] = 'add'

        pusher = CheckPusherClass(
            database_config=TEST_CONFIG["DATABASE"],
            microservice_config=TEST_CONFIG["MICROSERVICE"],
            storage_credencials=TEST_CONFIG["STORAGE"], )

        with self.assertRaises(Exception):
            pusher.push_data(
                processed_data=processed_data,
                queue_data=queue_data,
                job_data=job_data,)
