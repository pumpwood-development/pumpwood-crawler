# -*- coding: utf-8 -*-
"""Default crawler models."""

# import io
# import pandas
# import simplejson as json
from flask import request
from datetime import datetime

from sqlalchemy.sql import func
from slugify import slugify
from pumpwood_communication import exceptions
from pumpwoodflask_views.action import action

from sqlalchemy.dialects.postgresql.json import JSONB
from sqlalchemy.ext.declarative import declared_attr
from werkzeug.utils import secure_filename

from flask_sqlalchemy import SQLAlchemy
from pumpwood_flaskmisc.storage import allowed_extension

from pumpwoodflask_views.auth import AuthFactory


db = SQLAlchemy()


#################
# Crawler Classes
class PullerJob(object):
    """Define default PullerJob."""

    queue_set = None
    """One to many Job to Queue."""

    binded_pusher_job_set = None
    """Define many-to-many relation with Puller and Pusher Jobs."""

    name = db.Column(db.String(154), nullable=False, unique=True, index=True)
    notes = db.Column(db.Text(), nullable=True)
    last_on_queue = db.Column(db.DateTime(
        timezone=False), unique=True, nullable=True, index=True)
    default_parameters = db.Column(JSONB, server_default='{}')
    default_files = db.Column(JSONB, nullable=True)
    created_at = db.Column(db.DateTime(timezone=False), nullable=True,
                           server_default=func.now(), index=True)
    created_by_id = db.Column(db.Integer, nullable=False, index=True)

    def __repr__(self):
        """__repr__."""
        return '<%d:PullerJob %r>' % (self.id or -1, self.name)

    def create_job_queue(self, extra_parameters: dict = {}):
        """Create one or more job queues."""
        raise Exception('Not implemented create_job_queue')


QUEUE_STATUS_PULLER = (
    'idle', 'pulling', 'error_pulling', 'saving', 'error_saving',
    'finish_pulling', 'processing', "error_processing", 'pushing',
    'error_pushing', 'finish_pushing', 'canceled')


class PullerQueue(object):
    """Define default PullerQueue."""

    job_id = None
    """Define many-to-one foreign key with Puller Job."""
    job = None
    """Define many-to-one relation with Puller Job."""

    created_at = db.Column(db.DateTime(timezone=False),
                           nullable=True, default=datetime.utcnow,
                           index=True)
    start_time = db.Column(db.DateTime(timezone=False),
                           nullable=True, index=True)
    end_time = db.Column(db.DateTime(timezone=False),
                         nullable=True, index=True)
    status = db.Column(db.String(20),
                       nullable=False, server_default="idle", index=True,
                       default="idle")
    results = db.Column(JSONB, nullable=False,
                        server_default='{}', default={})
    extra_parameters = db.Column(JSONB, nullable=False,
                                 server_default='{}', default={})
    extra_files = db.Column(JSONB, nullable=False, server_default='{}',
                            default={})
    created_by_id = db.Column(db.Integer, nullable=False, index=True)

    #################################################################
    pusher_job_class = None
    """Name of the class of the pusher."""
    microservice = None
    """PumpWoodMicroService object."""
    #################################################################

    @declared_attr
    def __table_args__(cls):
        return (db.CheckConstraint(cls.status.in_(QUEUE_STATUS_PULLER)), )

    def __repr__(self):
        """__repr__."""
        return '<%d:PullerQueue >' % (self.id or -1, )

    @action(info="Run binded pullers of a pusher job.")
    def run_binded_pushers(self, send_to_rabbitmq: bool = True):
        """Run binded pullers of a pusher job."""
        auth_header = AuthFactory.get_auth_header()

        # Get pusher jobs associated with puller jobs
        binded_pusher_jobs = self.job.binded_pusher_job_set

        resp = []
        for pusher_job in binded_pusher_jobs:
            action_result = self.microservice.execute_action(
                self.pusher_job_class, action="create_job_queue",
                pk=pusher_job.id, parameters={
                    "puller_queue_id": self.id,
                    "send_to_rabbitmq": send_to_rabbitmq},
                auth_header=auth_header)
            resp.append(action_result["result"])
        return resp


################
# Pusher Classes
class PusherJob(object):
    """Define default PusherJob."""

    queue_set = None
    """Define one-to-many relation with Pusher Queue."""

    name = db.Column(db.String(154), nullable=False, unique=True, index=True)
    notes = db.Column(db.Text(), nullable=True)
    last_on_queue = db.Column(db.DateTime(timezone=False), unique=True,
                              nullable=True, index=True)
    etl_file = db.Column(db.String(200), nullable=False)
    """
    An Python file with one function defined as:
    def etl_process(storage, microservice, db_engine, fn_parameters, fn_files):
        import io
        import pandas as pd
        import numpy as np
        import slugify
        import dateutil

        --- code ---

        return{
        }
    """
    default_parameters = db.Column(JSONB, nullable=False,
                                   server_default='{}', default={})
    """
    An dictionary defing default paramenter to process:
        add_new_modeling_unit (bool): Set if process can add new
            modeling units.
        add_new_attribute (bool): Set if process can add new attribute.
        add_new_geoarea (bool): Set if process can add new geoarea.
    """
    default_extra_files = db.Column(JSONB, nullable=False,
                                    server_default='{}', default={})
    created_at = db.Column(db.DateTime(timezone=False), nullable=True,
                           server_default=func.now(), index=True)
    created_by_id = db.Column(db.Integer, nullable=False, index=True)

    # Class pameters
    EXTRA_FILES_EXTENSIONS = []
    """Set the queue extra files possible extensions."""
    storage_obj = None
    """Storage object."""
    microservice = None
    """PumpWoodMicroService object."""
    rabbitmq_obj = None
    """RabbitMQ communication object."""
    queue_class = None
    """PusherQueue class."""

    @action(info="Creates a queue of the job. Files passed on request" +
                 "will be appended at queue's extra_files.")
    def create_job_queue(self, puller_queue_id: int = None,
                         extra_parameters: dict = {},
                         send_to_rabbitmq: bool = True):
        """Create a job queue."""
        token = request.headers.get('Authorization', None)
        now = datetime.utcnow().strftime('%Y-%m-%d-%H:%M:%S%f')

        #########################
        # Saving the extra_files#
        file_keys = request.files.keys()
        extra_files = {}
        model_name = slugify(self.queue_class)
        for file_key in file_keys:
            file_list = request.files.getlist(file_key)

            if len(file_list) > 1:
                raise exceptions.PumpWoodException(
                    "More than one file for a key")

            file = file_list[0]
            file_filename = secure_filename(file.filename)
            allowed_extension(
                filename=file_filename,
                allowed_extensions=self.EXTRA_FILES_EXTENSIONS,
                exception=exceptions.PumpWoodException)
            file_path = '{model_name}__extra_files/'.format(
                model_name=model_name)
            self.storage_object.write_file(
                file_path=file_path, file_name=file_filename,
                data=file.read(),
                content_type=file.mimetype,
                if_exists='overide')
            extra_files[file_key] = file_path

        save_result = self.microservice.save({
                'model_class': self.queue_class, 'job_id': self.id,
                "puller_queue_id": puller_queue_id,
                'extra_parameters': extra_parameters,
                'extra_files': extra_files},
                auth_header={'Authorization': token})

        self.last_on_queue = now
        if send_to_rabbitmq:
            self.rabbitmq_obj.send(save_result)

        return save_result


QUEUE_STATUS_PUSHER = ('idle', 'processing', "error_processing", 'pushing',
                       'error_pushing', 'finish_pushing', 'canceled')


class PusherQueue(object):
    """Define a default queue for Pusher."""

    """Define table name for Pusher Job."""
    job_id = None
    """Define many-to-one foreign key with Pusher Job."""
    job = None
    """Define many-to-one relation with Pusher Job."""
    puller_queue_id = None
    """Define one-to-many relation with a puller queue."""

    created_at = db.Column(db.DateTime(timezone=False), nullable=True,
                           server_default=func.now(), index=True)
    start_time = db.Column(db.DateTime(timezone=False), nullable=True,
                           index=True)
    end_time = db.Column(db.DateTime(timezone=False), nullable=True,
                         index=True)
    status = db.Column(db.String(20), nullable=True, default="idle",
                       index=True)
    results = db.Column(JSONB, nullable=False,
                        server_default='{}', default={})
    extra_parameters = db.Column(JSONB, nullable=False,
                                 server_default='{}', default={})
    extra_files = db.Column(JSONB, nullable=False,
                            server_default='{}', default={})
    created_by_id = db.Column(db.Integer, nullable=False, index=True)

    @declared_attr
    def __table_args__(cls):
        return (db.CheckConstraint(cls.status.in_(QUEUE_STATUS_PUSHER)), )
